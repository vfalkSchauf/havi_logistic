﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using MVC.Data.Entities;
using MVC.Models.ConfigurationViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Interfaces
{
    public interface IConfigurationFacade
    {
        #region Gates
        /// <summary>
        /// Gets the current Gate settings from the repository
        /// and generates the ConfigurationGatesViewModel with the accquired 
        /// Information.
        /// </summary>
        /// <returns></returns>
        ConfigurationGatesViewModel GetGatesViewModel();

        /// <summary>
        /// Tells the IGatesRepository to add the passed along Gate.
        /// </summary>
        /// <param name="gate"></param>
        /// <returns></returns>
        Task AddGate(Gate gate);

        /// <summary>
        /// Tells the IGeneralSettingsRepositoy to set the default 
        /// gates using the information from the passed along Viewmodel.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task SetDefaultGates(ConfigurationGatesViewModel model);

        /// <summary>
        /// Tells the IGatesRepositoy to delete the Gate with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteGate(Guid id);

        /// <summary>
        /// Tells the IGatesRepository to update the information
        /// in the database to match the new values of the passed along gate.
        /// </summary>
        /// <param name="gate"></param>
        /// <returns></returns>
        Task EditGate(Gate gate);

        /// <summary>
        /// Gets and returns the gate with the given id from the
        /// IGatesRepository.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Gate</returns>
        Gate GetGate(Guid id);

        /// <summary>
        /// Gets and returns a list of all gates from the
        /// IGatesRepository.
        /// </summary>
        /// <returns></returns>
        List<Gate> GetAllGates();

        /// <summary>
        /// Get new GateViewModel
        /// </summary>
        /// <returns></returns>
        GateViewModel GetGateViewModel();

        /// <summary>
        /// Get GateViewModel from Gate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        GateViewModel GetGateViewModel(Guid id);

        /// <summary>
        /// Utilizes the ICsvReader to generate gates from the
        /// csv-file and tells the IGatesRepository to add the gates to the DB.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Task ImportGatesFromCSV(IFormFile file);
        #endregion

        #region ForwardingAgencies
        /// <summary>
        /// Accquires the list of all ForwardingAgencies from the IForwardingRepository
        /// to create the ForwardingAgenciesIndexViewModel.
        /// </summary>
        /// <returns></returns>
        ForwardingAgenciesIndexViewModel GetForwardingAgenciesIndexViewModel();

        /// <summary>
        /// Creates a ForwardingAgencyViewModel with a new but empty ForwardingAgency 
        /// and the List of all Gates from the IGatesRepository.
        /// </summary>
        /// <returns></returns>
        ForwardingAgencyViewModel GetForwardingAgencyViewModel();

        /// <summary>
        /// Creates a ForwardingAgencyViewModel, filled with the
        /// ForwardingAgency with the given ID, as accquired from the IForwardingAgenciesRepository, and the list of all Gates
        /// from the IGatesRepository.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ForwardingAgencyViewModel GetForwardingAgencyViewModel(Guid id);

        /// <summary>
        /// Takes the ForwardingAgency from the model and tells the
        /// IForwardingAgenciesRepository to add it to the DB.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddForwardingAgency(ForwardingAgencyViewModel model);

        /// <summary>
        /// Tells the IForwardingAgenciesRepository to delete the
        /// ForwardingAgency with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteForwardingAgency(Guid id);

        /// <summary>
        /// Gets the forwardingAgency from the model and tells
        /// the IForwardingAgenciesRepository to update the data for that Agency.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task EditForwardingAgency(ForwardingAgencyViewModel model);

        /// <summary>
        /// Uses the ICsvReader to create ForwardingAgencies from the 
        /// CSV-file and tells the IForwardingRepository to add the agencies 
        /// to the db.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Task ImportForwardingAgenciesFromCSV(IFormFile file);
        #endregion

        #region UnknownForwardingAgencies
        /// <summary>
        /// Gets the list of all unknownForwardingAgencies from the IUnknownForwardingAgenciesRepository
        /// to create the ViewModel.
        /// </summary>
        /// <returns></returns>
        UnknownForwardingAgenciesViewModel GetUnknownForwardingAgenciesViewModel();

        /// <summary>
        /// Creates the unkownSuppliers csv file and creates a filestream with access to it.
        /// </summary>
        /// <exception cref="Exception"></exception>
        /// <returns>FileStream</returns>
        FileStream ExportUnknownForwardingAgenciesCSVStream();

        /// <summary>
        /// Creates the unkownSuppliers xml file and creates a filestream with access to it.
        /// </summary>
        /// <exception cref="Exception"></exception>
        /// <returns>FileStream</returns>
        FileStream ExportUnknownForwardingAgenciesXMLStream();

        Task AddUnknownForwardingAgencieToKnownForwardingAgencies(Guid id);
        #endregion

        #region DeliveryCountries
        Task AddDeliveryCountry(DeliveryCountry DeliveryCountry);

        Task DeleteDeliveryCountry(Guid id);

        DeliveryCountry GetDeliveryCountry(Guid id);

        List<DeliveryCountry> GetAllDeliveryCountry();

        DeliveryCountriesViewModel GetDeliveryCountriesViewModel();

        /// <summary>
        /// Tells the IGeneralSettingsRepositoy to set the default 
        /// DeliveryCountry using the information from the passed along Viewmodel.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task SetDefaultDeliveryCountry(DeliveryCountriesViewModel model);

        Task EditDeliveryCountry(DeliveryCountry DeliveryCountry);
        #endregion

        #region Displays
        /// <summary>
        /// Gets the List of all DisplayConfigurations from the IDisplayConfigurationRepository
        /// to create the ViewModel.
        /// </summary>
        /// <returns></returns>
        DisplayIndexViewModel GetDisplayIndexViewModel();

        /// <summary>
        /// Tells the IDisplayConfigurationrepository to add the new displayConfig.
        /// </summary>
        /// <param name="displayConfig"></param>
        /// <returns></returns>
        Task AddDisplay(DisplayConfiguration displayConfig);

        /// <summary>
        /// Tells the IDisplayConfigurationRepository to remove the displayConfig with
        /// the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteDisplay(Guid id);

        /// <summary>
        /// Calls the DisplayConfigurationRepository to edit
        /// the DB values for the passed along displayConfig.
        /// </summary>
        /// <param name="displayConfiguration"></param>
        /// <returns></returns>
        Task EditDisplay(DisplayConfiguration displayConfiguration);

        /// <summary>
        /// Gets the DisplayConfiguration with the given ID from the
        /// IDisplayConfigurationRepository.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DisplayConfiguration GetDisplayConfiguration(Guid id);

        #endregion

        #region Users

        /// <summary>
        /// Gets the List of all Users from the UserManager and 
        /// creates the ViewModel with it.
        /// </summary>
        /// <returns></returns>
        UserIndexViewModel GetUserIndexViewModel();

        /// <summary>
        /// Creates an AppUser from the data provided by the 
        /// model and tell the UserManager to add the new user.
        /// Also sets the Roles of the user based on the AuthorizationGroup.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<IdentityResult> AddUser(UserViewModel model);

        /// <summary>
        /// Uses the UserManager to find the user with the given id
        /// and tells the manager to delete it.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IdentityResult> DeleteUser(string id);

        /// <summary>
        /// Gets the user with the given id from the UserManager
        /// and adds the list of AuthorizationGroups from the IAuthorizationGroupsRepository
        /// to the ViewModel.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<UserViewModel> GetUserViewModel(string id);

        /// <summary>
        /// Creates a UserViewModel where the user Data is empty
        /// but the List of AuthorizationGroups is acquired from the
        /// IAuthorizationGroupsRepository.
        /// </summary>
        /// <returns></returns>
        UserViewModel GetUserViewModel();


        /// <summary>
        /// Validates the Data.
        /// Gets the AppUser from the UserManager and updates the info. Sets the roles 
        /// and finally tells the UserManager to perform the update on the user.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userValidator"></param>
        /// <param name="passwordHasher"></param>
        /// <param name="passwordValidator"></param>
        /// <returns></returns>
        Task<IdentityResult> EditUser(UserViewModel model, IUserValidator<AppUser> userValidator, IPasswordHasher<AppUser> passwordHasher, IPasswordValidator<AppUser> passwordValidator);

        #endregion

        #region Authorization groups
        /// <summary>
        /// Gets the list of all the names from the AuthorizationGroups
        /// provided by the IAuthorizationGroupsRepository.
        /// </summary>
        /// <returns></returns>
        List<string> AuthorizationGroups();

        /// <summary>
        /// Creates the new Viewmodel and fills it with all groups accquired
        /// from the IAuthorizationGroupsRepository.
        /// </summary>
        /// <returns></returns>
        AuthorizationGroupsViewModel GetAuthorizationGroupsViewModel();

        /// <summary>
        /// Gets the AuthorizationGroup from the model and tells
        /// the AuthorizationGroupsRepository to add the group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddAuthorizationGroup(GroupViewModel model);

        /// <summary>
        /// Tells the AuthorizationGroupsRepository to apply the new values
        /// and updates the roles for all users of the group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task EditAuthorizationGroup(GroupViewModel model);

        /// <summary>
        /// Tells the AuthorizationGroupsRepository to remve the group.
        /// Also sets the AuthorizationGroup of all the users of the group to "" and updates
        /// the roles of the users. This will lead to all users of that group having no roles at all.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAuthorizationGroup(Guid id);

        /// <summary>
        /// Gets the group with the given id from the IAuthorizationGroupsRepository
        /// and creates the ViewModel with it.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        GroupViewModel GetGroupViewModel(Guid id);

        #endregion

        #region Active Directory
        /// <summary>
        /// Gets and returns the ADSettings from the IADSettingsRepository
        /// </summary>
        /// <returns></returns>
        ADSettings GetADSettings();

        /// <summary>
        /// Tells the IADSettingsRepository to set the
        /// settings to those passed along.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        Task SetADSettings(ADSettings settings);

        #endregion
    }
}
