﻿using System;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Interfaces
{
    public interface IProcessingHubFacade
    {


        /// <summary>
        /// Updates the Comment of the registration with the given id
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="LicensePlate"></param>
        /// <returns></returns>
        Task UpdateLicensePlate(Guid ID, string LicensePlate);

        /// <summary>
        /// Updates the Comment of the registration with the given id
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Comment"></param>
        /// <returns></returns>
        Task UpdateComment(Guid ID, string Comment);

        /// <summary>
        /// exectues 'Call(id, curTime)' on the IOpenRegistrationRepository to set the time.
        /// afterwards tells the DisplayFacade to Update the display. 
        /// If the Update was not successfull, Call is executed again on the 
        /// IOpenRegisttaionRepository with new DateTime() as time.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task Call(Guid id, DateTime curTime);

        /// <summary>
        /// exectues 'SetEntry(id, curTime)' on the IOpenRegistrationRepository to set the time.
        /// afterwards tells the DisplayFacade to Update the display. 
        /// If the Update was not successfull, SetEntry is executed again on the 
        /// IOpenRegisttaionRepository with new DateTime() as time.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task SetEntry(Guid id, DateTime curTime);

        /// <summary>
        /// tells the OpenRegistrationsRepository to remove the registration with 
        /// the given id.
        /// Creates a new ClosedRegistration from the registration data and calls
        /// all clients to add the closed registration.
        /// Finally updates the displays
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task SetExit(Guid id, DateTime curTime);
        
        /// <summary>
        /// Withdraw latest call with given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task WithdrawCall(Guid id);

        /// <summary>
        /// Withdraw latest Entry with given id,
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task WithdrawEntry(Guid id);

        /// <summary>
        /// Tells the openRegistrationRepository to set the new gate.
        /// Updates the displays afterwards.
        /// If the Update was unsuccessfull, call setGate on the OpenRegistrationRepository again with
        /// the pre change value, to restore the original state.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task SetGate(Guid id, string value);
    }
}
