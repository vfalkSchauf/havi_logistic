﻿using System.Collections.Generic;

namespace MVC.BusinessLogic.Interfaces
{
    public interface IADReader
    {
        /// <summary>
        /// Creates a list of all groupnames that the
        /// user with the given name is part of.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<string> GetADGroups(string name);

        /// <summary>
        /// Check the Login at the Active Directery with the given Username and password.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool CheckADLogin(string userName, string password);

    }
}
