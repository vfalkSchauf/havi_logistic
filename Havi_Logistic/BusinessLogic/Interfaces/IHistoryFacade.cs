﻿using MVC.Models;
using System.IO;

namespace MVC.BusinessLogic.Interfaces
{
    public interface IHistoryFacade
    {
        /// <summary>
        /// Gets all closed Registrations from the IClosedRegistrationsRepository and fills
        /// the new ViewModel with them.
        /// </summary>
        /// <returns></returns>
        HistoryViewModel GetHistoryViewModel();

        /// <summary>
        /// Gets the CSV string from the IExportFacade, writes it to a .csv-file and returns a filestream
        /// to said file.
        /// </summary>
        /// <param name="UserRole"></param>
        /// <returns></returns>
        FileStream ExportHistoryCSV(string userRole);
    }
}
