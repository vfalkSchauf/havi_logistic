﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Controllers.SignalR;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Implementations
{
    public class ProcessingHubFacade : IProcessingHubFacade
    {
        private readonly ILogger<ProcessingHubFacade> _logger;

        private readonly IServiceProvider _serviceProvider;
        private readonly IHubContext<HistoryHub> _hubContext;

        public ProcessingHubFacade(ILogger<ProcessingHubFacade> logger, IServiceProvider serviceProvider, IHubContext<HistoryHub> hubContext)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
            _hubContext = hubContext;
        }

        public async Task UpdateComment(Guid ID, string Comment)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();

                    await _openRegistrationsRepository.SetComment(ID, Comment);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateLicensePlate(Guid ID, string LicensePlate)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();

                    await _openRegistrationsRepository.SetLicensePlate(ID, LicensePlate);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Call(Guid id, DateTime curTime)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    await _openRegistrationsRepository.Call(id, curTime);
                }
            }
            catch (Exception)
            {
                // throw the exception further, so that the user will see the message
                throw;
            }
        }

        public async Task WithdrawCall(Guid id)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    await _openRegistrationsRepository.WithdrawCall(id);
                }
            }
            catch (Exception)
            {
                // throw the exception further, so that the user will see the message
                throw;
            }
        }

        public async Task SetEntry(Guid id, DateTime curTime)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    await _openRegistrationsRepository.SetEntry(id, curTime);
                }
            }
            catch (Exception)
            {
                // throw the exception further, so that the user will see the message
                throw;
            }
        }

        public async Task WithdrawEntry(Guid id)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    await _openRegistrationsRepository.WithdrawEntry(id);
                }
            }
            catch (Exception)
            {
                // throw the exception further, so that the user will see the message
                throw;
            }
        }

        public async Task SetExit(Guid id, DateTime curTime)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    var _closedRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IClosedRegistrationsRepository>();
                    var _forwardingAgenciesRepository = scope.ServiceProvider.GetRequiredService<IForwardingAgenciesRepository>();

                    var regist = _openRegistrationsRepository.Get(id);
                    var openTimes = _openRegistrationsRepository.GetRegistrationTimesFromRegistration(regist.ID);
                    await _openRegistrationsRepository.Remove(regist, openTimes);

                    openTimes.Where(x => x.IsLastEdited == true).FirstOrDefault().TimeOfExit = curTime;

                    var erpID = 0;
                    if (!String.IsNullOrEmpty(regist.ForwardingAgency))
                    {
                        var agency = _forwardingAgenciesRepository.Get(regist.ForwardingAgency);
                        if (agency != null)
                        {
                            erpID = agency.ERP_ID;
                        }
                    }
                    var closed = new ClosedRegistration(regist, erpID);
                    List<ClosedTimes> closedTimes = new List<ClosedTimes>();

                    foreach (OpenTimes openTime in openTimes)
                        closedTimes.Add(new ClosedTimes(openTime));

                    await _closedRegistrationsRepository.Add(closed, closedTimes);

                    if (closed.JobType == EJobType.DeliverAndPickUp)
                        await _hubContext.Clients.All.SendAsync("AddEntry", closed, closedTimes);
                    else if (closed.JobType == EJobType.Deliver)
                    {
                        await _hubContext.Clients.Groups("Deliver").SendAsync("AddEntry", closed, closedTimes);
                        await _hubContext.Clients.Groups("DeliverAndPickUp").SendAsync("AddEntry", closed, closedTimes);
                    }
                    else if (closed.JobType == EJobType.PickUp)
                    {
                        await _hubContext.Clients.Groups("PickUp").SendAsync("AddEntry", closed, closedTimes);
                        await _hubContext.Clients.Groups("DeliverAndPickUp").SendAsync("AddEntry", closed, closedTimes);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task SetGate(Guid id, string value)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _openRegistrationsRepository = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var _displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();

                    var oldGate = await _openRegistrationsRepository.SetGate(id, value);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
