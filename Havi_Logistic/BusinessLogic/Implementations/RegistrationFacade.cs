﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Controllers.SignalR;
using MVC.Data.Entities;
using MVC.Models;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Implementations
{
    public class RegistrationFacade : IRegistrationFacade
    {
        private readonly ILogger _logger;

        private readonly IForwardingAgenciesRepository _agenciesRepository;
        private readonly IGeneralSettingsRepository _generalSettingsRepository;
        private readonly IOpenRegistrationsRepository _openRegistrationsRepository;
        private readonly IGatesRepository _gatesRepository;
        private readonly IDisplayFacade _displayFacade;
        private readonly IDeliveryCountriesRepository _deliveryCountryRepository;
        private readonly IForwardingAgenciesRepository _forwardingAgenciesRepository;

        private readonly IServiceProvider _serviceProvider;
        private readonly IHubContext<ProcessingHub> _hubContext;

        public RegistrationFacade(ILogger<RegistrationFacade> logger, IForwardingAgenciesRepository agenciesRepository, IGeneralSettingsRepository generalSettingsRepository,
            IOpenRegistrationsRepository openRegistrationsRepository, IHubContext<ProcessingHub> hubContext, IGatesRepository gatesRepository, IServiceProvider serviceProvider,
            IDisplayFacade displayFacade, IDeliveryCountriesRepository deliveryCountries, IForwardingAgenciesRepository forwardingAgenciesRepository)
        {
            _logger = logger;
            _agenciesRepository = agenciesRepository;
            _generalSettingsRepository = generalSettingsRepository;
            _openRegistrationsRepository = openRegistrationsRepository;
            _hubContext = hubContext;
            _gatesRepository = gatesRepository;
            _serviceProvider = serviceProvider;
            _displayFacade = displayFacade;
            _deliveryCountryRepository = deliveryCountries;
            _forwardingAgenciesRepository = forwardingAgenciesRepository;
        }

        public RegistrationViewModel GetRegistrationViewModel()
        {
            return new RegistrationViewModel()
            {
                Deliver = true,
                Countries = _deliveryCountryRepository.GetAll(),
                DefaultDeliveryCountry = _generalSettingsRepository.GetGeneralSettings().DefaultDeliveryCountry,
                SelectedDeliveryCountry = _generalSettingsRepository.GetGeneralSettings().DefaultDeliveryCountry,
                ForwardingAgencies = _forwardingAgenciesRepository.GetAll()
            };
        }

        public async Task AddRegistrationFromViewModel(RegistrationViewModel model)
        {
            _logger.LogInformation("AddRegistrationFromViewModel");
            if (model == null)
            {
                _logger.LogError("AddRegistrationFromViewModel: Model is null");
                throw new ArgumentNullException("Model is null");
            }

            var registration = CreateOpenRegistrationFromViewModel(model);
            var times = CreateRegistrationTimesFromRegistration(registration);

            await _openRegistrationsRepository.Add(registration, new List<OpenTimes> { times });

            await UpdateProcessingClients(registration);
        }

        private OpenRegistration CreateOpenRegistrationFromViewModel(RegistrationViewModel model)
        {
            var registration = new OpenRegistration
            {
                ID = Guid.NewGuid(),
                LicensePlate = model.LicensePlate,
                LicensePlateCompressed = CompressLicensePlate(model.LicensePlate),
                Transportnumber = model.Transportnumber,
                Comment = model.Comment,
                DeliveryCountry = model.SelectedDeliveryCountry
            };

            if (!model.ForwardingAgencyUnknown)
                registration.ForwardingAgency = model.ForwardingAgency;

            if (model.Deliver)
            {
                if (model.PickUp)
                    registration.JobType = EJobType.DeliverAndPickUp;
                else
                    registration.JobType = EJobType.Deliver;
            }
            else if (model.PickUp)
            {
                registration.JobType = EJobType.PickUp;
            }


            if (string.IsNullOrWhiteSpace(registration.Comment))
                registration.Comment = string.Empty;

            return registration;
        }

        private OpenTimes CreateRegistrationTimesFromRegistration(OpenRegistration registration)
        {
            return new OpenTimes
            {
                TimeOfRegistration = DateTime.Now,
                IsLastEdited = true,
                Gate = GetGateForForwardingAgency(registration.ForwardingAgency, registration.JobType),
                RegistratoinInformationID = registration.ID
            };
        }

        private async Task UpdateProcessingClients(OpenRegistration registration)
        {
            var processingRegistrationModel = new ProcessingRegistrationViewModel
            {
                Registration = registration,
                Times = _openRegistrationsRepository.GetRegistrationTimesFromRegistration(registration.ID),
                Gates = _gatesRepository.GetAll(),
                DeliveryCountries = _deliveryCountryRepository.GetAll()
            };

            if (registration.JobType == EJobType.DeliverAndPickUp)
                await _hubContext.Clients.All.SendAsync("AddRegistration", processingRegistrationModel);
            else if (registration.JobType == EJobType.Deliver)
            {
                await _hubContext.Clients.Groups("Deliver").SendAsync("AddRegistration", processingRegistrationModel);
                await _hubContext.Clients.Groups("DeliverAndPickUp").SendAsync("AddRegistration", processingRegistrationModel);
            }
            else if (registration.JobType == EJobType.PickUp)
            {
                await _hubContext.Clients.Groups("PickUp").SendAsync("AddRegistration", processingRegistrationModel);
                await _hubContext.Clients.Groups("DeliverAndPickUp").SendAsync("AddRegistration", processingRegistrationModel);
            }
        }

        public string CompressLicensePlate(string licensePlate)
        {
            if (!String.IsNullOrEmpty(licensePlate))
            {
                licensePlate = licensePlate.ToUpper();
                licensePlate = licensePlate.Replace(" ", String.Empty);
                var compressed = Regex.Replace(licensePlate, "[^0-9{a-zA-Z}]", "", RegexOptions.None);
                return compressed;
            }
            return "";
        }

        private string GetGateForForwardingAgency(string name, EJobType jobType)
        {
            // if the name is null or empty return the default gate.
            if (String.IsNullOrEmpty(name))
            {
                return GetDefaultGate(jobType);
            }
            else
            {
                var forwardingAgency = _agenciesRepository.Get(name);
                if (forwardingAgency == null)
                {
#pragma warning disable CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
                    AddUnknownForwardingAgency(name);
#pragma warning restore CS4014 // Da dieser Aufruf nicht abgewartet wird, wird die Ausführung der aktuellen Methode fortgesetzt, bevor der Aufruf abgeschlossen ist
                    return GetDefaultGate(jobType);
                }
                else
                {
                    if (jobType == EJobType.DeliverAndPickUp)
                    {
                        if (String.IsNullOrEmpty(forwardingAgency.DeliveryAndPickUpGate))
                        {
                            if (String.IsNullOrEmpty(forwardingAgency.DeliveryGate))
                                return GetDefaultGate(jobType);
                            else
                                return forwardingAgency.DeliveryGate;
                        }
                        return forwardingAgency.DeliveryAndPickUpGate;
                    }
                    if (jobType == EJobType.Deliver)
                    {
                        if (String.IsNullOrEmpty(forwardingAgency.DeliveryGate))
                            return GetDefaultGate(jobType);
                        return forwardingAgency.DeliveryGate;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(forwardingAgency.DeliveryGate))
                            return GetDefaultGate(jobType);
                        return forwardingAgency.PickUpGate;
                    }
                }
            }
        }

        private async Task AddUnknownForwardingAgency(string name)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var _unkownRepository = scope.ServiceProvider.GetRequiredService<IUnknownForwardingAgenciesRepository>();
                await _unkownRepository.Add(name);
            }
        }

        private string GetDefaultGate(EJobType jobType)
        {
            var generalSettings = _generalSettingsRepository.GetGeneralSettings();
            if (generalSettings == null)
                return "";

            if (jobType == EJobType.Deliver || jobType == EJobType.DeliverAndPickUp)
            {
                return generalSettings.DefaultDeliveryGate;
            }
            else
            {
                return generalSettings.DefaultPickUpGate;
            }
        }
    }
}
