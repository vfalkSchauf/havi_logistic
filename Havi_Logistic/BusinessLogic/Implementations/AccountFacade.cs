﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Repositories.Interfaces;

namespace MVC.BusinessLogic.Implementations
{
    public class AccountFacade : IAccountFacade
    {
        private readonly ILogger<AccountFacade> _logger;
        private readonly IADSettingsRepository _settingsRepository;

        public AccountFacade(ILogger<AccountFacade> logger, IADSettingsRepository settingsRepository)
        {
            _logger = logger;
            _settingsRepository = settingsRepository;
        }

        public bool UseAD
        {
            get
            {
                var settings = _settingsRepository.Get();
                if (settings != null)
                    return settings.UseAD;
                _logger.LogWarning("Error loading settings while trying to check for UseAD");
                return false;
            }
        }

        public bool GenerateAccountsForNewADUsers
        {
            get
            {
                var settings = _settingsRepository.Get();
                if (settings != null)
                    return settings.GenerateAccountsForNewADUsers;
                _logger.LogWarning("Error loading settings while trying to check for GenerateAccountsForNewADUsers");
                return false;
            }
        }

    }
}
