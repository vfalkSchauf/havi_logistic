﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace MVC.BusinessLogic.Implementations
{
    public class RegistrationHubFacade : IRegistrationHubFacade
    {
        private readonly ILogger<RegistrationHubFacade> _logger;

        private readonly IForwardingAgenciesRepository _forwardingAgenciesRepository;

        public RegistrationHubFacade(ILogger<RegistrationHubFacade> logger, IForwardingAgenciesRepository forwardingAgenciesRepository)
        {
            _logger = logger;
            _forwardingAgenciesRepository = forwardingAgenciesRepository;
        }

        public List<string> GetForwardingAgenciesWithName(string input)
        {
            var all = (from a in _forwardingAgenciesRepository.GetAll()
                       where a.Name.ToUpper().Contains(input.ToUpper())
                       select a.Name).ToList();

            return all;
        }
    }
}
