﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using System;
using System.Linq;

namespace MVC.BusinessLogic.Implementations
{
    public class ExportFacade : IExportFacade
    {
        private readonly ILogger<ExportFacade> _logger;

        private readonly ApplicationDBContext _context;

        public ExportFacade(ILogger<ExportFacade> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public string GenerateHistoryCSV(string userRole)
        {
            _logger.LogInformation("Generating history csv");
            try
            {
                var history = (from r in _context.RegistrationHistory
                               select r).ToList();
                var csv = "";
                var keys = "Licenseplate,ForwardingAgency,ERPID,Transportnumber,DeliveryCountry,TimeOfRegistration,Duration,Type,Gate,TimeOfCall,TimeOfEntry,TimeOfExit,Comment";
                csv += keys;
                foreach (var regist in history)
                {
                    if (regist.JobType == EJobType.Deliver && userRole == EJobType.PickUp.ToString() ||
                        regist.JobType == EJobType.PickUp && userRole == EJobType.Deliver.ToString())
                        continue;

                    var times = _context.ClosedTimes.Where(x => x.RegistratoinInformationID == regist.ID)
                        .OrderByDescending(x => x.TimeOfExit)
                        .ThenByDescending(x => x.TimeOfEntry)
                        .ThenByDescending(x => x.TimeOfCall)
                        .ToList();

                    foreach (var time in times)
                    {
                        var row = "\n";

                        row += "\"" + regist.LicensePlate + "\"" + ",";
                        row += "\"" + regist.ForwardingAgency + "\"" + ",";
                        row += regist.ForwardingAgencyERPID + ",";
                        row += regist.Transportnumber + ",";
                        row += regist.DeliveryCountry + ",";
                        row += time.TimeOfRegistration + ",";
                        if (time.TimeOfExit != new DateTime() && time.TimeOfEntry != new DateTime())
                        {
                            row += (time.TimeOfExit - time.TimeOfEntry).TotalMinutes.ToString("0") + ",";
                        }
                        else
                        {
                            row += "0" + ",";
                        }

                        switch (regist.JobType)
                        {
                            case Data.Entities.EJobType.Deliver:
                                row += "An" + ",";
                                break;
                            case Data.Entities.EJobType.DeliverAndPickUp:
                                row += "An & Ab" + ",";
                                break;
                            case Data.Entities.EJobType.PickUp:
                                row += "Ab" + ",";
                                break;
                        }

                        row += time.Gate + ",";
                        row += time.TimeOfCall + ",";
                        row += time.TimeOfEntry + ",";
                        row += time.TimeOfExit + ",";
                        row += regist.Comment;

                        csv += row;
                    }
                }
                return csv;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while generating history csv. Message: " + e.Message);
                throw;
            }
        }

        public string GenerateUnknownForwardingAgenciesCSVData()
        {
            _logger.LogInformation("Generating unknown forwarding agencies csv data");
            try
            {
                var agencies = (from a in _context.UnkownForwardingAgencies
                                select a).ToList();

                var csvData = "Name,FirstAppereance,NumberOfAppereances";
                foreach (var a in agencies)
                {
                    csvData += "\n";
                    csvData += a.Name;
                    csvData += ",";
                    csvData += a.FirstAppereance.ToString();
                    csvData += ",";
                    csvData += a.NumberOfAppereances.ToString();
                }
                return csvData;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to generate csv data. Message: " + e.Message);
                throw new Exception("Es ist ein Fehler bei der Erzeugung der csv Daten aufgetreten. Fehler: " + e.Message);
            }
        }

        public string GenerateUnknownForwardingAgenciesXMLData()
        {
            _logger.LogInformation("Generating unknown forwarding agencies xml data");
            try
            {
                var agencies = (from a in _context.UnkownForwardingAgencies
                                select a).ToList();

                var xmlData = "<UnknownForwardingAgencies>";

                foreach (var a in agencies)
                {
                    xmlData += "\n";
                    xmlData += "<UnknownForwardingAgency>";
                    xmlData += "\n<Name>" + a.Name + "</Name>";
                    xmlData += "\n<FirstAppereance>" + a.FirstAppereance + "</FirstAppereance>";
                    xmlData += "\n<NumberOfAppereances>" + a.NumberOfAppereances + "</NumberOfAppereances>";
                    xmlData += "\n</UnknownForwardingAgency>";
                }

                xmlData += "\n</UnknownForwardingAgencies>";
                return xmlData;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to generate xml data. Message: " + e.Message);
                throw new Exception("Es ist ein Fehler bei der Erzeugung der xml Daten aufgetreten. Fehler: " + e.Message);
            }

        }
    }
}
