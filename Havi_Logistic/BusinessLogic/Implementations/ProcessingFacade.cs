﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Models;
using MVC.Repositories.Interfaces;

namespace MVC.BusinessLogic.Implementations
{
    public class ProcessingFacade : IProcessingFacade
    {
        private ILogger<ProcessingFacade> _logger;

        private IGatesRepository _gatesRepository;
        private IOpenRegistrationsRepository _openRegistrationsRepository;
        private IDeliveryCountriesRepository _deliveryCountriesRepository;

        public ProcessingFacade(ILogger<ProcessingFacade> logger, IGatesRepository gatesRepository, IOpenRegistrationsRepository openRegistrationsRepository, IDeliveryCountriesRepository deliveryCountriesRepository)
        {
            _logger = logger;
            _gatesRepository = gatesRepository;
            _openRegistrationsRepository = openRegistrationsRepository;
            _deliveryCountriesRepository = deliveryCountriesRepository;
        }

        public ProcessingViewModel GetProcessingViewModel()
        {
            var gates = _gatesRepository.GetAll();
            var regists = _openRegistrationsRepository.GetAll();
            var times = _openRegistrationsRepository.GetAllRegistrationTimes(regists);

            var model = new ProcessingViewModel()
            {
                Gates = gates,
                Registrations = regists,
                Times = times,
            };
            return model;
        }
    }
}
