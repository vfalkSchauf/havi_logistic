﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;

namespace MVC.BusinessLogic.Implementations
{
    public class ADReader : IADReader
    {
        private readonly ILogger<ADReader> _logger;
        private readonly IADSettingsRepository _adSettingsRepository;

        private string _serverAddress;
        private string _domainNames;
        private string _domainUserName;
        private string _domainUserPass;


        public ADReader(ILogger<ADReader> logger, IADSettingsRepository settingsRepository)
        {
            _logger = logger;
            _adSettingsRepository = settingsRepository;
        }

        public List<string> GetADGroups(string userName)
        {
            List<string> groups = new List<string>();
            try
            {
                using (var ctx = CreatePrincipalContext())
                {
                    if (ctx == null)
                    {
                        _logger.LogWarning("Could not create PrincipalContext! Check if the server info is correct and make sure that it is an ActiveDirectory server!");
                        return null;
                    }
                    // find a user
                    UserPrincipal user = UserPrincipal.FindByIdentity(ctx, userName);

                    if (user != null)
                    {
                        // get the authorization groups - those are the "roles" 
                        var g = user.GetAuthorizationGroups();

                        foreach (Principal principal in g)
                        {
                            groups.Add(principal.Name);
                        }
                    }
                }
                return groups;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not get list of groups" + " error: " + e.Message);
                throw e;
            }
        }

        private PrincipalContext CreatePrincipalContext()
        {
            try
            {
                LoadSettings();
                var principalContext = new PrincipalContext(ContextType.Domain, _serverAddress, _domainNames, _domainUserName, _domainUserPass);
                return principalContext;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not create PrincipalContext! Check if the server info is correct and make sure that it is an ActiveDirectory server!" + e.Message);
                throw e;
            }

        }

        private void LoadSettings()
        {
            try
            {
                var settings = _adSettingsRepository.Get();
                _serverAddress = settings.ServerIP;
                _domainNames = settings.DomainNames;
                _domainUserName = settings.DomainUserName;
                _domainUserPass = settings.DomainUserPassword;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to load ad Settings. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                throw e;
            }
        }

        public bool CheckADLogin(string userName, string password)
        {
            try
            {
                using (var ctx = CreatePrincipalContext())
                {
                    if (ctx == null)
                    {
                        _logger.LogWarning("Could not create PrincipalContext! Check if the server info is correct and make sure that it is an ActiveDirectory server!");
                        return false;
                    }
                    return ctx.ValidateCredentials(userName, password);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not validate Credentials." + " error: " + e.Message);
                throw e;
            }
        }
    }
}
