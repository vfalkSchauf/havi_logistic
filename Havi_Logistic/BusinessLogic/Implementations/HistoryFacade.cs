﻿using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Models;
using MVC.Repositories.Interfaces;
using System;
using System.IO;

namespace MVC.BusinessLogic.Implementations
{
    public class HistoryFacade : IHistoryFacade
    {
        private readonly ILogger<HistoryFacade> _logger;
        private readonly IClosedRegistrationsRepository _closedRegistrationsRepository;
        private readonly IExportFacade _exportFacade;
        private readonly IDeliveryCountriesRepository _deliveryCountriesRepository;

        public HistoryFacade(ILogger<HistoryFacade> logger, IClosedRegistrationsRepository closedRegistrationsRepository, IDeliveryCountriesRepository deliveryCountriesRepository,IExportFacade exportFacade)
        {
            _logger = logger;
            _closedRegistrationsRepository = closedRegistrationsRepository;
            _deliveryCountriesRepository = deliveryCountriesRepository;
            _exportFacade = exportFacade;
        }

        public FileStream ExportHistoryCSV(string userRole)
        {
            try
            {
                Directory.CreateDirectory("DataExport");
                StreamWriter writer = new StreamWriter("DataExport/GesamteHistorieExport.csv");
                var data = _exportFacade.GenerateHistoryCSV(userRole);
                writer.Write(data);
                writer.Flush();
                writer.Close();
                return new FileStream("DataExport/GesamteHistorieExport.csv", FileMode.Open);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not Export UnkownSuppliersCSV. Error while creating Filestream");
                throw e;
            }
        }

        public HistoryViewModel GetHistoryViewModel()
        {
            var model = new HistoryViewModel
            {
                Registrations = _closedRegistrationsRepository.GetAll(),
            };
                model.Times = _closedRegistrationsRepository.GetAllTimes(model.Registrations);
            return model;
        }
    }
}
