﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Models.ConfigurationViewModels;
using MVC.Models.ViewModelSelectHelper;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Implementations
{
    public class ConfigurationFacade : IConfigurationFacade
    {
        private readonly ILogger<ConfigurationFacade> _logger;

        private readonly IGeneralSettingsRepository _generalSettingsRepository;
        private readonly IGatesRepository _gatesRepository;
        private readonly IForwardingAgenciesRepository _forwardingAgenciesRepository;
        private readonly IUnknownForwardingAgenciesRepository _unknownForwardingAgenciesRepository;
        private readonly IDeliveryCountriesRepository _deliveryCountriesRepository;
        private readonly IDisplayConfigurationRepository _displayConfigurationRepository;
        private readonly IADSettingsRepository _adSettingsRepository;
        private readonly ICsvReader _csvReader;
        private readonly IExportFacade _exportFacade;
        private readonly IAuthorizationGroupsRepository _authorizationGroupsRepository;
        private readonly UserManager<AppUser> _userManager;

        public ConfigurationFacade(ILogger<ConfigurationFacade> logger, IGeneralSettingsRepository generalSettingsRepository, IGatesRepository gatesRepository,
                                        IForwardingAgenciesRepository forwardingAgenciesRepository, IUnknownForwardingAgenciesRepository unknownForwardingAgenciesRepository,
                                        IDisplayConfigurationRepository displayConfigurationRepository, ICsvReader csvReader, IExportFacade exportFacade,
                                        UserManager<AppUser> userManager, IAuthorizationGroupsRepository authorizationGroupsRepository,
                                        IADSettingsRepository adSettingsRepository, IDeliveryCountriesRepository deliveryCountriesRepository)
        {
            _logger = logger;
            _generalSettingsRepository = generalSettingsRepository;
            _gatesRepository = gatesRepository;
            _forwardingAgenciesRepository = forwardingAgenciesRepository;
            _unknownForwardingAgenciesRepository = unknownForwardingAgenciesRepository;
            _displayConfigurationRepository = displayConfigurationRepository;
            _csvReader = csvReader;
            _exportFacade = exportFacade;
            _userManager = userManager;
            _authorizationGroupsRepository = authorizationGroupsRepository;
            _adSettingsRepository = adSettingsRepository;
            _deliveryCountriesRepository = deliveryCountriesRepository;
        }

        #region Gates
        public async Task AddGate(Gate gate)
        {
            await _gatesRepository.Add(gate);
        }

        public async Task DeleteGate(Guid id)
        {
            Gate gate = _gatesRepository.Get(id);
            await _gatesRepository.Delete(id);
            await _generalSettingsRepository.RemoveDefaultGateIfItWasSelected(gate.Name);
        }

        public Gate GetGate(Guid id)
        {
            return _gatesRepository.Get(id);
        }

        public List<Gate> GetAllGates()
        {
            return _gatesRepository.GetAll();
        }

        public ConfigurationGatesViewModel GetGatesViewModel()
        {
            var model = new ConfigurationGatesViewModel();
            var generalSettings = _generalSettingsRepository.GetGeneralSettings();
            if (generalSettings != null)
            {
                model.DefaultDeliveryGate = generalSettings.DefaultDeliveryGate;
                model.DefaultPickUpGate = generalSettings.DefaultPickUpGate;
            }
            model.Gates = _gatesRepository.GetAll();
            return model;
        }

        public async Task SetDefaultGates(ConfigurationGatesViewModel model)
        {
            if (model == null)
            {
                _logger.LogError("Cannot set default gates. model = null");
                return;
            }
            model.DefaultDeliveryGate = model.DefaultDeliveryGate == null ? "" : model.DefaultDeliveryGate;
            model.DefaultPickUpGate = model.DefaultPickUpGate == null ? "" : model.DefaultPickUpGate;

            await _generalSettingsRepository.SetDefaultGates(model);
        }

        public async Task EditGate(Gate gate)
        {
            await _gatesRepository.Set(gate);
        }

        public async Task ImportGatesFromCSV(IFormFile file)
        {
            if (file == null)
                throw new Exception("Es wurde keine Datei ausgewählt, oder die Datei ist leer.");

            _csvReader.Read(file);
            string[] requiredKeys = { "NAME" };
            if (_csvReader.IsValid && _csvReader.ContainsKeys(requiredKeys))
            {
                var keys = _csvReader.Keys;
                var values = _csvReader.Values;

                List<Gate> gates = new List<Gate>();

                for (int i = 0; i < values.Length; i++)
                {
                    Gate gate = new Gate();
                    for (int j = 0; j < keys.Length; j++)
                    {
                        switch (keys[j].ToUpper())
                        {
                            case "NAME":
                                gate.Name = values[i][j];
                                break;
                            case "DIRECTION":
                                gate.GateDirection = (EDirectionArrows)Enum.Parse(typeof(EDirectionArrows), values[i][j]);
                                break;
                            case "GATETYP":
                                gate.GateType = (EJobType)Enum.Parse(typeof(EJobType), values[i][j]);
                                break;
                            case "DESCRIPTION":
                                gate.Description = values[i][j];
                                break;
                            case "USERELAYATCALL":
                                gate.UseRelayAtCall = bool.Parse(values[i][j]);
                                break;
                        }
                    }
                    gates.Add(gate);
                }

                try
                {
                    await _gatesRepository.Import(gates);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                _logger.LogError("Could not import Gates from CSV. File was invalid or did not contain the required keys.");
                throw new Exception("Datei konnte nicht importiert werden. Stellen Sie sicher, dass sie der Beschreibung entspricht.");
            }
        }



        public GateViewModel GetGateViewModel()
        {
            try
            {
                GateViewModel model = new GateViewModel()
                {
                    Gate = new Gate()
                };

                List<UseRelayItem> useRelayItems = new List<UseRelayItem>() { new UseRelayItem("Nein", false), new UseRelayItem("Ja", true) };

                model.UseRelaySelect = new SelectList(useRelayItems, "Value", "Key");

                return model;

            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Could not GetGateViewModel. Exeption: {0}", ex.Message));
                throw ex;
            }
        }

        public GateViewModel GetGateViewModel(Guid id)
        {
            try
            {
                GateViewModel model = new GateViewModel()
                {
                    Gate = _gatesRepository.Get(id)
                };

                List<UseRelayItem> useRelayItems = new List<UseRelayItem>() { new UseRelayItem("Nein", false), new UseRelayItem("Ja", true) };

                model.UseRelaySelect = new SelectList(useRelayItems, "Value", "Key", model.Gate.UseRelayAtCall.ToString());

                return model;

            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Could not GetGateViewModel. Exeption: {0}", ex.Message));
                throw ex;
            }
        }
        #endregion

        #region ForwardingAgencies
        public ForwardingAgenciesIndexViewModel GetForwardingAgenciesIndexViewModel()
        {
            var model = new ForwardingAgenciesIndexViewModel();
            model.ForwardingAgencies = _forwardingAgenciesRepository.GetAll();
            return model;
        }

        public ForwardingAgencyViewModel GetForwardingAgencyViewModel()
        {
            var model = new ForwardingAgencyViewModel();
            model.ForwardingAgency = new ForwardingAgency();
            model.Gates = _gatesRepository.GetAll();
            return model;
        }

        public ForwardingAgencyViewModel GetForwardingAgencyViewModel(Guid id)
        {
            var model = new ForwardingAgencyViewModel();
            model.ForwardingAgency = _forwardingAgenciesRepository.Get(id);
            model.Gates = _gatesRepository.GetAll();
            return model;
        }

        public async Task AddForwardingAgency(ForwardingAgencyViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("ForwardingAgencyViewModel == null");
            }
            if (model.ForwardingAgency == null)
            {
                throw new ArgumentNullException("ForwardingAgencyViewModel.ForwardingAgency == null");
            }
            if (model.ForwardingAgency.DeliveryGate == null)
                model.ForwardingAgency.DeliveryGate = "";
            if (model.ForwardingAgency.PickUpGate == null)
                model.ForwardingAgency.PickUpGate = "";
            if (model.ForwardingAgency.DeliveryAndPickUpGate == null)
                model.ForwardingAgency.DeliveryAndPickUpGate = "";

            await _forwardingAgenciesRepository.Add(model.ForwardingAgency);
        }

        public async Task EditForwardingAgency(ForwardingAgencyViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("ForwardingAgencyViewModel == null");
            }
            if (model.ForwardingAgency == null)
            {
                throw new ArgumentNullException("ForwardingAgencyViewModel.ForwardingAgency == null");
            }
            if (model.ForwardingAgency.DeliveryGate == null)
                model.ForwardingAgency.DeliveryGate = "";
            if (model.ForwardingAgency.PickUpGate == null)
                model.ForwardingAgency.PickUpGate = "";
            if (model.ForwardingAgency.DeliveryAndPickUpGate == null)
                model.ForwardingAgency.DeliveryAndPickUpGate = "";

            await _forwardingAgenciesRepository.Edit(model.ForwardingAgency);
        }

        public async Task DeleteForwardingAgency(Guid id)
        {
            await _forwardingAgenciesRepository.Delete(id);
        }

        public async Task ImportForwardingAgenciesFromCSV(IFormFile file)
        {
            if (file == null)
                throw new Exception("Es wurde keine Datei ausgewählt, oder die Datei ist leer.");

            _csvReader.Read(file);
            string[] requiredKeys = { "Name" };
            if (_csvReader.IsValid && _csvReader.ContainsKeys(requiredKeys))
            {
                var keys = _csvReader.Keys;
                var values = _csvReader.Values;

                List<ForwardingAgency> agencies = new List<ForwardingAgency>();

                for (int i = 0; i < values.Length; i++)
                {
                    ForwardingAgency agency = new ForwardingAgency();
                    try
                    {
                        for (int j = 0; j < keys.Length; j++)
                        {
                            switch (keys[j].ToUpper())
                            {
                                case "NAME":
                                    agency.Name = values[i][j];
                                    break;
                                case "DELIVERYGATE":
                                    agency.DeliveryGate = values[i][j];
                                    break;
                                case "PICKUPGATE":
                                    agency.PickUpGate = values[i][j];
                                    break;
                                case "ERPID":
                                    agency.ERP_ID = int.Parse(values[i][j]);
                                    break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Could not parse csv values into forwarding agency values. Message: " + e.Message);
                        throw new Exception("Daten konnten nicht importiert werden. Die Datentypen stimmen nicht.");
                    }
                    agencies.Add(agency);
                }

                try
                {
                    await _forwardingAgenciesRepository.Import(agencies);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                _logger.LogError("Could not import Gates from CSV. File was invalid or did not contain the required keys.");
                throw new Exception("Datei konnte nicht importiert werden. Stellen Sie sicher, dass sie der Beschreibung entspricht.");
            }
        }
        #endregion

        #region UnknownForwardingAgencies
        public UnknownForwardingAgenciesViewModel GetUnknownForwardingAgenciesViewModel()
        {
            var model = new UnknownForwardingAgenciesViewModel();
            model.UnknownForwardingAgencies = _unknownForwardingAgenciesRepository.GetAll();
            return model;
        }

        public FileStream ExportUnknownForwardingAgenciesCSVStream()
        {
            try
            {
                Directory.CreateDirectory("DataExport");
                StreamWriter writer = new StreamWriter("DataExport/UnknownForwardingAgencies.csv");
                var data = _exportFacade.GenerateUnknownForwardingAgenciesCSVData();
                writer.Write(data);
                writer.Flush();
                writer.Close();
                return new FileStream("DataExport/UnknownForwardingAgencies.csv", FileMode.Open);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not Export UnknownForwardingAgencies. Error while creating Filestream");
                throw e;
            }
        }

        public FileStream ExportUnknownForwardingAgenciesXMLStream()
        {
            try
            {
                Directory.CreateDirectory("DataExport");
                StreamWriter writer = new StreamWriter("DataExport/UnknownForwardingAgencies.xml");
                var data = _exportFacade.GenerateUnknownForwardingAgenciesXMLData();
                writer.Write(data);
                writer.Flush();
                writer.Close();
                return new FileStream("DataExport/UnknownForwardingAgencies.xml", FileMode.Open);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not export UnknownForwardingAgencies. Error while creating the filestream.");
                throw e;
            }
        }

        public async Task AddUnknownForwardingAgencieToKnownForwardingAgencies(Guid id)
        {
            try
            {
                var unkownForwardingAgency = _unknownForwardingAgenciesRepository.Get(id);
                var addedSuccsessfully = await Task.Run(() => _forwardingAgenciesRepository.AddForwardingAgencyFromUnkownForwardingAgency(unkownForwardingAgency));

                if (addedSuccsessfully)
                    await _unknownForwardingAgenciesRepository.Remove(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not create ForwardingAgancy from UnkownForwardingAgancy");
                throw e;
            }
        }
        #endregion

        #region DeliveryCountries
        public async Task AddDeliveryCountry(DeliveryCountry DeliveryCountry)
        {
            await _deliveryCountriesRepository.Add(DeliveryCountry);
        }

        public async Task DeleteDeliveryCountry(Guid id)
        {
            await _deliveryCountriesRepository.Delete(id);
        }

        public DeliveryCountry GetDeliveryCountry(Guid id)
        {
            return _deliveryCountriesRepository.Get(id);
        }

        public List<DeliveryCountry> GetAllDeliveryCountry()
        {
            return _deliveryCountriesRepository.GetAll();
        }

        public DeliveryCountriesViewModel GetDeliveryCountriesViewModel()
        {
            var model = new DeliveryCountriesViewModel
            {
                DeliveryCountries = _deliveryCountriesRepository.GetAll(),
                DefaultDeliveryCountry = _generalSettingsRepository.GetGeneralSettings().DefaultDeliveryCountry
            };

            return model;
        }

        public async Task EditDeliveryCountry(DeliveryCountry DeliveryCountry)
        {
            await _deliveryCountriesRepository.Set(DeliveryCountry);
        }

        public async Task SetDefaultDeliveryCountry(DeliveryCountriesViewModel model)
        {
            if (model == null)
            {
                _logger.LogError("Cannot set default DeliveryCountry. model = null");
                return;
            }
            model.DefaultDeliveryCountry = model.DefaultDeliveryCountry == null ? "" : model.DefaultDeliveryCountry;

            await _generalSettingsRepository.SetDefaultDeliveryCountry(model);
        }
        #endregion

        #region Displays

        public DisplayIndexViewModel GetDisplayIndexViewModel()
        {
            var model = new DisplayIndexViewModel();
            model.Displays = _displayConfigurationRepository.GetAll();
            return model;
        }

        public async Task AddDisplay(DisplayConfiguration displayConfig)
        {
            await _displayConfigurationRepository.Add(displayConfig);
        }

        public async Task EditDisplay(DisplayConfiguration displayConfiguration)
        {
            await _displayConfigurationRepository.Edit(displayConfiguration);
        }

        public async Task DeleteDisplay(Guid id)
        {
            await _displayConfigurationRepository.Delete(id);
        }

        public DisplayConfiguration GetDisplayConfiguration(Guid id)
        {
            return _displayConfigurationRepository.Get(id);
        }

        #endregion

        #region Users

        public UserIndexViewModel GetUserIndexViewModel()
        {
            return new UserIndexViewModel() { Users = _userManager.Users.ToList() };
        }

        public async Task<IdentityResult> AddUser(UserViewModel model)
        {
            AppUser user = new AppUser
            {
                UserName = model.UserName,
                Email = model.Email,
                AuthorizationGroup = model.Group,

            };
            IdentityResult result = null;
            try
            {
                result = await _userManager.CreateAsync(user, model.Password);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying _userManager.CreateAsync. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                throw new Exception("Fehler beim Erstellen des Benutzers.");
            }
            if (result.Succeeded)
                await SetUserRoles(user);
            return result;
        }

        public async Task<IdentityResult> DeleteUser(string id)
        {
            AppUser user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                var error = new IdentityError();
                error.Description = "Benutzer nicht gefunden!";
                var result = IdentityResult.Failed(error);
                return result;
            }
            return await _userManager.DeleteAsync(user);
        }

        public async Task<UserViewModel> GetUserViewModel(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                return null;
            var model = new UserViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                Id = user.Id,
                Groups = AuthorizationGroups(),
                Group = user.AuthorizationGroup
            };
            return model;
        }

        public UserViewModel GetUserViewModel()
        {
            var model = new UserViewModel
            {
                Groups = AuthorizationGroups()
            };
            return model;
        }

        public List<string> AuthorizationGroups()
        {
            var all = _authorizationGroupsRepository.GetAll();
            var list = new List<string>();

            foreach (var g in all)
            {
                list.Add(g.Name);
            }

            return list;
        }

        public async Task<IdentityResult> EditUser(UserViewModel model, IUserValidator<AppUser> _userValidator, IPasswordHasher<AppUser> _passwordHasher, IPasswordValidator<AppUser> _passwordValidator)
        {
            var user = await _userManager.FindByIdAsync(model.Id);
            if (user != null)
            {
                user.Email = model.Email;
                user.UserName = model.UserName;
                user.AuthorizationGroup = model.Group;

                IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                if (!validEmail.Succeeded)
                {
                    return validEmail;
                }

                if (!String.IsNullOrEmpty(model.Password))
                {
                    var validPass = await _passwordValidator.ValidateAsync(_userManager, user, model.Password);
                    if (validPass.Succeeded)
                    {
                        user.PasswordHash = _passwordHasher.HashPassword(user, model.Password);
                    }
                    else
                    {
                        return validPass;
                    }
                }
                await SetUserRoles(user);
                return await _userManager.UpdateAsync(user);
            }
            else
            {
                var error = new IdentityError();
                error.Description = "Benutzer wurde nicht gefunden!";
                return IdentityResult.Failed(error);
            }
        }

        private async Task SetUserRoles(AppUser user)
        {
            try
            {
                var roles = await _userManager.GetRolesAsync(user);
                IdentityResult resultRemove = null;
                if (String.IsNullOrEmpty(user.AuthorizationGroup))
                {
                    resultRemove = await _userManager.RemoveFromRolesAsync(user, roles);
                    return;
                }

                var group = _authorizationGroupsRepository.Get(user.AuthorizationGroup);
                List<string> addRoles = new List<string>();

                if (group.CanUseRegistration)
                    addRoles.Add("Registration");

                if (group.CanUseConfig)
                    addRoles.Add("Configuration");

                if (group.CanUseHistory)
                    addRoles.Add("History");

                if (group.CanUseProcessing)
                    addRoles.Add("Processing");

                if (group.SetEntrance)
                {
                    addRoles.Add("Entry");
                    addRoles.Add("WithdrawEntry");
                }

                if (group.AllowDeliver && group.AllowPickup)
                    addRoles.Add("DeliverAndPickUp");
                else if (group.AllowPickup)
                    addRoles.Add("PickUp");
                else if (group.AllowDeliver)
                    addRoles.Add("Deliver");

                if (group.SetExit)
                    addRoles.Add("Exit");

                if (group.SetGate)
                    addRoles.Add("Gate");

                if (group.AllowEntrance)
                {
                    addRoles.Add("Call");
                    addRoles.Add("WithdrawCall");
                }

                if (group.Export)
                    addRoles.Add("Export");

                resultRemove = await _userManager.RemoveFromRolesAsync(user, roles);
                var resultAdd = await _userManager.AddToRolesAsync(user, addRoles);

            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to set userRoles. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                throw new Exception("Fehler beim setzten der Benutzerrollen.");
            }
        }

        #endregion

        #region AuthorizationGroups

        public AuthorizationGroupsViewModel GetAuthorizationGroupsViewModel()
        {
            return new AuthorizationGroupsViewModel { Groups = _authorizationGroupsRepository.GetAll() };
        }

        public async Task AddAuthorizationGroup(GroupViewModel model)
        {
            await _authorizationGroupsRepository.Add(model.Group);
        }

        public async Task EditAuthorizationGroup(GroupViewModel model)
        {
            var oldVersion = _authorizationGroupsRepository.Get(model.Group.ID);
            if (oldVersion == null)
            {
                _logger.LogInformation("Could not edit Authorization group because the premodified version could not be found.");
                throw new Exception("Fehler beim Bearbeiten der Berechtigungsgruppe.");
            }
            var users = (from u in _userManager.Users
                         where u.AuthorizationGroup == oldVersion.Name
                         select u).ToList();

            await _authorizationGroupsRepository.Edit(model.Group);

            foreach (var user in users)
            {
                user.AuthorizationGroup = model.Group.Name;
                await SetUserRoles(user);
                await _userManager.UpdateAsync(user);
            }
        }

        public async Task DeleteAuthorizationGroup(Guid id)
        {
            var oldVersion = _authorizationGroupsRepository.Get(id);
            if (oldVersion == null)
            {
                _logger.LogInformation("Could not delete Authorization group because the group could not be found.");
                throw new Exception("Fehler beim Löschen der Berechtigungsgruppe.");
            }
            var users = (from u in _userManager.Users
                         where u.AuthorizationGroup == oldVersion.Name
                         select u).ToList();

            foreach (var user in users)
            {
                user.AuthorizationGroup = "";
                await SetUserRoles(user);
                await _userManager.UpdateAsync(user);
            }

            await _authorizationGroupsRepository.Delete(id);
        }

        public GroupViewModel GetGroupViewModel(Guid id)
        {
            return new GroupViewModel { Group = _authorizationGroupsRepository.Get(id) };
        }
        #endregion

        #region Active Directory

        public ADSettings GetADSettings()
        {
            return _adSettingsRepository.Get();
        }

        public async Task SetADSettings(ADSettings settings)
        {
            await _adSettingsRepository.Set(settings);
        }


        #endregion
    }
}
