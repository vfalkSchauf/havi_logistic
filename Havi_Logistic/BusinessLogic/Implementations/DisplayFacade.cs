﻿
using KnausTabbert.Display.Escape;
using KnausTabbert.Display.GTX;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Display;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace MVC.BusinessLogic.Implementations
{
    public class DisplayFacade : IDisplayFacade
    {
        private readonly ILogger<DisplayFacade> _logger;

        private readonly IServiceProvider _serviceProvider;

        private OpenTimes _latestTime;

        public DisplayFacade(ILogger<DisplayFacade> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public OpenTimes Update(OpenTimes latestTime)
        {
            try
            {
                _logger.LogInformation("Updating displays.");

                _latestTime = latestTime;
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _displayRepository = scope.ServiceProvider.GetRequiredService<IDisplayConfigurationRepository>();
                    var displayConfigurations = _displayRepository.GetAll();

                    var _openRegistrationRepos = scope.ServiceProvider.GetRequiredService<IOpenRegistrationsRepository>();
                    var openRegistrations = _openRegistrationRepos.GetAll();

                    var times = _openRegistrationRepos.GetAllRegistrationTimes(openRegistrations);
                    var _gatesRepository = scope.ServiceProvider.GetRequiredService<IGatesRepository>();
                    var gates = _gatesRepository.GetAll();

                    var newLatestTime = getLatestTimeWhereCallWasSet(times, gates);

                    bool isTimeNew = false;

                    if (_latestTime == null && newLatestTime != null)
                        isTimeNew = true;
                    else if (_latestTime != null && newLatestTime != null)
                        isTimeNew = newLatestTime.ID != _latestTime.ID;

                    bool useRelay = (shouldRelayBeUsed(newLatestTime, gates)) && isTimeNew;

                    for (int i = 0; i < displayConfigurations.Count; i++)
                    {
                        Send(displayConfigurations[i], openRegistrations, times, gates, useRelay);
                    }

                    return newLatestTime;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Exception occured: {0} \nInnerException: {1}\nStacktrace: {2}", ex.Message, ex.InnerException, ex.StackTrace));
                throw null;
            }
        }

        private void Send(DisplayConfiguration configuration, List<OpenRegistration> openRegistrations, List<OpenTimes> times, List<Gate> gates, bool useRelay)
        {
            try
            {
                var timesToDisplay = GetRegistrationsTimesToDisplay(configuration.Rows, configuration.EntryRemovalType, times);
                var stringsToDisplay = GetStringsToDisplay(configuration.Rows, configuration.CharsPerLine, openRegistrations, timesToDisplay, gates);
                bool firstRegistIsCall = false;
                if (timesToDisplay.Count > 0)
                {
                    if (timesToDisplay[0].TimeOfCall != new DateTime())
                        firstRegistIsCall = true;
                }

                byte[] protocol = CreateProtocol(stringsToDisplay, 1, firstRegistIsCall).ToArray();



                if (configuration.RelayTime > 0 && useRelay)
                {
                    byte[] relayProtocol = CreateRelayProtocol(configuration.RelayTime).ToByteArray();
                    int protocolLength = protocol.Length;
                    Array.Resize<byte>(ref protocol, protocolLength + relayProtocol.Length);
                    Array.Copy(relayProtocol, 0, protocol, protocolLength, relayProtocol.Length);
                }

                IPAddress ip = IPAddress.Parse(configuration.IPAddress);
                Task.Run(() => Send(protocol, false, ip, configuration.Port, configuration.TcpTimeoutInMs, configuration.ModeBreakInMs));

            }
            catch (Exception e)
            {
                _logger.LogError("Exception while sending data to the displays. Message: " + e.Message);
            }
        }

        private byte[] GenerateEscapeData(List<OpenTimes> Times, int rows)
        {
            // each row there are 2 escape displays ( X and ->)
            byte[] data = new byte[2 * rows];

            for (int i = 0; i < rows; i++)
            {
                if (i < Times.Count)
                {

                    // if the current registration is allowed to enter, turn on the -> and turn off the x
                    if (Times.Where(x => x.IsLastEdited == true).FirstOrDefault()
                                        .TimeOfCall != new DateTime())
                    {
                        data[i * 2] = 0x30;
                        data[i * 2 + 1] = 0x31;
                    }
                    // if the current registration isnt allowed to enter, turn on the x and turn off the ->
                    else
                    {
                        data[i * 2] = 0x31;
                        data[i * 2 + 1] = 0x30;
                    }
                }
                // if the registration doesnt exist, turn off both the x and the ->
                else
                {
                    data[i * 2] = 0x30;
                    data[i * 2 + 1] = 0x30;
                }
            }
            return data;
        }

        /// <summary>
        /// Creates a Socket and Sends the data passed as param. if it is gtx it will check for ack
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isEsc"></param>
        /// <returns></returns>
        private void Send(byte[] data, bool isEsc, IPAddress ipAddress, int port, int timeout, int modeBreakInMS)
        {
            bool dataSent = false;

            IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);
            using (Socket socket = new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.SendTimeout = timeout;
                socket.ReceiveTimeout = timeout;

                try
                {
                    IAsyncResult result = socket.BeginConnect(ipEndPoint, null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(timeout, true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                // return false if the connection failed so that a reconnect will be tried
                if (!socket.Connected)
                {
                    _logger.LogError("Could not connect to display");
                    throw new Exception("Could not connect."); // return false;//
                }


                try
                {
                    socket.Send(data);
                }
                catch (Exception ex)
                {
                    if (socket.Connected)
                        socket.Disconnect(false);

                    throw ex;
                }

                Thread.Sleep(modeBreakInMS); // Wait after sending in any case (either to receive an answer or to be able to send next package)...

                // only wait for ack for gtx because esc doesnt send it
                if (!isEsc)
                {
                    try
                    {
                        byte[] receivedByteBuffer = new byte[6];
                        socket.Receive(receivedByteBuffer);

                        // 0x06 is the ack message
                        dataSent = receivedByteBuffer.Contains((byte)0x06);
                        // Logging
                    }
                    catch (Exception ex)
                    {
                        if (socket.Connected)
                            socket.Disconnect(false);

                        throw new Exception("Error while waiting for 'ACK' message. Is the display connected and configured to respond with an 'ACK' message? See inner exception for more details.", ex);
                    }
                }
                // make sure that true is returned when esc protocoll is sent because it doesnt send ack.
                else
                {
                    dataSent = true;
                }
                if (socket.Connected)
                    socket.Disconnect(false);
            }
        }

        private GtxProtocol CreateProtocol(List<DisplayLine> messageList, byte adress, bool firstRegistIsCall)
        {

            var protocol = new GtxProtocol(messageList, firstRegistIsCall, (byte)messageList.Count, adress, false, true, true, false);

            return protocol;
        }

        private OpenTimes getLatestTimeWhereCallWasSet(List<OpenTimes> times, List<Gate> gates)
        {
            if (!times.Any(x => x.IsLastEdited && x.TimeOfCall != new DateTime() && x.TimeOfEntry == new DateTime()))
                return null;

            OpenTimes latestTime = null;
            foreach (OpenTimes time in times.Where(x => x.IsLastEdited))
            {
                if (latestTime == null && time.TimeOfEntry == new DateTime())
                {
                    latestTime = time;
                    continue;
                }
                else if (latestTime == null)
                {
                    continue;
                }

                if (time.TimeOfCall > latestTime.TimeOfCall && time.TimeOfEntry == new DateTime())
                    latestTime = time;
            }

            return latestTime;
        }

        private bool shouldRelayBeUsed(OpenTimes time, List<Gate> gates)
        {
            if (time == null)
                return false;

            if (string.IsNullOrWhiteSpace(time.Gate))
                return false;

            if (gates.Count == 0)
                return false;

            if (!gates.Any(x => x.Name == time.Gate))
                return false;

            return gates.Where(x => x.Name == time.Gate).First().UseRelayAtCall;
        }

        private EscapeRelayProtocol CreateRelayProtocol(int relaytime)
        {
            return new EscapeRelayProtocol(relaytime);
        }

        private List<DisplayLine> GetStringsToDisplay(int rows, int charsPerLine, List<OpenRegistration> openRegistrations, List<OpenTimes> Times, List<Gate> gates)
        {
            var list = new List<DisplayLine>();

            foreach (var time in Times)
            {
                DisplayLine line = new DisplayLine("", false);

                string LicensePlate = openRegistrations.Where(x => x.ID == time.RegistratoinInformationID).FirstOrDefault().LicensePlate;

                if (LicensePlate.Length <= charsPerLine - 3)
                {
                    for (int i = 0; i < charsPerLine - 3; i++)
                    {
                        if (LicensePlate.Length > i)
                        {
                            line.Content += LicensePlate.ElementAt(i);
                        }
                        else
                        {
                            line.Content += " ";
                        }
                    }

                    line.Content += GetGatePartAsString(time, gates);


                }
                else
                {
                    line.Content = LicensePlate.Substring(0, charsPerLine - 3);

                    line.Content += GetGatePartAsString(time, gates);
                }

                if (time.TimeOfCall != new DateTime())
                    line.Flash = true;

                list.Add(line);
            }

            if (Times.Count < rows)
            {
                for (int i = Times.Count; i < rows - 1; i++)
                {
                    DisplayLine line = new DisplayLine("", false);
                    line.Content = line.Content.PadLeft(charsPerLine, ' ');
                    list.Add(line);
                }

                var date = string.Empty;
                date = string.Format("{0:dd.MM.yyyy HH:mm}", DateTime.Now);
                //date = string.Format("{0:HH:mm:ss}                 ", DateTime.Now);

                if (date.Length < charsPerLine)
                    date = date.PadLeft(charsPerLine, ' ');
                else if (date.Length > charsPerLine)
                    date = date.Substring(0, charsPerLine);

                list.Add(new DisplayLine(date, false));
            }

            return list;
        }

        private string GetGatePartAsString(OpenTimes time, List<Gate> gates)
        {
            string gatePart = string.Empty;

            gatePart += " ";
            if (time.TimeOfCall == new DateTime() || time.Gate.Length == 0)
            {
                gatePart += "  ";
            }
            else
            {
                if (time.Gate.Length >= 2 || time.Gate.Length == 2)
                    gatePart += time.Gate.Substring(0, 2);
                else
                    gatePart += " " + time.Gate;

                //gatePart += " ";

                //if (gates.Any(x => x.Name == time.Gate))
                //    gatePart += transformArrowToChar(gates.Where(x => x.Name == time.Gate).First().GateDirection);
                //else
                //    gatePart += " ";
            }

            return gatePart;
        }

        private string transformArrowToChar(EDirectionArrows gateDirection = EDirectionArrows.None)
        {
            switch (gateDirection)
            {
                case EDirectionArrows.Up:
                    return "↑";
                case EDirectionArrows.Right:
                    return "→";
                //case EDirectionArrows.Down:
                //    return "↓";
                case EDirectionArrows.Left:
                    return "←";
                default:
                    return " ";
            }
        }

        private List<OpenTimes> GetRegistrationsTimesToDisplay(int rows, EEntryRemovalType type, List<OpenTimes> times)
        {
            var list = new List<OpenTimes>();

            var registered = times.Where(x => x.TimeOfCall == new DateTime() && x.IsLastEdited == true)
                                            .OrderByDescending(x => x.TimeOfCall).ToList();

            var called = times.Where(x => x.TimeOfCall != new DateTime() && x.IsLastEdited == true)
                                            .OrderByDescending(x => x.TimeOfCall).ToList();

            var top = new List<OpenTimes>();
            if (type == EEntryRemovalType.Entry)
            {
                top = called.Where(x => x.IsLastEdited == true && x.TimeOfEntry == new DateTime())
                    .OrderByDescending(x => x.TimeOfCall)
                    .ToList();
            }
            else if (type == EEntryRemovalType.Exit)
            {
                top = called.Where(x => x.IsLastEdited == true && x.TimeOfExit == new DateTime())
                    .OrderByDescending(x => x.TimeOfEntry)
                    .ThenByDescending(x => x.TimeOfCall)
                    .ToList();
            }

            if (top.Count <= rows)
            {
                list.AddRange(top);

                if (registered.Count <= rows - top.Count)
                {
                    list.AddRange(registered);
                }
                else
                {
                    list.AddRange(registered.GetRange(0, rows - top.Count).OrderByDescending(x => x.TimeOfRegistration));
                }
            }
            else
            {
                list.AddRange(top.GetRange(0, rows));
            }

            return list;
        }
    }
}
