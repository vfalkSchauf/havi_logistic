﻿using MVC.Display;
using System.Collections.Generic;

namespace KnausTabbert.Display.GTX
{
    public class GtxTextMessage
    {
        private List<DisplayLine> MessageList = new List<DisplayLine>();
        private bool _FirstRegistIsCall;

        public GtxTextMessage(List<DisplayLine> messageList, bool firstRegistIsCall)
        {
            MessageList = messageList;
            _FirstRegistIsCall = firstRegistIsCall;
        }

        public void Clear()
        {
            MessageList.Clear();
        }

        //public void AddLine(string text)
        //{
        //    MessageList.Add(text);
        //}

        public byte[] ToArray()
        {
            List<byte> list = new List<byte>();
            bool first = true;
            foreach (var msg in MessageList)
            {

                foreach (byte b in StringToByteArr(msg, first))
                {
                    list.Add(b);
                }
                first = false;

            }
            return list.ToArray();
        }

        private byte[] StringToByteArr(DisplayLine msg, bool first)
        {
            List<byte> bytes = new List<byte>();
            // Steuercode damit erscheinungsform der zeichen geändert werden kann (zum Beispiel durch 'D')
            //byte preFix = 0x01C;
            //bytes.Add(preFix);
            // D == default settings. chars will be displayed normal (not blinking / different color/ ..)
            //bytes.Add((byte)('D'));

            if (msg.Flash)
            {
                //First Row should Blink if it was a Call
                bytes.Add(0x1C);
                bytes.Add(0x46);

            }
            //if (_FirstRegistIsCall && first)
            //{
            //    //First Row should Blink if it was a Call
            //    bytes.Add(0x1C);
            //    bytes.Add(0x46);
            //}

            foreach (char c in msg.Content)
            {
                byte value = (byte)c;

                switch (c)
                {
                    case 'Ü':
                        value = 0x9A;
                        break;
                    case 'Ö':
                        value = 0x99;
                        break;
                    case 'Ä':
                        value = 0x8E;
                        break;
                    //case 'ß':
                    //    val = 0xE1;
                    //    break;

                    //- Pfeil links: 0xAE
                    //- Pfeil rechts: 0xAF
                    //- Pfeil oben: 0xAD
                    case '↑':
                        value = 0xAD;
                        break;
                    case '→':
                        value = 0xAF;
                        break;
                    case '↓':
                        value = 0x20;
                        break;
                    case '←':
                        value = 0xAE;
                        break;
                }


                bytes.Add(value);
            }

            if (msg.Flash)
            {
                //First Row should Blink if it was a Call
                bytes.Add(0x1C);
                bytes.Add(0x46);

                //Rest should behave normal
                bytes.Add(0x01C);
                bytes.Add((byte)('D'));
            }

            //if (_FirstRegistIsCall && first)
            //{
            //    //First Row should Blink if it was a Call end
            //    bytes.Add(0x1C);
            //    bytes.Add(0x46);

            //    //Rest should behave normal
            //    bytes.Add(0x01C);
            //    bytes.Add((byte)('D'));
            //}

            return bytes.ToArray();
        }

    }
}
