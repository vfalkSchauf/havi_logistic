﻿using System;

namespace KnausTabbert.Display.Escape
{
    public class EscapeRelayProtocol
    {
        public static byte[] RelayProtocol { get { return new byte[4] { 0x1B, 0x21, 0x52, 0x0D };}}

        public byte[] RelayProtocolToSetTime { get; set; }

        public EscapeRelayProtocol(int relayTime)
        {
            int erstezahle = (relayTime / 10) % 10;
            int zweitezahl = (relayTime % 10);
            byte asdf = (byte)erstezahle.ToString()[0];
            byte asdf2 = (byte)zweitezahl.ToString()[0];
            RelayProtocolToSetTime = new byte[5] { 0x1B, 0x52, (byte)((relayTime / 10) % 10).ToString()[0], (byte)(relayTime % 10).ToString()[0], 0x0D };
        }

        public byte[] ToByteArray()
        {
            byte[] protocol = new byte[RelayProtocol.Length + RelayProtocolToSetTime.Length];

            Array.Copy(RelayProtocolToSetTime, protocol, RelayProtocolToSetTime.Length);
            Array.Copy(RelayProtocol, 0, protocol, RelayProtocolToSetTime.Length, RelayProtocol.Length);

            return protocol;
        }
    }
}
