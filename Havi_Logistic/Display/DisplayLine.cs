﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Display
{
    public class DisplayLine
    {
        public string Content { get; set; }

        public bool Flash { get; set; }

        public DisplayLine(string content, bool flash)
        {
            Content = content;
            Flash = flash;
        }
    }
}
