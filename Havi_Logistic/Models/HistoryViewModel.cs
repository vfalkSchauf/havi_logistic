﻿using MVC.Data.Entities;
using System.Collections.Generic;

namespace MVC.Models
{
    public class HistoryViewModel
    {
        public List<ClosedRegistration> Registrations { get; set; }

        public List<ClosedTimes> Times { get; set; }

        public string ClientRole { get; set; }
    }
}
