﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MVC.Data.Entities;
using System;
using System.Collections.Generic;

namespace MVC.Models
{
    public class RegistrationViewModel
    {
        public string LicensePlate { get; set; }

        public string ForwardingAgency { get; set; }

        public string Transportnumber { get; set; }

        public bool ForwardingAgencyUnknown { get; set; }

        public bool Deliver { get; set; }

        public bool PickUp { get; set; }

        public string Comment { get; set; }

        public List<DeliveryCountry> Countries { get; set; }

        public string SelectedDeliveryCountry { get; set; }

        public string DefaultDeliveryCountry { get; set; }

        public List<ForwardingAgency> ForwardingAgencies { get; set; }
    }
}
