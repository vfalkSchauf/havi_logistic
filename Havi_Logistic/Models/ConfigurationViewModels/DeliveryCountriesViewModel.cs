﻿using MVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Models.ConfigurationViewModels
{
    public class DeliveryCountriesViewModel
    {
        public string DefaultDeliveryCountry { get; set; }

        public List<DeliveryCountry> DeliveryCountries { get; set; }
    }
}
