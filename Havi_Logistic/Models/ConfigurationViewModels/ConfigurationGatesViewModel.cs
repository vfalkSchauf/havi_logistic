﻿using MVC.Data.Entities;
using System.Collections.Generic;

namespace MVC.Models.ConfigurationViewModels
{
    public class ConfigurationGatesViewModel
    {
        public string DefaultDeliveryGate { get; set; }

        public string DefaultPickUpGate { get; set; }

        public List<Gate> Gates { get; set; }
    }
}
