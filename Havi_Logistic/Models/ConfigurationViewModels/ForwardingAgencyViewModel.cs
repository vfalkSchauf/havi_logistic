﻿using MVC.Data.Entities;
using System.Collections.Generic;

namespace MVC.Models.ConfigurationViewModels
{
    public class ForwardingAgencyViewModel
    {
        public List<Gate> Gates { get; set; }

        public ForwardingAgency ForwardingAgency { get; set; }
    }
}
