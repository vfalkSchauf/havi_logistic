﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MVC.Data.Entities;
using MVC.Models.ViewModelSelectHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Models.ConfigurationViewModels
{
    public class GateViewModel
    {
        public Gate Gate { get; set; }

        public SelectList UseRelaySelect { get; set; }
    }
}
