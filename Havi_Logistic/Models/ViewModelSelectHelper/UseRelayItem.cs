﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Models.ViewModelSelectHelper
{
    public class UseRelayItem
    {
        public UseRelayItem(string key, bool value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }

        public bool Value { get; set; }
    }
}
