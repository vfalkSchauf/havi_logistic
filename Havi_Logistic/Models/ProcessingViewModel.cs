﻿using MVC.Data.Entities;
using System.Collections.Generic;

namespace MVC.Models
{
    public class ProcessingViewModel
    {
        public List<OpenRegistration> Registrations { get; set; }

        public List<OpenTimes> Times { get; set; }

        public List<Gate> Gates { get; set; }

        public string ClientRole { get; set; }
    }
}
