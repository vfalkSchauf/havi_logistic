﻿using Microsoft.EntityFrameworkCore;
using MVC.Data.DBContext;

namespace MVC.Factories
{
    public class DBContextFactory
    {
        private readonly string _connectionString;

        public DBContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ApplicationDBContext GetNewDBContext()
        {
            DbContextOptionsBuilder<ApplicationDBContext> builder = new DbContextOptionsBuilder<ApplicationDBContext>();
            builder.UseSqlServer(_connectionString);

            return new ApplicationDBContext(builder.Options);
        }
    }
}
