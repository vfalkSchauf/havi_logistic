﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MVC.HostedServices
{
    public class DisplayService : IHostedService, IDisposable
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;

        private readonly ILogger _logger;
        private Timer _timer;

        private OpenTimes latestTime;

        public DisplayService(IServiceProvider serviceProvider, ILogger<DisplayService> logger, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
            _configuration = configuration;
            latestTime = null;
        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation("Display Service is starting.");
                using (var scope = _serviceProvider.CreateScope())
                {
                    var _generalSettingsRepository = scope.ServiceProvider.GetRequiredService<IGeneralSettingsRepository>();

                    _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(int.Parse(_configuration["DisplayDuration"])));

                    return Task.CompletedTask;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Exception occured: {0} \nInnerException: {1}\nStacktrace: {2}", ex.Message, ex.InnerException, ex.StackTrace));
                throw ex;
            }
        }

        private void DoWork(object state)
        {
            try
            {
                //if you want to debug the display
                //_timer.Change(Timeout.Infinite, Timeout.Infinite);

                using (var scope = _serviceProvider.CreateScope())
                {
                    var displayFacade = scope.ServiceProvider.GetRequiredService<IDisplayFacade>();
                    latestTime = displayFacade.Update(latestTime);
                }

                //if you wnat to debug the display
                //_timer.Change(TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Exception occured: {0} \nInnerException: {1}\nStacktrace: {2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation("Display Service is stopping.");

                _timer?.Change(Timeout.Infinite, 0);

                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Exception occured: {0} \nInnerException: {1}\nStacktrace: {2}", ex.Message, ex.InnerException, ex.StackTrace));
                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _timer?.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Exception occured: {0} \nInnerException: {1}\nStacktrace: {2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
        }

    }
}
