﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFClosedRegistrationsRepository : IClosedRegistrationsRepository
    {
        private readonly ILogger<EFClosedRegistrationsRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFClosedRegistrationsRepository(ILogger<EFClosedRegistrationsRepository> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Add(ClosedRegistration registration, List<ClosedTimes> times)
        {
            _logger.LogInformation("Adding registration to history.");
            try
            {
                _context.RegistrationHistory.Add(registration);
                _context.ClosedTimes.AddRange(times);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add registration to history. Message: " + e.Message);
                throw new Exception("Eintrag konnte der Historie nicht hinzugefügt werden. Fehler: " + e.Message);
            }
        }

        public List<ClosedRegistration> GetAll()
        {
            _logger.LogInformation("Returning all Registrations from the history.");
            try
            {
                return _context.RegistrationHistory
                        .ToList();
            }
            catch (Exception e)
            {
                _logger.LogError("Cannot get all Closed Registrations. Message: " + e.Message);
                throw new Exception("Historie konnte nicht geladen werden. Fehler: " + e.Message);
            }
        }

        public List<ClosedTimes> GetAllTimes(List<ClosedRegistration> regists)
        {
            List<ClosedTimes> times = new List<ClosedTimes>();

            foreach (ClosedRegistration regist in regists)
            {
                times.AddRange(_context.ClosedTimes.Where(x => x.RegistratoinInformationID == regist.ID).ToList());
            }

            return times;
        }
    }
}
