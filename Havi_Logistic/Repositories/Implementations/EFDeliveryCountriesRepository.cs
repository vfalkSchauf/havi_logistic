﻿using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFDeliveryCountriesRepository : IDeliveryCountriesRepository
    {
        private readonly ILogger<EFDeliveryCountriesRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFDeliveryCountriesRepository(ILogger<EFDeliveryCountriesRepository> logger, ApplicationDBContext context)
        {
            _context = context;
            _logger = logger;
        }

        public List<DeliveryCountry> GetAll()
        {
            return _context.DeliveryCountries.OrderBy(x => x.Country).ToList();
        }

        public async Task Add(DeliveryCountry DeliveryCountry)
        {
            if (DeliveryCountry == null)
            {
                _logger.LogWarning("Tried to add DeliveryCountry = null");
                return;
            }

            try
            {
                await _context.DeliveryCountries.AddAsync(DeliveryCountry);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add DeliveryCountry to DB. Message: " + e.Message);
                throw new Exception("Das Lieferland konnte nicht hinzugefügt werden. Stellen Sie sicher, dass das Land nicht bereits erstellt wurde.");
            }
        }

        public async Task Delete(Guid id)
        {
            var DeliveryCountry = Get(id);
            if (DeliveryCountry == null)
            {
                _logger.LogWarning("Cannot remove DeliveryCountry. No DeliveryCountry with id: " + id + " could be found.");
                return;
            }
            try
            {
                _context.DeliveryCountries.Remove(DeliveryCountry);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete DeliveryCountry. id: " + id + " Message: " + e.Message);
                throw;
            }
        }

        public DeliveryCountry Get(Guid id)
        {
            DeliveryCountry DeliveryCountry = (from g in _context.DeliveryCountries
                                               where g.ID == id
                                               select g).FirstOrDefault();
            return DeliveryCountry;
        }

        public async Task Set(DeliveryCountry DeliveryCountry)
        {
            if (DeliveryCountry == null)
            {
                _logger.LogWarning("Could not set DeliveryCountry. Param was null.");
                return;
            }
            var dbGate = Get(DeliveryCountry.ID);
            if (dbGate == null)
            {
                _logger.LogWarning("Could not set DeliveryCountry. No gate with id: " + DeliveryCountry.ID + " exists in the db.");
                return;
            }
            try
            {
                dbGate.Country = DeliveryCountry.Country;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not set DeliveryCountry. Message: " + e.Message);
                throw new Exception("Änderungen konnten nicht gespeichert werden. Stellen Sie sicher, dass das Lieferland nicht bereits erstellt wurde");
            }
        }
    }
}
