﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Factories;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFOpenRegistrationsRepository : IOpenRegistrationsRepository
    {
        private readonly ILogger<EFOpenRegistrationsRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFOpenRegistrationsRepository(ILogger<EFOpenRegistrationsRepository> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task SetLicensePlate(Guid id, string newLicensePlate)
        {
            try
            {
                var registration = GetOpenRegistrationByID(id);
                if (registration == null)
                {
                    _logger.LogError("Couldnt set licensePlate for id: " + id + " registration couldnt be found.");
                    throw new Exception("Kennzeichen konnte nicht gesetzt werden. Die entsprechende Anmeldung wurde nicht gefunden.");
                }

                registration.LicensePlate = newLicensePlate;

                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task SetComment(Guid id, string newComment)
        {
            try
            {
                var registration = GetOpenRegistrationByID(id);
                if (registration == null)
                {
                    _logger.LogError("Couldnt set Comment for id: " + id + " registration couldnt be found.");
                    throw new Exception("Kommentar konnte nicht gesetzt werden. Die entsprechende Anmeldung wurde nicht gefunden.");
                }

                registration.Comment = newComment;

                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Add(OpenRegistration registration, List<OpenTimes> times = null)
        {
            _logger.LogInformation("Adding registration to DB");
            try
            {
                registration.LicensePlate = registration.LicensePlate.ToUpper();
                registration.ForwardingAgency = registration.ForwardingAgency?.ToUpper();

                if (times != null)
                {
                    await _context.OpenTimes.AddRangeAsync(times);
                }

                await _context.OpenRegistrations.AddAsync((OpenRegistration)registration);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to add OpenRegistration to DB. Message: " + e.Message);
                throw;
            }
        }

        public List<OpenRegistration> GetAll()
        {
            return _context.OpenRegistrations
                .ToList();
        }

        public OpenRegistration GetOpenRegistrationByID(Guid id)
        {
            return _context.OpenRegistrations
                .Where(r => r.ID == id)
                .FirstOrDefault();
        }

        public async Task Call(Guid id, DateTime curTime)
        {
            _logger.LogInformation("Setting call in DB for id: " + id + " to time: " + curTime.ToString());
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Aufruf konnte nicht ausgeführt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }

                times.Where(x => x.IsLastEdited == true).FirstOrDefault().TimeOfCall = curTime;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to set call in DB. Message: " + e.Message);
                throw new Exception("Aufruf konnte nicht ausgeführt werden. Fehler: " + e.Message);
            }
        }

        public async Task WithdrawCall(Guid id)
        {
            _logger.LogInformation("Withdraw Call in DB for id: " + id);
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Zurückziehen des Aufrufes konnte nicht ausgeführt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }

                OpenTimes oldTime = times.Where(x => x.IsLastEdited == true).FirstOrDefault();

                times.Where(x => x.IsLastEdited == true).FirstOrDefault().IsLastEdited = false;

                OpenTimes time = new OpenTimes()
                {
                    RegistratoinInformationID = oldTime.RegistratoinInformationID,
                    TimeOfRegistration = oldTime.TimeOfRegistration,
                    IsLastEdited = true,
                    Gate = oldTime.Gate
                };

                await _context.AddAsync(time);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to withdraw call in DB. Message: " + e.Message);
                throw new Exception("Das Zurückziehen des Aufrufes konnte nicht ausgeführt werden. Fehler: " + e.Message);
            }
        }

        public async Task UndoLastCallWithdraw(Guid id)
        {
            _logger.LogInformation("Undo latest Call Withdraw in DB for id: " + id);
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Rückgängig machen des Zurückziehes des Aufrufes konnte nicht ausgeführt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }

                times.Remove(times.Where(x => x.IsLastEdited == true).FirstOrDefault());
                times.Where(x => x.TimeOfRegistration == times.Max(c => c.TimeOfRegistration)).FirstOrDefault().IsLastEdited = true;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to undo latest call withdraw in DB. Message: " + e.Message);
                throw new Exception("Rückgängig machen des Zurückziehes des Aufrufes konnte nicht ausgeführt werden. Fehler: " + e.Message);
            }
        }

        public async Task SetEntry(Guid id, DateTime curTime)
        {
            _logger.LogInformation("Setting entry in db for id: " + id + " to time: " + curTime.ToString());
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Einfahrt konnte nicht gesetzt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }


                times.Where(x => x.IsLastEdited == true).FirstOrDefault().TimeOfEntry = curTime;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to set entry in DB. Message: " + e.Message);
                throw new Exception("Einfahrt konnte nicht gesetzt werden. Fehler: " + e.Message);
            }
        }

        public async Task WithdrawEntry(Guid id)
        {
            _logger.LogInformation("Withdraw Entry in DB for id: " + id);
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Zurückziehen der Einfahrt konnte nicht ausgeführt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }

                OpenTimes oldTime = times.Where(x => x.IsLastEdited == true).FirstOrDefault();

                times.Where(x => x.IsLastEdited == true).FirstOrDefault().IsLastEdited = false;

                OpenTimes time = new OpenTimes()
                {
                    RegistratoinInformationID = oldTime.RegistratoinInformationID,
                    TimeOfRegistration = oldTime.TimeOfRegistration,
                    TimeOfCall = oldTime.TimeOfCall,
                    IsLastEdited = true,
                    Gate = oldTime.Gate
                };

                await _context.AddAsync(time);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to withdraw entry in DB. Message: " + e.Message);
                throw new Exception("Das Zurückziehen der Einfahrt konnte nicht ausgeführt werden. Fehler: " + e.Message);
            }
        }

        public async Task UndoLastEntryWithdraw(Guid id)
        {
            _logger.LogInformation("Undo latest Entry Withdraw in DB for id: " + id);
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);

                if (times == null)
                {
                    throw new Exception("Rückgängig machen des Zurückziehes der Einfahrt konnte nicht ausgeführt werden. Der Eintrag wurde nicht in der DB gefunden.");
                }

                times.Remove(times.Where(x => x.IsLastEdited == true).FirstOrDefault());
                times.Where(x => x.TimeOfCall == times.Max(c => c.TimeOfCall)).FirstOrDefault().IsLastEdited = true;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to undo latest Entry withdraw in DB. Message: " + e.Message);
                throw new Exception("Rückgängig machen des Zurückziehes der Einfahrt konnte nicht ausgeführt werden. Fehler: " + e.Message);
            }
        }

        public async Task Remove(OpenRegistration registration, List<OpenTimes> times)
        {
            _logger.LogInformation("Removing registration from openRegistrations DB table");
            try
            {
                _context.OpenRegistrations.Remove(registration);
                _context.OpenTimes.RemoveRange(times);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while removing openRegistration from db. Message: " + e.Message);
                throw new Exception("Eintrag konnte nicht von der Liste der offenen Anmeldungen entfernt werden.");
            }
        }

        public OpenRegistration Get(Guid id)
        {
            try
            {
                var regist = GetOpenRegistrationByID(id);

                return regist;
            }
            catch (Exception e)
            {
                _logger.LogError("Could not get OpenRegistration with id: " + id + " Message: " + e.Message);
                throw new Exception("Anmeldung konnte nicht gefunden werden. Fehler: " + e.Message);
            }
        }

        public async Task<string> SetGate(Guid id, string value)
        {
            try
            {
                var times = GetRegistrationTimesFromRegistration(id);
                if (times == null)
                {
                    _logger.LogError("Couldnt set Gate for id: " + id + " registration couldnt be found.");
                    throw new Exception("Tor konnte nicht gesetzt werden. Die entsprechende Anmeldung wurde nicht gefunden.");
                }

                var latestRegistration = times.Where(r => r.IsLastEdited == true).FirstOrDefault();

                var oldGate = latestRegistration.Gate;
                latestRegistration.Gate = value;

                await _context.SaveChangesAsync();
                return oldGate;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OpenTimes> GetRegistrationTimesFromRegistration(Guid registrationID)
        {
            return _context.OpenTimes.Where(x => x.RegistratoinInformationID == registrationID).ToList();
        }

        public List<OpenTimes> GetAllRegistrationTimes(List<OpenRegistration> regists)
        {
            List<OpenTimes> times = new List<OpenTimes>();

            foreach (OpenRegistration regist in regists)
            {
                times.AddRange(GetRegistrationTimesFromRegistration(regist.ID));
            }

            return times;
        }
    }
}
