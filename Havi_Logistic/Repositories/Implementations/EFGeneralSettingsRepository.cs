﻿using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Models.ConfigurationViewModels;
using MVC.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFGeneralSettingsRepository : IGeneralSettingsRepository
    {
        private readonly ILogger<EFGeneralSettingsRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFGeneralSettingsRepository(ILogger<EFGeneralSettingsRepository> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public GeneralSettings GetGeneralSettings()
        {
            return _context.GeneralSettings.FirstOrDefault();
        }

        public async Task SetDefaultGates(ConfigurationGatesViewModel gate)
        {
            _logger.LogInformation("Setting default gates");
            var settings = GetGeneralSettings();
            if (settings == null)
            {
                _logger.LogError("Could not set default gates. Settings == null.");
                return;
            }
            settings.DefaultDeliveryGate = gate.DefaultDeliveryGate;
            settings.DefaultPickUpGate = gate.DefaultPickUpGate;
            await _context.SaveChangesAsync();
        }

        public async Task SetDefaultDeliveryCountry(DeliveryCountriesViewModel model)
        {
            _logger.LogInformation("Setting default DeliveryCountry");
            var settings = GetGeneralSettings();

            if (settings == null)
            {
                _logger.LogError("Could not set default DeliveryCountry. Settings == null.");
                return;
            }

            settings.DefaultDeliveryCountry = model.DefaultDeliveryCountry;

            await _context.SaveChangesAsync();
        }

        public async Task RemoveDefaultGateIfItWasSelected(string name)
        {
            _logger.LogInformation("Remove default Gate with Name: " + name);

            var settings = GetGeneralSettings();
            if (settings == null)
            {
                _logger.LogError("Could not remove default Gate with name \"" + name + "\". Settings == null.");
                return;
            }

            if (settings.DefaultDeliveryGate == name)
                settings.DefaultDeliveryGate = null;
            if (settings.DefaultPickUpGate == name)
                settings.DefaultPickUpGate = null;

            await _context.SaveChangesAsync();

        }

    }
}
