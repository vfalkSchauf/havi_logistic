﻿using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFForwardingAgenciesRepository : IForwardingAgenciesRepository
    {
        private readonly ILogger<EFForwardingAgenciesRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFForwardingAgenciesRepository(ILogger<EFForwardingAgenciesRepository> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Add(ForwardingAgency forwardingAgency)
        {
            if (forwardingAgency == null)
            {
                throw new ArgumentNullException("Spedition ist null");
            }
            _logger.LogInformation("Adding ForwardingAgency.");
            try
            {
                _context.Add(forwardingAgency);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add forwarding agency to DB. Message: " + e.Message);
                throw new Exception("Spedition konnte nicht hinzugefügt werden. Stellen Sie sicher, dass der Name nicht bereits vergeben ist.");
            }
        }

        public async Task<bool> AddForwardingAgencyFromUnkownForwardingAgency(UnknownForwardingAgency unknownForwardingAgency)
        {
            if (unknownForwardingAgency == null)
            {
                throw new ArgumentNullException("Spedition ist null");
            }
            _logger.LogInformation("Adding ForwardingAgency from UnknownForwardingAgancy.");
            try
            {
                ForwardingAgency forwardingAgency = new ForwardingAgency()
                {
                    ID = unknownForwardingAgency.ID,
                    Name = unknownForwardingAgency.Name
                };

                _context.Add(forwardingAgency);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add forwarding agency from Unknown forwarding agency on to DB. Message: " + e.Message);
                throw new Exception("Die unbekannte Spedition konnte nicht zu einer Bekannten gemacht werden. Stellen Sie sicher, dass der Name nicht bereits vergeben ist.");
            }
        }

        public ForwardingAgency Get(string name)
        {
            var agency = (from a in _context.ForwardingAgencies
                          where a.Name == name
                          select a).FirstOrDefault();
            return agency;
        }

        public ForwardingAgency Get(Guid id)
        {
            var agency = (from a in _context.ForwardingAgencies
                          where a.ID == id
                          select a).FirstOrDefault();
            return agency;
        }

        public List<ForwardingAgency> GetAll()
        {
            return _context.ForwardingAgencies.ToList();
        }

        public async Task Edit(ForwardingAgency forwardingAgency)
        {
            if (forwardingAgency == null)
            {
                _logger.LogError("Could not edit forwarding agency. param was null.");
                throw new ArgumentNullException("forwardingAgency is null");
            }
            try
            {
                var curAgency = (from a in _context.ForwardingAgencies
                                 where a.ID == forwardingAgency.ID
                                 select a).FirstOrDefault();
                if (curAgency == null)
                {
                    _logger.LogWarning("Could not edit forwarding agency. Forwarding agency wasnt found in db. id: " + forwardingAgency.ID);
                    return;
                }
                curAgency.Name = forwardingAgency.Name;
                curAgency.PickUpGate = forwardingAgency.PickUpGate;
                curAgency.DeliveryGate = forwardingAgency.DeliveryGate;
                curAgency.DeliveryAndPickUpGate = forwardingAgency.DeliveryAndPickUpGate;
                curAgency.ERP_ID = forwardingAgency.ERP_ID;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not edit forwarding agency. Message: " + e.Message);
                throw new Exception("Spedition konnte nicht bearbeitet werden. Stellen Sie sicher, dass der Name nicht bereits verwendet wird.");
            }
        }

        public async Task Delete(Guid id)
        {
            var agency = Get(id);
            if (agency == null)
            {
                _logger.LogWarning("Did not remove ForwardingAgency with id: " + id + " because it wasnt found in the db.");
                return;
            }
            _context.ForwardingAgencies.Remove(agency);
            await _context.SaveChangesAsync();
        }

        public async Task Import(List<ForwardingAgency> agencies)
        {
            var oldData = new List<ForwardingAgency>();
            var uniqueNames = CheckForUniqueNames(agencies);
            if (!uniqueNames)
            {
                throw new Exception("Die Namen der Tore müssen einzigartig sein!");
            }
            try
            {
                oldData = (from f in _context.ForwardingAgencies
                           select f).ToList();
                _context.ForwardingAgencies.RemoveRange(oldData);
                await _context.ForwardingAgencies.AddRangeAsync(agencies);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                try
                {
                    await _context.ForwardingAgencies.AddRangeAsync(oldData);
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Could not add the old data back to db. Message: " + ex.Message);
                }
                _logger.LogError("Error while trying to import gates into db. Message: " + e.Message);
                throw new Exception("Fehler beim Speichern der Daten in der Datenbank. Fehler: " + e.Message);
            }
        }

        private bool CheckForUniqueNames(List<ForwardingAgency> agencies)
        {
            for (int i = 0; i < agencies.Count; i++)
            {
                for (int j = i + 1; j < agencies.Count; j++)
                {
                    if (agencies[i].Name == agencies[j].Name)
                        return false;
                }
            }

            return true;
        }

    }
}
