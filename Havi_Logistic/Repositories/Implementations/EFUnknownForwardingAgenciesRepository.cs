﻿using Microsoft.Extensions.Logging;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Repositories.Implementations
{
    public class EFUnknownForwardingAgenciesRepository : IUnknownForwardingAgenciesRepository
    {
        private readonly ILogger<EFUnknownForwardingAgenciesRepository> _logger;

        private readonly ApplicationDBContext _context;

        public EFUnknownForwardingAgenciesRepository(ILogger<EFUnknownForwardingAgenciesRepository> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Add(string name)
        {
            _logger.LogInformation("Adding unknown forwarding agency. name: " + name);
            try
            {
                var existing = (from u in _context.UnkownForwardingAgencies
                                where u.Name.ToUpper() == name.ToUpper()
                                select u).FirstOrDefault();
                if (existing != null)
                {
                    _logger.LogInformation("ForwardingAgency was already in db. Increasing number of appereances.");
                    existing.NumberOfAppereances++;
                    await _context.SaveChangesAsync();
                    return;
                }
                else
                {
                    var _unkown = new UnknownForwardingAgency();
                    _unkown.Name = name;
                    _unkown.NumberOfAppereances = 1;
                    _unkown.FirstAppereance = DateTime.Now;
                    _context.UnkownForwardingAgencies.Add(_unkown);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error adding unkown forwarding agency. Message: " + e.Message);
            }
        }

        public UnknownForwardingAgency Get(Guid id)
        {
            return _context.UnkownForwardingAgencies.Where(x => x.ID == id).First();
        }

        public List<UnknownForwardingAgency> GetAll()
        {
            return (from u in _context.UnkownForwardingAgencies
                    select u).ToList();
        }

        public async Task Remove(Guid id)
        {
            var UnknownForwardingAgancy = _context.UnkownForwardingAgencies.Where(x => x.ID == id).First();
            if (UnknownForwardingAgancy == null)
            {
                _logger.LogWarning("Cannot remove UnknownforwardingAgancy. No UnknownforwardingAgancy with id: " + id + " could be found.");
                return;
            }
            try
            {
                _context.UnkownForwardingAgencies.Remove(UnknownForwardingAgancy);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete UnknownforwardingAgancy. id: " + id + " Message: " + e.Message);
                throw;
            }
        }
    }
}
