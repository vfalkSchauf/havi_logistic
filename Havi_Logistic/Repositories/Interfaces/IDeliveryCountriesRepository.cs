﻿using MVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC.Repositories.Interfaces
{
    public interface IDeliveryCountriesRepository
    {
        /// <summary>
        /// Returns a list of all DeliveryCountries from the DB.
        /// </summary>
        /// <returns></returns>
        List<DeliveryCountry> GetAll();

        /// <summary>
        /// Adds the passed along DeliverCountry to the DB.
        /// </summary>
        /// <param name="DeliverCountry"></param>
        /// <returns></returns>
        Task Add(DeliveryCountry DeliverCountry);

        /// <summary>
        /// Removes the DeliverCountry with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task Delete(Guid id);

        /// <summary>
        /// Gets the DeliverCountry with the given id.
        /// Will return null if no such Gate could be found.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DeliveryCountry Get(Guid id);

        /// <summary>
        /// Gets the corresponding DB entry for the param DeliveryCountry and
        /// sets the entry's values to match the gate's
        /// </summary>
        /// <param name="DeliveryCountry"></param>
        /// <returns></returns>
        Task Set(DeliveryCountry DeliveryCountry);
    }
}
