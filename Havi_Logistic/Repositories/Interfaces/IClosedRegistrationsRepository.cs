﻿using MVC.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC.Repositories.Interfaces
{
    public interface IClosedRegistrationsRepository
    {
        /// <summary>
        /// Adds the ClosedRegistration to the DB.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task Add(ClosedRegistration registration, List<ClosedTimes> times);

        /// <summary>
        /// Gets all ClosedRegistraions from the DB and returns them as a list.
        /// </summary>
        /// <returns></returns>
        List<ClosedRegistration> GetAll();

        /// <summary>
        /// Get all RegistrationtionTimes from all given Registrations out of the DB
        /// </summary>
        /// <param name="registrations"></param>
        /// <returns></returns>
        List<ClosedTimes> GetAllTimes(List<ClosedRegistration> registrations);
    }
}
