﻿using MVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC.Repositories.Interfaces
{
    public interface IUnknownForwardingAgenciesRepository
    {
        /// <summary>
        /// Adds a new UnknownForwardingAgency with the given name, numberOFAppereances = 1 and
        /// the time of first Appereance to the current servertime.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task Add(string name);

        /// <summary>
        /// Get the UnknownForwardingAgancy from DB with given id;
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UnknownForwardingAgency Get(Guid id);

        /// <summary>
        /// Retrieves the list of all UnknownForwardingAgencies from the DB.
        /// </summary>
        /// <returns></returns>
        List<UnknownForwardingAgency> GetAll();

        /// <summary>
        /// Removes a UnknownForwardingAgancy
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task Remove(Guid id);
    }
}
