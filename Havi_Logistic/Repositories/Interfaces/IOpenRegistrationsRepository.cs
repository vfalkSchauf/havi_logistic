﻿using MVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC.Repositories.Interfaces
{
    public interface IOpenRegistrationsRepository
    {
        /// <summary>
        /// Uses toUpper() on the license plate and the forwarding agency.
        /// Adds the registration to the DB. 
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task Add(OpenRegistration registration, List<OpenTimes> times = null);

        /// <summary>
        /// Gets all OpenRegistrations from the DB and returns them as a list.
        /// </summary>
        /// <returns></returns>
        List<OpenRegistration> GetAll();

        /// <summary>
        /// Get all Registration Times from a Registration
        /// </summary>
        /// <returns></returns>
        List<OpenTimes> GetRegistrationTimesFromRegistration(Guid registrationID);

        OpenRegistration GetOpenRegistrationByID(Guid id);

        /// <summary>
        /// Get all RegistrationTimes from all Openregistration in List
        /// </summary>
        /// <param name="regists"></param>
        /// <returns></returns>
        List<OpenTimes> GetAllRegistrationTimes(List<OpenRegistration> regists);

        /// <summary>
        /// Sets the time of Call for the OpenRegistration with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task Call(Guid id, DateTime curTime);

        /// <summary>
        /// Withdraw latest call for the OpenRegistration with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task WithdrawCall(Guid id);

        /// <summary>
        /// Undo the latest CallWithdraw if somethink went wrong
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task UndoLastCallWithdraw(Guid id);

        /// <summary>
        /// Withdraw latest entry for the Registration with given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task WithdrawEntry(Guid id);


        /// <summary>
        /// undo the latest entrywithdraw.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task UndoLastEntryWithdraw(Guid id);

        /// <summary>
        /// Sets the time of Entry for the OpenRegistration with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="curTime"></param>
        /// <returns></returns>
        Task SetEntry(Guid id, DateTime curTime);

        /// <summary>
        /// Removes the passed along registration from the DB.
        /// </summary>
        /// <param name="registration"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        Task Remove(OpenRegistration registration, List<OpenTimes> time);

        
        Task SetLicensePlate(Guid id, string newLicensePlate);

        Task SetComment(Guid id, string newComment);

        /// <summary>
        /// Gets and returns the OpenRegistration with the given id from the DB.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        OpenRegistration Get(Guid id);

        /// <summary>
        /// Sets the new Value for the gate for an OpenRegistration with the give id.
        /// Afterwards returns the old gate.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<string> SetGate(Guid id, string value);
    }
}
