﻿using MVC.Data.Entities;
using MVC.Models.ConfigurationViewModels;
using System.Threading.Tasks;

namespace MVC.Repositories.Interfaces
{
    public interface IGeneralSettingsRepository
    {
        /// <summary>
        /// Loads the GeneralSettings from the db.
        /// Will always load the first entry and there should always be exactly 1 entry.
        /// </summary>
        /// <returns></returns>
        GeneralSettings GetGeneralSettings();

        /// <summary>
        /// Sets the Default Gates to the Gates passed along in the ViewModel.
        /// </summary>
        /// <param name="gate"></param>
        /// <returns></returns>
        Task SetDefaultGates(ConfigurationGatesViewModel gate);

        /// <summary>
        /// Sets the Default DeliveryCountry.
        /// </summary>
        /// <param name="deliveryCountry"></param>
        /// <returns></returns>
        Task SetDefaultDeliveryCountry(DeliveryCountriesViewModel deliveryCountry);

        /// <summary>
        /// Searches the Default Gates for the given name and removes it if it Exists.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task RemoveDefaultGateIfItWasSelected(string name);
    }
}
