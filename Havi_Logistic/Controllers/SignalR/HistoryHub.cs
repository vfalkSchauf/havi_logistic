﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace MVC.Controllers.SignalR
{
    public class HistoryHub : Hub
    {

        private readonly ILogger<HistoryHub> _logger;

        public HistoryHub(ILogger<HistoryHub> logger)
        {
            _logger = logger;
        }

        public async Task JoinGroup(string groupName)
        {
            try
            {
            if (!String.IsNullOrEmpty(groupName))
                await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while Joining " + groupName + "group. Message: " + e.Message);
            }
        }
    }
}
