﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC.Controllers.SignalR
{
    /// <summary>
    /// Handels the actions performed on the processinglist.
    /// </summary>
    public class ProcessingHub : Hub
    {

        private readonly ILogger<ProcessingHub> _logger;
        private readonly IServiceProvider _serviceProvider;

        public ProcessingHub(ILogger<ProcessingHub> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task JoinGroup(string groupName)
        {
            try
            {
                if (!String.IsNullOrEmpty(groupName))
                    await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while Joining " + groupName + "group. Message: " + e.Message);
            }
        }

        /// <summary>
        /// Sets the licencePlate of the registration with the given id
        /// </summary>
        /// <param name="ID">ID of the registration</param>
        /// <param name="LicencPlate">Name of the Company</param>
        /// <returns></returns>
        public async Task UpdateLicensePlate(Guid ID, string LicencPlate)
        {
            _logger.LogInformation("ProcessingHub, UpdateComment: Method called for the registration with id: " + ID);

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.UpdateLicensePlate(ID, LicencPlate);
                    await Clients.All.SendAsync("UpdateLicensePlate", ID.ToString(), LicencPlate);
                }
                catch (Exception e)
                {
                    _logger.LogError("ProcessingHub, UpdateLicensePlate: Error! registration id: " + ID + " error message: " + e.Message + " inner exception message: " + e.InnerException?.Message);
                    await Clients.Caller.SendAsync("Error", "Das Kennzeichen konnte nicht gesetzt werden. Fehler: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Sets the comment of the registration with the given id
        /// </summary>
        /// <param name="ID">ID of the registration</param>
        /// <param name="Comment">Name of the Company</param>
        /// <returns></returns>
        public async Task UpdateComment(Guid ID, string Comment)
        {
            _logger.LogInformation("ProcessingHub, UpdateComment: Method called for the registration with id: " + ID);

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.UpdateComment(ID, Comment);
                    await Clients.All.SendAsync("UpdateComment", ID.ToString(), Comment);
                }
                catch (Exception e)
                {
                    _logger.LogError("ProcessingHub, UpdateComment: Error! registration id: " + ID + " error message: " + e.Message + " inner exception message: " + e.InnerException?.Message);
                    await Clients.Caller.SendAsync("Error", "Der Kommentar konnte nicht gesetzt werden. Fehler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "Call")]
        public async Task Call(Guid id)
        {
            _logger.LogInformation("Call id:" + id);
            var curTime = DateTime.Now;

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.Call(id, curTime);
                    await Clients.All.SendAsync("SetCall", id, curTime);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while calling id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Aufruf konnte nicht ausgeführt werden. Feler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "WithdrawCall")]
        public async Task WithdrawCall(Guid id)
        {
            _logger.LogInformation("WithdrawCall id:" + id);

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.WithdrawCall(id);
                    await Clients.All.SendAsync("WithdrawCall", id);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while Call Withdrawing with id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Zurückziehen des Aufrufes konnte nicht ausgeführt werden. Fehler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "Entry")]
        public async Task SetEntry(Guid id)
        {
            _logger.LogInformation("SetEntry id: " + id);

            var curTime = DateTime.Now;
            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.SetEntry(id, curTime);
                    await Clients.All.SendAsync("SetEntry", id, curTime);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while setting entry for id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Einfahrt konnte nicht gesetzt werden. Fehler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "WithdrawEntry")]
        public async Task WithdrawEntry(Guid id)
        {
            _logger.LogInformation("WithdrawEnry id:" + id);

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.WithdrawEntry(id);
                    await Clients.All.SendAsync("WithdrawEntry", id);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while Entry Withdrawing with id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Zurückziehen der Einfahrt konnte nicht ausgeführt werden. Fehler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "Exit")]
        public async Task SetExit(Guid id)
        {
            _logger.LogInformation("SetExit id: " + id);

            var curTime = DateTime.Now;
            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    await _processingHubFacade.SetExit(id, curTime);
                    await Clients.All.SendAsync("SetExit", id, curTime);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while setting exit for id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Ausfahrt konnte nicht gesetzt werden. Eventuell werden auf der Anzeige noch Fahrzeuge angezeigt, welche sich nicht mehr auf dem Gelände befinden. Fehler: " + e.Message);
                }
            }
        }

        [Authorize(Roles = "Gate")]
        public async Task SetGate(Guid id, string value)
        {
            _logger.LogInformation("Set Gate id: " + id + " value: " + value);

            using (var scope = _serviceProvider.CreateScope())
            {
                try
                {
                    var _processingHubFacade = scope.ServiceProvider.GetRequiredService<IProcessingHubFacade>();
                    var _gatesRepository = scope.ServiceProvider.GetRequiredService<IGatesRepository>();
                    await _processingHubFacade.SetGate(id, value);

                    var gates = _gatesRepository.GetAll();
                    string gatesAsJavaScriptList = getGatesAsJavaScriptObjects(gates);
                    await Clients.All.SendAsync("SetGate", id, value, gatesAsJavaScriptList);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while setting gate for id: " + id + ". Message: " + e.Message);
                    await Clients.Caller.SendAsync("Error", "Tor konnte nicht gesetzt werden. Fehler: " + e.Message);
                }
            }
        }

        private string getGatesAsJavaScriptObjects(List<Gate> gates)
        {
            try
            {
                string list = "[";

                for (int i = 0; i < gates.Count; i++)
                {
                    if (i != gates.Count - 1)
                        list += string.Format("{{ \"id\":\"{0}\", \"name\":\"{1}\", \"gateType\":\"{2}\" }},", gates[i].ID, gates[i].Name, gates[i].GateType);
                    else
                        list += string.Format("{{ \"id\":\"{0}\", \"name\":\"{1}\", \"gateType\":\"{2}\" }} ]", gates[i].ID, gates[i].Name, gates[i].GateType);
                }
                return list;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while transforming gatesList in Javascript objectlist. Exception: " + ex.Message);
                throw ex;
            }
        }
    }
}
