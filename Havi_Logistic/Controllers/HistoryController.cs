﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using System;
using System.Linq;

namespace MVC.Controllers
{
    /// <summary>
    /// Handles actions performed in the "Historie"-Area.
    /// Note: Some actions such as the "Auswahl exportieren" and "Erweitert filtern" are done on the client with js.
    /// </summary>
    [Authorize(Roles = "History")]
    public class HistoryController : Controller
    {
        private readonly ILogger<HistoryController> _logger;
        private readonly IHistoryFacade _historyFacade;

        public HistoryController(ILogger<HistoryController> logger, IHistoryFacade historyFacade)
        {
            _logger = logger;
            _historyFacade = historyFacade;
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index History");
            var model = _historyFacade.GetHistoryViewModel();
            string userRole = getDeliverPickupRole();

            if (userRole == "DeliverAndPickUp")
            {
                model.ClientRole = userRole;
                return View(model);
            }
            else if (userRole == "Deliver")
            {
                model.ClientRole = userRole;
                model.Registrations = model.Registrations.Where(x => x.JobType == EJobType.Deliver || x.JobType == EJobType.DeliverAndPickUp).ToList();
                model.Times = model.Times.Where(x => model.Registrations.Any(y => y.ID == x.RegistratoinInformationID)).ToList();
            }
            else if (userRole == "PickUp")
            {
                model.ClientRole = userRole;
                model.Registrations = model.Registrations.Where(x => x.JobType == EJobType.PickUp || x.JobType == EJobType.DeliverAndPickUp).ToList();
                model.Times = model.Times.Where(x => model.Registrations.Any(y => y.ID == x.RegistratoinInformationID)).ToList();
            }

            return View(model);
        }

        [Authorize(Roles = "Export")]
        public IActionResult ExportHistoryCSV()
        {
            _logger.LogInformation("Export History CSV");
            try
            {
                var stream = _historyFacade.ExportHistoryCSV(getDeliverPickupRole());
                var response = File(stream, "configuration/ExportHistoryCSV", "GesamteHistorieExport.csv");
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError("Error while exporting history csv. Message: " + e.Message);
                throw;
            }
        }

        private string getDeliverPickupRole()
        {
            string userRole = string.Empty;

            if (this.HttpContext.User.IsInRole("DeliverAndPickUp"))
                userRole = "DeliverAndPickUp";
            else if (this.HttpContext.User.IsInRole("Deliver"))
                userRole = "Deliver";
            else if (this.HttpContext.User.IsInRole("PickUp"))
                userRole = "PickUp";

            return userRole;
        }
    }
}