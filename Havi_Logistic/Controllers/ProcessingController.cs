﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using System.Linq;

namespace MVC.Controllers
{
    /// <summary>
    /// Note: Most actions are handled in the ProcessingHub as they are initiated through SignalR and Javascript
    /// </summary>
    [Authorize(Roles = "Processing")]
    public class ProcessingController : Controller
    {
        private readonly ILogger<ProcessingController> _logger;

        private readonly IProcessingFacade _processingFacade;

        public ProcessingController(ILogger<ProcessingController> logger, IProcessingFacade processingFacade)
        {
            _processingFacade = processingFacade;
            _logger = logger;
        }


        public IActionResult Index()
        {
            _logger.LogInformation("Index");

            var model = _processingFacade.GetProcessingViewModel();

            if (this.HttpContext.User.IsInRole("DeliverAndPickUp"))
            {
                model.ClientRole = "DeliverAndPickUp";
                return View(model);
            }
            else if (this.HttpContext.User.IsInRole("Deliver"))
            {
                model.ClientRole = "Deliver";
                model.Registrations = model.Registrations.Where(x => x.JobType == EJobType.Deliver || x.JobType == EJobType.DeliverAndPickUp).ToList();
                model.Gates = model.Gates.Where(x => x.GateType == EJobType.Deliver || x.GateType == EJobType.DeliverAndPickUp).ToList();
                model.Times = model.Times.Where(x => model.Registrations.Any(y => y.ID == x.RegistratoinInformationID)).ToList();
            }
            else if (this.HttpContext.User.IsInRole("PickUp"))
            {
                model.ClientRole = "PickUp";
                model.Registrations = model.Registrations.Where(x => x.JobType == EJobType.PickUp || x.JobType == EJobType.DeliverAndPickUp).ToList();
                model.Gates = model.Gates.Where(x => x.GateType == EJobType.PickUp || x.GateType == EJobType.DeliverAndPickUp).ToList();
                model.Times = model.Times.Where(x => model.Registrations.Any(y => y.ID == x.RegistratoinInformationID)).ToList();
            }

            return View(model);
        }
    }
}