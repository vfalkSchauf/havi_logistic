﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Models;
using MVC.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    /// <summary>
    /// Handles the login and logout features.
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private ILogger<AccountController> _logger;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signInManager;
        private IADReader _adReader;
        private readonly IAuthorizationGroupsRepository _groupsRepository;
        private readonly IAccountFacade _accountFacade;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IADReader adReader,
            IAuthorizationGroupsRepository groupsRepository, IAccountFacade accountFacade, ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _adReader = adReader;
            _groupsRepository = groupsRepository;
            _accountFacade = accountFacade;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl, string msg = "")
        {
            ViewBag.returnUrl = returnUrl;
            switch (returnUrl)
            {
                case "/Registration":
                    ViewBag.targetArea = "Registration";
                    break;
                case "/Processing":
                    ViewBag.targetArea = "Processing";
                    break;
                case "/History":
                    ViewBag.targetArea = "History";
                    break;
                case "/Configuration":
                    ViewBag.targetArea = "Configuration";
                    break;
                default:
                    ViewBag.targetArea = "Registration";
                    break;
            }

            ViewBag.ErrorMessage = msg;

            return View(new LoginViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel details, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(details.Password))
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Password), "Das Passwort darf nicht leer sein.");
                }
                else
                {
                    AppUser user = await _userManager.FindByNameAsync(details.UserName);
                    if (user != null)
                    {
                        await _signInManager.SignOutAsync();
                        var result = await _signInManager.PasswordSignInAsync(details.UserName, details.Password, details.RememberMe, false);
                        if (result.Succeeded)
                        {
                            returnUrl = getReturnUrl(user.AuthorizationGroup);
                            return Redirect(returnUrl ?? "/");
                        }

                        else if (_accountFacade.UseAD && _adReader.CheckADLogin(details.UserName, details.Password))
                        {
                            await _signInManager.SignInAsync(user, false);
                            returnUrl = getReturnUrl(user.AuthorizationGroup);
                            return Redirect(returnUrl ?? "/");
                        }

                    }
                    else
                    {
                        if (_accountFacade.UseAD && _accountFacade.GenerateAccountsForNewADUsers)
                        {
                            _logger.LogInformation("Creating User from AD Information.");
                            try
                            {
                                user = await CreateUserFromAD(User);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Error while trying to create user from AD Information. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                                return Redirect(returnUrl ?? "/");
                            }

                            if (user != null && !_signInManager.IsSignedIn(User))
                            {
                                await _signInManager.SignOutAsync();
                                _logger.LogInformation("logged out user");
                                await _signInManager.SignInAsync(user, false);
                                _logger.LogInformation("Logged in user");
                                returnUrl = getReturnUrl(user.AuthorizationGroup);
                                return Redirect(returnUrl ?? "/");
                            }
                        }

                        return RedirectToAction("Login", new { returnUrl });

                    }
                    ModelState.AddModelError(nameof(LoginViewModel.UserName), "Benutzername oder Passwort inkorrekt.");
                    ModelState.AddModelError(nameof(LoginViewModel.Password), "");
                }
            }

            return View(details);
        }

        private string getReturnUrl(string authorizationGroup)
        {
            var roles = _groupsRepository.Get(authorizationGroup);

            if (roles.CanUseRegistration)
                return null;
            else if (roles.CanUseProcessing)
                return "/Processing";
            else if (roles.CanUseHistory)
                return "/History";
            else if (roles.CanUseConfig)
                return "/Configuration";

            return null;
        }

        [Authorize]
        public async Task<IActionResult> Logout(string targetArea = null)
        {
            _logger.LogInformation("Logging out user with name: " + User?.Identity?.Name);
            await _signInManager.SignOutAsync();

            return LocalRedirect("~/Account/Login");
        }

        [AllowAnonymous]
        public async Task<IActionResult> AccessDenied(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            try
            {
                if (User?.Identity?.IsAuthenticated == true)
                {
                    var userName = User?.Identity?.Name;
                    if (!String.IsNullOrEmpty(userName))
                    {
                        var user = await _userManager.FindByNameAsync(userName);

                        if (user != null)
                        {
                            _logger.LogInformation("User not null");
                            if (!_signInManager.IsSignedIn(User) && _accountFacade.UseAD)
                            {
                                _logger.LogInformation("not signed in and use ad");
                                await _signInManager.SignOutAsync();
                                _logger.LogInformation("logged out user");
                                await _signInManager.SignInAsync(user, false);
                                _logger.LogInformation("logged in user");
                                return Redirect(returnUrl);
                            }
                            // User is authenticated and was found in db and still landed in access denied so he is not supposed to have access
                            return RedirectToAction("Login", new { returnUrl, msg = "Zugriff verweigert." });
                        }
                        else
                        {
                            if (_accountFacade.UseAD && _accountFacade.GenerateAccountsForNewADUsers)
                            {
                                _logger.LogInformation("Creating User from AD Information.");
                                try
                                {
                                    user = await CreateUserFromAD(User);
                                }
                                catch (Exception e)
                                {
                                    _logger.LogError("Error while trying to create user from AD Information. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                                    return RedirectToAction("Login", new { returnUrl, msg = e.Message });
                                }

                                if (user != null && !_signInManager.IsSignedIn(User))
                                {
                                    await _signInManager.SignOutAsync();
                                    _logger.LogInformation("logged out user");
                                    await _signInManager.SignInAsync(user, false);
                                    _logger.LogInformation("Logged in user");
                                    return Redirect(returnUrl);
                                }
                            }

                            return RedirectToAction("Login", new { returnUrl });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error while handeling 'Access Denied'. Message: " + e.Message + " inner: " + e.InnerException?.Message);
            }
            return RedirectToAction("Login");
        }

        private async Task<AppUser> CreateUserFromAD(ClaimsPrincipal user)
        {
            var userName = User?.Identity?.Name;
            if (String.IsNullOrEmpty(userName))
                return null;

            var adGroups = new List<String>();
            try
            {
                adGroups = _adReader.GetADGroups(userName);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to get ad groups for user: " + userName + ". Message: " + e.Message + " inner: " + e.InnerException?.Message);
                throw new Exception("Fehler beim erstellen des Nutzers aus den AD Daten. ");
            }
            var authGroups = _groupsRepository.GetAll();

            var newUser = new AppUser
            {
                UserName = userName
            };

            var matchingGroups = new List<string>();
            foreach (var adGrp in adGroups)
            {
                foreach (var authGrp in authGroups)
                {
                    if (adGrp == authGrp.ADGroupName)
                    {
                        matchingGroups.Add(authGrp.Name);
                    }
                }
            }
            if (matchingGroups.Count > 0)
            {
                newUser.AuthorizationGroup = matchingGroups[0];
            }

            var result = await _userManager.CreateAsync(newUser);

            if (!string.IsNullOrEmpty(newUser.AuthorizationGroup))
                await SetUserRoles(newUser);

            return newUser;
        }

        private async Task SetUserRoles(AppUser user)
        {
            var group = _groupsRepository.Get(user.AuthorizationGroup);
            if (group == null)
            {
                return;
            }
            List<string> addRoles = new List<string>();

            var roles = await _userManager.GetRolesAsync(user);

            if (group.CanUseRegistration)
                addRoles.Add("Registration");


            if (group.CanUseConfig)
                addRoles.Add("Configuration");

            if (group.CanUseHistory)
                addRoles.Add("History");

            if (group.CanUseProcessing)
                addRoles.Add("Processing");

            if (group.AllowDeliver && group.AllowPickup)
                addRoles.Add("DeliverAndPickUp");
            else if (group.AllowPickup)
                addRoles.Add("PickUp");
            else if (group.AllowDeliver)
                addRoles.Add("Deliver");

            if (group.SetEntrance)
            {
                addRoles.Add("Entry");
                addRoles.Add("WithdrawEntry");
            }

            if (group.SetExit)
                addRoles.Add("Exit");

            if (group.SetGate)
                addRoles.Add("Gate");

            if (group.AllowEntrance)
            {
                addRoles.Add("Call");
                addRoles.Add("WithdrawCall");
            }

            if (group.Export)
                addRoles.Add("Export");

            var resultRemove = await _userManager.RemoveFromRolesAsync(user, roles);
            var resultAdd = await _userManager.AddToRolesAsync(user, addRoles);
        }
    }
}