﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Data.Entities;
using MVC.Models.ConfigurationViewModels;
using System;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    /// <summary>
    /// Handles all actions perfomed while beeing in the "Konfiguration"-Area
    /// </summary>
    [Authorize(Roles = "Configuration")]
    public class ConfigurationController : Controller
    {
        private readonly ILogger<ConfigurationController> _logger;

        private readonly IConfigurationFacade _configurationFacade;

        private readonly IUserValidator<AppUser> _userValidator;
        private readonly IPasswordHasher<AppUser> _passwordHasher;
        private readonly IPasswordValidator<AppUser> _passwordValidator;

        public ConfigurationController(ILogger<ConfigurationController> logger, IConfigurationFacade configurationFacade,
            IUserValidator<AppUser> userValidator, IPasswordValidator<AppUser> passwordValidator, IPasswordHasher<AppUser> passwordHasher)
        {
            _logger = logger;
            _userValidator = userValidator;
            _passwordHasher = passwordHasher;
            _passwordValidator = passwordValidator;
            _configurationFacade = configurationFacade;
        }

        public IActionResult Index(string msg = "")
        {
            return RedirectToAction("Gates", new { msg });
        }

        #region Gates
        public IActionResult Gates(string msg = "")
        {
            _logger.LogInformation("Config Gates");
            var model = _configurationFacade.GetGatesViewModel();
            ViewBag.ErrorMessage = msg;
            return View(model);
        }

        public IActionResult EditGate(Guid id)
        {
            _logger.LogInformation("Config EditGate id: " + id);
            try
            {
                var model = _configurationFacade.GetGateViewModel(id);
                if (model == null)
                {
                    _logger.LogError("Could not show EditGate View because the gate with id: " + id + " could not be found.");
                    var error = "Tor konnte nicht geladen werden.";
                    return RedirectToAction("Gates", new { msg = error });
                }
                return View(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not display EditGate View. Message: " + e.Message);
                var error = e.Message;
                return RedirectToAction("Gates", new { msg = error });
            }
        }

        public IActionResult AddGate()
        {
            _logger.LogInformation("Config AddGate");
            try
            {
                var model = _configurationFacade.GetGateViewModel();
                return View(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not display AddGateView . Message: " + e.Message);
                var error = e.Message;
                return RedirectToAction("Gates", new { msg = error });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteGate(Guid id)
        {
            _logger.LogInformation("POST: DeleteGate id: " + id);
            try
            {
                await _configurationFacade.DeleteGate(id);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete gate with id: " + id + " Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("Gates", new { msg = ViewBag.ErrorMessage });
        }

        [HttpPost]
        public async Task<IActionResult> EditGate(GateViewModel model)
        {
            _logger.LogInformation("POST: SetGate");
            if (!ModelState.IsValid)
                return View(model);
            try
            {
                await _configurationFacade.EditGate(model.Gate);
                return RedirectToAction("Gates");
            }
            catch (Exception e)
            {
                _logger.LogError("Could not set Gate. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
                return View(model);
            }

        }

        [HttpPost]
        public async Task<IActionResult> AddGate(GateViewModel model)
        {
            _logger.LogInformation("POST: AddGate");
            if (ModelState.IsValid)
            {
                try
                {
                    await _configurationFacade.AddGate(model.Gate);
                    return RedirectToAction("Gates");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SetDefaultGates(ConfigurationGatesViewModel model)
        {
            _logger.LogInformation("POST: SetDefaultGates");
            await _configurationFacade.SetDefaultGates(model);
            return RedirectToAction("Gates");
        }

        [HttpPost]
        public async Task<IActionResult> ImportGatesFromCSV(IFormFile file)
        {
            _logger.LogInformation("POST: ImportGatesFromCSV");
            try
            {
                await _configurationFacade.ImportGatesFromCSV(file);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to import gates from csv file. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("Gates", new { msg = ViewBag.ErrorMessage });
        }
        #endregion

        #region ForwardingAgencies
        public IActionResult ForwardingAgencies(string msg = "")
        {
            _logger.LogInformation("Config ForwardingAgencies");
            var model = _configurationFacade.GetForwardingAgenciesIndexViewModel();
            ViewBag.ErrorMessage = msg;
            return View(model);
        }

        public IActionResult AddForwardingAgency()
        {
            _logger.LogInformation("Config AddForwardingAgency");
            var model = _configurationFacade.GetForwardingAgencyViewModel();
            return View(model);
        }

        public IActionResult EditForwardingAgency(Guid id)
        {
            _logger.LogInformation("Config EditForwardingAgency");
            var model = _configurationFacade.GetForwardingAgencyViewModel(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddForwardingAgency(ForwardingAgencyViewModel model)
        {
            _logger.LogInformation("POST: AddForwardingAgency");
            if (!ModelState.IsValid)
            {
                model.Gates = _configurationFacade.GetAllGates();
                return View(model);
            }
            try
            {
                await _configurationFacade.AddForwardingAgency(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add ForwardingAgency. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
                model.Gates = _configurationFacade.GetAllGates();
                return View(model);
            }
            return RedirectToAction("ForwardingAgencies");
        }

        [HttpPost]
        public async Task<IActionResult> EditForwardingAgency(ForwardingAgencyViewModel model)
        {
            _logger.LogInformation("POST: EditForwardingAgency");
            if (!ModelState.IsValid)
            {
                model.Gates = _configurationFacade.GetAllGates();
                return View(model);
            }
            try
            {
                await _configurationFacade.EditForwardingAgency(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not edit ForwardingAgency. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
                model.Gates = _configurationFacade.GetAllGates();
                return View(model);
            }
            return RedirectToAction("ForwardingAgencies");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteForwardingAgency(Guid id)
        {
            _logger.LogInformation("POST: DeleteForwardingAgency id: " + id);
            try
            {
                await _configurationFacade.DeleteForwardingAgency(id);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete forwarding agency with id: " + id + " Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("ForwardingAgencies", new { msg = ViewBag.ErrorMessage });
        }

        [HttpPost]
        public async Task<IActionResult> ImportForwardingAgenciesFromCSV(IFormFile file)
        {
            _logger.LogInformation("POST: ImportForwardingAgenciesFromCSV");
            try
            {
                await _configurationFacade.ImportForwardingAgenciesFromCSV(file);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while trying to import forwarding agencies from csv file. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("ForwardingAgencies", new { msg = ViewBag.ErrorMessage });
        }
        #endregion

        #region UnknownForwardingAgencies
        public IActionResult UnknownForwardingAgencies(string msg = "")
        {
            _logger.LogInformation("Config UnknownForwardingAgencies");
            var model = _configurationFacade.GetUnknownForwardingAgenciesViewModel();
            ViewBag.ErrorMessage = msg;
            return View(model);
        }

        public IActionResult ExportUnknownForwardingAgenciesCSV()
        {
            var errorMessage = "";
            try
            {
                var stream = _configurationFacade.ExportUnknownForwardingAgenciesCSVStream();
                var response = File(stream, "configuration/ExportUnknownSuppliersCSV", "UnknownSuppliers_CSV_Data.csv");
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while trying to export UnkownsuppliersCSV. Errorcode: 43");
                errorMessage = "Fehler beim Exportieren der unbekannten Lieferanten als csv-Datei. Fehlercode: 43";
            }
            return RedirectToAction("BackToConfig", new { section = "UnknownSuppliers", errorMessage });
        }

        public IActionResult ExportUnknownForwardingAgenciesXML()
        {
            var errorMessage = "";
            try
            {
                var stream = _configurationFacade.ExportUnknownForwardingAgenciesXMLStream();
                var response = File(stream, "configuration/ExportUnknownSuppliersXML", "UnknownSuppliers_XML_Data.xml");
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while trying to addUnloading. Errorcode: 46 " + e.Message);
                errorMessage = "Fehler beim hinzufügen einer Entladestelle. Fehlercode: 46";
            }
            return RedirectToAction("BackToConfig", new { section = "UnknownSuppliers", errorMessage });
        }

        public async Task<IActionResult> AddToFowardingAgencies(Guid id)
        {
            var errorMessage = "";
            try
            {
               await _configurationFacade.AddUnknownForwardingAgencieToKnownForwardingAgencies(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Eroor while try to add an UnknownForwardingAgancy to the known Forwardingagancys " + e.Message);
                errorMessage = "Fehler beim hinzufügen von der Unbekannten Spedition";
            }
            return RedirectToAction("UnknownForwardingAgencies", new { section = "UnknownSuppliers", errorMessage });
        }

        #endregion

        #region DeliveryCountries
        public IActionResult DeliveryCountries(string msg = "")
        {
            _logger.LogInformation("Config Delivery Countries");
            var model = _configurationFacade.GetDeliveryCountriesViewModel();
            ViewBag.ErrorMessage = msg;
            return View(model);
        }

        public IActionResult EditDeliveryCountry(Guid id)
        {
            _logger.LogInformation("Config EditDeliveryCountry id: " + id);
            try
            {
                var model = _configurationFacade.GetDeliveryCountry(id);
                if (model == null)
                {
                    _logger.LogError("Could not show EditDeliveryCountry View because the DeliveryCountry with id: " + id + " could not be found.");
                    var error = "Das Lieferland konnte nicht geladen werden.";
                    return RedirectToAction("DeliveryCountries", new { msg = error });
                }
                return View(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not display EditDeliveryCountry View. Message: " + e.Message);
                var error = e.Message;
                return RedirectToAction("DeliveryCountries", new { msg = error });
            }
        }

        public IActionResult AddDeliveryCountry()
        {
            _logger.LogInformation("Config AddDeliveryCountry");
            return View(new DeliveryCountry());
        }

        [HttpPost]
        public async Task<IActionResult> DeleteDeliveryCountry(Guid id)
        {
            _logger.LogInformation("POST: DeleteDeliveryCountry id: " + id);
            try
            {
                await _configurationFacade.DeleteDeliveryCountry(id);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete DeliveryCountry with id: " + id + " Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("DeliveryCountries", new { msg = ViewBag.ErrorMessage });
        }

        [HttpPost]
        public async Task<IActionResult> EditDeliveryCountry(DeliveryCountry DeliveryCountry)
        {
            _logger.LogInformation("POST: SetDeliveryCountry");
            if (!ModelState.IsValid)
                return View(DeliveryCountry);
            try
            {
                await _configurationFacade.EditDeliveryCountry(DeliveryCountry);
                return RedirectToAction("DeliveryCountries");
            }
            catch (Exception e)
            {
                _logger.LogError("Could not set DeliveryCountry. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
                return View(DeliveryCountry);
            }

        }

        [HttpPost]
        public async Task<IActionResult> AddDeliveryCountry(DeliveryCountry DeliveryCountry)
        {
            _logger.LogInformation("POST: AddGate");
            if (ModelState.IsValid)
            {
                try
                {
                    await _configurationFacade.AddDeliveryCountry(DeliveryCountry);
                    return RedirectToAction("DeliveryCountries");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    return View(DeliveryCountry);
                }
            }
            else
            {
                return View(DeliveryCountry);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SetDefaultDeliveryCountry(DeliveryCountriesViewModel model)
        {
            _logger.LogInformation("POST: SetDefaultDeliveryCountry");
            await _configurationFacade.SetDefaultDeliveryCountry(model);
            return RedirectToAction("DeliveryCountries");
        }

        #endregion

        #region Displays
        public IActionResult Displays(string msg = "")
        {
            _logger.LogInformation("Config Displays");
            var model = _configurationFacade.GetDisplayIndexViewModel();
            ViewBag.ErrorMessage = msg;
            return View(model);
        }

        public IActionResult AddDisplay()
        {
            _logger.LogInformation("Config AddDisplay");
            return View(new DisplayConfiguration());
        }

        public IActionResult EditDisplay(Guid id)
        {
            _logger.LogInformation("Config EditDisplay");
            DisplayConfiguration model = null;
            try
            {
                model = _configurationFacade.GetDisplayConfiguration(id);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not show edit display View. Error: " + e.Message);
                ViewBag.ErrorMessage = "Ansicht zur Bearbeitung einer Anzeige konnte nicht geöffnet werden. Fehler: " + e.Message;
                return RedirectToAction("Displays", new { msg = ViewBag.ErrorMessage });
            }

            if (model == null)
            {
                _logger.LogError("Could not show EditDisplay view. Display with id: " + id + " couldnt be found.");
                var error = "Anzeige konnte nicht bearbeitet werden, da kein entsprechender Eintrag gefunden wurde.";
                return RedirectToAction("Displays", new { msg = error });
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddDisplay(DisplayConfiguration model)
        {
            _logger.LogInformation("POST: AddDisplay");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                await _configurationFacade.AddDisplay(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not add Display. Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
                return View(model);
            }

            return RedirectToAction("Displays");
        }

        [HttpPost]
        public async Task<IActionResult> EditDisplay(DisplayConfiguration model)
        {
            _logger.LogInformation("POST: EditDisplay id: " + model.ID);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                await _configurationFacade.EditDisplay(model);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not edit display with id: " + model.ID + " Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("Displays");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteDisplay(Guid id)
        {
            _logger.LogInformation("POST: DeleteDisplay id: " + id);

            try
            {
                await _configurationFacade.DeleteDisplay(id);
            }
            catch (Exception e)
            {
                _logger.LogError("Could not delete display with id: " + id + " Message: " + e.Message);
                ViewBag.ErrorMessage = e.Message;
            }
            return RedirectToAction("Displays");
        }
        #endregion

        #region User

        public IActionResult UserIndex(string msg = "")
        {
            _logger.LogInformation("Config User");
            ViewBag.ErrorMessage = msg;
            var model = _configurationFacade.GetUserIndexViewModel();
            return View(model);
        }

        public IActionResult AddUser()
        {
            _logger.LogInformation("Config AddUser");
            var model = _configurationFacade.GetUserViewModel();
            return View(model);
        }

        public async Task<IActionResult> EditUser(string id)
        {
            _logger.LogInformation("Config EditUser");
            var user = await _configurationFacade.GetUserViewModel(id);
            if (user != null)
            {
                return View(user);
            }
            else
            {
                _logger.LogError("User with id: " + id + " could not be found. Editing will be canceled.");
                return RedirectToAction("UserIndex", new { msg = "Benutzer wurde nicht gefunden!" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(UserViewModel model)
        {
            _logger.LogInformation("POST: AddUser");

            var valid = true;
            if (model != null && String.IsNullOrEmpty(model.Password))
            {
                ModelState.AddModelError(nameof(model.Password), "Das Passwort darf nicht leer sein.");
                valid = false;
            }

            if (ModelState.IsValid && valid)
            {
                try
                {
                    IdentityResult result = await _configurationFacade.AddUser(model);
                    if (result.Succeeded)
                        return RedirectToAction("UserIndex");
                    foreach (IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError("Error while trying to add User. Message: " + e.Message + " inner: " + e.InnerException?.Message);
                    ModelState.AddModelError("", e.Message);
                }
            }
            var grps = _configurationFacade.AuthorizationGroups();
            model.Groups = grps;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            _logger.LogInformation("POST: DeleteUser id: " + id);
            IdentityResult result = await _configurationFacade.DeleteUser(id);
            if (!result.Succeeded)
            {
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return RedirectToAction("UserIndex");
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(UserViewModel model)
        {
            _logger.LogInformation("POST: EditUser");

            if (ModelState.IsValid)
            {
                IdentityResult result = await _configurationFacade.EditUser(model, _userValidator, _passwordHasher, _passwordValidator);

                if (result.Succeeded)
                {
                    return RedirectToAction("UserIndex");
                }
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

            }
            var grps = _configurationFacade.AuthorizationGroups();
            model.Groups = grps;
            return View(model);
        }

        #endregion

        #region AuthorizationGroups

        public IActionResult AuthorizationGroups(string msg = "")
        {
            _logger.LogInformation("Config AuthorizationGroups");
            ViewBag.ErrorMessage = msg;
            var model = _configurationFacade.GetAuthorizationGroupsViewModel();
            return View(model);
        }

        public IActionResult AddAuthorizationGroup()
        {
            _logger.LogInformation("Config AddAuthorizationGroup");
            return View(new GroupViewModel { Group = new AuthorizationGroup() });
        }

        public IActionResult EditAuthorizationGroup(Guid id)
        {
            _logger.LogInformation("Config EditAuthorizationGroup id: " + id);
            var model = _configurationFacade.GetGroupViewModel(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddAuthorizationGroup(GroupViewModel model)
        {
            _logger.LogInformation("POST: AddAuthorizationGroup name: " + model?.Group?.Name);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await _configurationFacade.AddAuthorizationGroup(model);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
            }

            return RedirectToAction("AuthorizationGroups", new { msg = ViewBag.ErrorMessage });
        }

        [HttpPost]
        public async Task<IActionResult> EditAuthorizationGroup(GroupViewModel model)
        {
            _logger.LogInformation("POST: EditAuthorizationGroup id: " + model?.Group?.ID);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await _configurationFacade.EditAuthorizationGroup(model);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
            }

            return RedirectToAction("AuthorizationGroups", new { msg = ViewBag.ErrorMessage });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAuthorizationGroup(Guid id)
        {
            _logger.LogInformation("POST: DeleteAuthorizationGroup id: " + id);
            try
            {
                await _configurationFacade.DeleteAuthorizationGroup(id);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
            }

            return RedirectToAction("AuthorizationGroups", new { msg = ViewBag.ErrorMessage });
        }
        #endregion

        #region Active Directory

        public IActionResult ADSettings(string msg = "")
        {
            _logger.LogInformation("Config ActiveDirectory");
            ViewBag.ErrorMessage = msg;
            var model = _configurationFacade.GetADSettings();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SetADSettings(ADSettings settings)
        {
            try
            {
                await _configurationFacade.SetADSettings(settings);
            }
            catch (Exception e)
            {
                _logger.LogError("Error while setting ad settings. Message: " + e.Message);
                return RedirectToAction("ADSettings", new { msg = e.Message });
            }
            return RedirectToAction("ADSettings");
        }
        #endregion
    }
}