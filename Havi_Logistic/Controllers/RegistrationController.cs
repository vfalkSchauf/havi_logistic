﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC.BusinessLogic.Interfaces;
using MVC.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    /// <summary>
    /// Handles the process of registrating a vehicle.
    /// Note: the suggestion of forwarding agencies is done in the registrationHub using SignalR and js.
    /// </summary>
    [Authorize(Roles = "Registration")]
    public class RegistrationController : Controller
    {
        private readonly ILogger<RegistrationController> _logger;

        private readonly IRegistrationFacade _registrationFacade;


        public RegistrationController(ILogger<RegistrationController> logger, IRegistrationFacade registrationFacade)
        {
            _logger = logger;
            _registrationFacade = registrationFacade;
        }

        public IActionResult Index(string message = "")
        {
            ViewBag.Message = message;

            return View(_registrationFacade.GetRegistrationViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> AddRegistration(RegistrationViewModel model)
        {
            _logger.LogInformation("AddRegistration");
            if (ModelState.IsValid)
            {
                try
                {
                    await _registrationFacade.AddRegistrationFromViewModel(model);
                }
                catch (Exception e)
                {
                    return RedirectToAction("Index", new { message = "Fehler! " + e.Message });
                }

                return RedirectToAction("Index", new { message = "Anmeldung erfolgreich!" });
            }
            _logger.LogWarning("Didnt add Registration. Model was invalid.");
            return View("Index", _registrationFacade.GetRegistrationViewModel());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}