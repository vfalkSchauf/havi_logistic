﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC.Migrations
{
    public partial class forwardingagencyGate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DeliveryAndPickUpGate",
                table: "ForwardingAgencies",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryAndPickUpGate",
                table: "ForwardingAgencies");
        }
    }
}
