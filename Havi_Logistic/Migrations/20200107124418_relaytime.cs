﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC.Migrations
{
    public partial class relaytime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RelayTime",
                table: "Displays",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RelayTime",
                table: "Displays");
        }
    }
}
