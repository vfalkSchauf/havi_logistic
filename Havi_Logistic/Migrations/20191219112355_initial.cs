﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ADSettings",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UseAD = table.Column<bool>(nullable: false),
                    GenerateAccountsForNewADUsers = table.Column<bool>(nullable: false),
                    ServerIP = table.Column<string>(nullable: true),
                    DomainNames = table.Column<string>(nullable: true),
                    DomainUserName = table.Column<string>(nullable: true),
                    DomainUserPassword = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ADSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationGroups",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ADGroupName = table.Column<string>(nullable: true),
                    CanUseConfig = table.Column<bool>(nullable: false),
                    CanUseProcessing = table.Column<bool>(nullable: false),
                    CanUseHistory = table.Column<bool>(nullable: false),
                    CanUseRegistration = table.Column<bool>(nullable: false),
                    AllowPickup = table.Column<bool>(nullable: false),
                    AllowDeliver = table.Column<bool>(nullable: false),
                    AllowEntrance = table.Column<bool>(nullable: false),
                    SetEntrance = table.Column<bool>(nullable: false),
                    SetExit = table.Column<bool>(nullable: false),
                    SetGate = table.Column<bool>(nullable: false),
                    Export = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationGroups", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DeliveryCountries",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Country = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliveryCountries", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Displays",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    IPAddress = table.Column<string>(nullable: true),
                    Port = table.Column<int>(nullable: false),
                    TcpTimeoutInMs = table.Column<int>(nullable: false),
                    ModeBreakInMs = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Rows = table.Column<int>(nullable: false),
                    CharsPerLine = table.Column<int>(nullable: false),
                    TextAlign = table.Column<int>(nullable: false),
                    UsePaging = table.Column<bool>(nullable: false),
                    PagingTimespan = table.Column<byte>(nullable: false),
                    FlashWhenCalled = table.Column<bool>(nullable: false),
                    EntryRemovalType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Displays", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ForwardingAgencies",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DeliveryGate = table.Column<string>(nullable: true),
                    PickUpGate = table.Column<string>(nullable: true),
                    ERP_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForwardingAgencies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Gates",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    GateDirection = table.Column<int>(nullable: false),
                    GateType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gates", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GeneralSettings",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    DefaultDeliveryGate = table.Column<string>(nullable: true),
                    DefaultPickUpGate = table.Column<string>(nullable: true),
                    DefaultDeliveryCountry = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RegistrationInformation",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    LicensePlate = table.Column<string>(nullable: true),
                    LicensePlateCompressed = table.Column<string>(nullable: true),
                    ForwardingAgency = table.Column<string>(nullable: true),
                    JobType = table.Column<int>(nullable: false),
                    Transportnumber = table.Column<string>(nullable: true),
                    DeliveryCountry = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    ForwardingAgencyERPID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegistrationInformation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RegistrationTimes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    RegistratoinInformationID = table.Column<Guid>(nullable: false),
                    Gate = table.Column<string>(nullable: true),
                    IsLastEdited = table.Column<bool>(nullable: false),
                    TimeOfRegistration = table.Column<DateTime>(nullable: false),
                    TimeOfCall = table.Column<DateTime>(nullable: false),
                    TimeOfEntry = table.Column<DateTime>(nullable: false),
                    TimeOfExit = table.Column<DateTime>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegistrationTimes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnkownForwardingAgencies",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FirstAppereance = table.Column<DateTime>(nullable: false),
                    NumberOfAppereances = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnkownForwardingAgencies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    AuthorizationGroup = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationGroups_Name",
                table: "AuthorizationGroups",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryCountries_Country",
                table: "DeliveryCountries",
                column: "Country",
                unique: true,
                filter: "[Country] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ForwardingAgencies_Name",
                table: "ForwardingAgencies",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Gates_Name",
                table: "Gates",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Role",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UnkownForwardingAgencies_Name",
                table: "UnkownForwardingAgencies",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "User",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "User",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ADSettings");

            migrationBuilder.DropTable(
                name: "AuthorizationGroups");

            migrationBuilder.DropTable(
                name: "DeliveryCountries");

            migrationBuilder.DropTable(
                name: "Displays");

            migrationBuilder.DropTable(
                name: "ForwardingAgencies");

            migrationBuilder.DropTable(
                name: "Gates");

            migrationBuilder.DropTable(
                name: "GeneralSettings");

            migrationBuilder.DropTable(
                name: "RegistrationInformation");

            migrationBuilder.DropTable(
                name: "RegistrationTimes");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "UnkownForwardingAgencies");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
