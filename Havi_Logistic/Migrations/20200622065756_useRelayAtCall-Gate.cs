﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC.Migrations
{
    public partial class useRelayAtCallGate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "UseRelayAtCall",
                table: "Gates",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UseRelayAtCall",
                table: "Gates");
        }
    }
}
