﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MVC.BusinessLogic.Implementations;
using MVC.BusinessLogic.Interfaces;
using MVC.Controllers.SignalR;
using MVC.Data.DBContext;
using MVC.Data.Entities;
using MVC.Factories;
using MVC.HostedServices;
using MVC.Repositories.Implementations;
using MVC.Repositories.Interfaces;
using MVC.Validation;
using System;

namespace LKW3000_ODW_Elektrik
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });



            // SignalR
            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = true;
            });


            services.AddDbContext<ApplicationDBContext>(options =>
                        options.UseSqlServer(Configuration.GetConnectionString("ApplicationDBContext")), ServiceLifetime.Scoped);

            services.AddIdentity<AppUser, IdentityRole>(opts =>
            {
                opts.User.RequireUniqueEmail = false;
                opts.User.AllowedUserNameCharacters += "/\\";
                opts.Password.RequiredLength = 6;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = true;
                opts.Password.RequireUppercase = true;
                opts.Password.RequireDigit = true;

            }).AddEntityFrameworkStores<ApplicationDBContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<GermanIdentityErrorDescriber>();



            //Facades
            services.AddScoped<IRegistrationHubFacade, RegistrationHubFacade>();
            services.AddScoped<IRegistrationFacade, RegistrationFacade>();
            services.AddScoped<IConfigurationFacade, ConfigurationFacade>();
            services.AddScoped<IProcessingFacade, ProcessingFacade>();
            services.AddScoped<IDisplayFacade, DisplayFacade>();
            services.AddScoped<IProcessingHubFacade, ProcessingHubFacade>();
            services.AddScoped<IHistoryFacade, HistoryFacade>();
            services.AddScoped<IUnknownForwardingAgenciesRepository, EFUnknownForwardingAgenciesRepository>();
            services.AddScoped<ICsvReader, CsvReader>();
            services.AddScoped<IExportFacade, ExportFacade>();
            services.AddScoped<IADReader, ADReader>();
            services.AddScoped<IAccountFacade, AccountFacade>();

            // Repositorys
            services.AddScoped<IGeneralSettingsRepository, EFGeneralSettingsRepository>();
            services.AddScoped<IGatesRepository, EFGatesRepository>();
            services.AddScoped<IForwardingAgenciesRepository, EFForwardingAgenciesRepository>();
            services.AddScoped<IGeneralSettingsRepository, EFGeneralSettingsRepository>();
            services.AddScoped<IOpenRegistrationsRepository, EFOpenRegistrationsRepository>();
            services.AddScoped<IClosedRegistrationsRepository, EFClosedRegistrationsRepository>();
            services.AddScoped<IDisplayConfigurationRepository, EFDisplayConfigurationRepository>();
            services.AddScoped<IAuthorizationGroupsRepository, EFAuthorizationGroupsRepository>();
            services.AddScoped<IADSettingsRepository, EFADSettingsRepository>();
            services.AddScoped<IDeliveryCountriesRepository, EFDeliveryCountriesRepository>();

            services.AddSingleton<DBContextFactory>(c => new DBContextFactory(Configuration.GetConnectionString("ApplicationDBContext")));

            services.AddHostedService<DisplayService>();

            // Cookie athentication
            //services.AddAuthentication();

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.Cookie.Name = "LKW3000Cookie";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = "/Account/Login";
                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });

            services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Registration/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            //app.UseMvcWithDefaultRoute();
            // HAS TO BE BEFORE USE MVC!!!!
            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ProcessingHub>("/ProcessingHub");
                routes.MapHub<HistoryHub>("/HistoryHub");
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Registration}/{action=Index}/{id?}");
            });
            SeedData.CreateRolesAndAdmin(app.ApplicationServices).Wait();
        }
    }
}
