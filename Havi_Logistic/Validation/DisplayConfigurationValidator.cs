﻿using FluentValidation;
using MVC.Data.Entities;

namespace MVC.Validation
{
    public class DisplayConfigurationValidator : AbstractValidator<DisplayConfiguration>
    {
        public DisplayConfigurationValidator()
        {
            RuleFor(m => m.Name).NotEmpty().WithMessage("Der Name darf nicht leer sein.");
            RuleFor(m => m.IPAddress).NotEmpty().WithMessage("Die IP Adresse darf nicht leer sein.");
            RuleFor(m => m.Port).NotEmpty().WithMessage("Der Port muss angegeben werden.");
            RuleFor(m => m.Rows).NotEmpty().WithMessage("Die Anzal der Zeilen muss angegeben werden.");
            RuleFor(m => m.CharsPerLine).NotEmpty().WithMessage("Die Anzahl der Zeichen pro Zeile muss angegeben werden.");
        }
    }
}
