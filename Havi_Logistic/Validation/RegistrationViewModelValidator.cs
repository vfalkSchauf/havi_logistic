﻿using FluentValidation;
using MVC.Models;

namespace MVC.Validation
{
    public class RegistrationViewModelValidator : AbstractValidator<RegistrationViewModel>
    {
        public RegistrationViewModelValidator()
        {
            RuleFor(model => model.LicensePlate).NotEmpty().WithMessage("Das Kennzeichen darf nicht leer sein.");
            RuleFor(model => model.ForwardingAgency).NotEmpty().When(model => model.ForwardingAgencyUnknown == false).WithMessage("Die Spedition darf nicht leer sein. Wenn Sie die Spedition nicht bekannt ist, wählen Sie das entsprechende Auswahlfeld an.");
            RuleFor(model => model.Deliver).Must(m => m == true).When(model => model.PickUp == false).WithMessage("Es muss mindestens eine der Aktionen 'Anliefern' oder 'Abholen' ausgewählt sein.");
        }
    }
}
