﻿using FluentValidation;
using MVC.Models.ConfigurationViewModels;

namespace MVC.Validation
{
    public class GroupViewModelValidator : AbstractValidator<GroupViewModel>
    {
        public GroupViewModelValidator()
        {
            RuleFor(model => model.Group.Name).NotEmpty().WithMessage("Der Name darf nicht leer sein");
            RuleFor(model => model.Group.CanUseRegistration).NotEqual(false)
            .When(t => t.Group.CanUseProcessing.Equals(false))
            .When(t => t.Group.CanUseHistory.Equals(false))
            .When(t => t.Group.CanUseConfig.Equals(false))
            .WithMessage("Eine Berechtigungsgruppe muss mindestens auf eine Seite zugriffsrechte bekommen. Darum müssen Sie eine der folgenden Optionen auswählen: Anmeldung, Bearbeitung, Historie, Konfiguration");
        }
    }
}
