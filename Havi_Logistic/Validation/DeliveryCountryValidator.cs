﻿using FluentValidation;
using MVC.Data.Entities;

namespace MVC.Validation
{
    public class DeliveryCountryValidator : AbstractValidator<DeliveryCountry>
    {
        public DeliveryCountryValidator()
        {
            RuleFor(model => model.Country).NotEmpty().Matches("").WithMessage("Das Lieferland darf nicht leer sein.");
        }
    }
}