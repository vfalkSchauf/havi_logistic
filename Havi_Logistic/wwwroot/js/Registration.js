﻿$(document).ready(function () {
    setupInfoPopOver();
});

function setupInfoPopOver() {
    $('[data-toggle="popover"]').popover();
    var data = "<ul>";
    data += "<li>Das Kennzeichen ist ein Pflichtfeld.</li>";
    data += "<hr />";
    data += "<li>Die Spedition ist ebenfalls ein Pflichtfeld. Sollten Sie den Namen der Spedition nicht in der Liste finden, tragen Sie ihn einfach in das Feld ein. Sollte Ihnen der Name nicht bekannt sein, wählen Sie die entsprechende Option an.</li>";
    data += "<li>Sobald Ihnen Speditionsvorschläge zur Verfügung stehen, können Sie diese durch klick auf \"Auswählen\" aus einer Liste auswählen.</li>"
    data += "<hr />";
    data += "<li>Es muss mindestens eine der beiden Optionen \"Anliefern\" oder \"Abholen\" ausgewählt werden.</li>"
    data += "</ul>";
    var popover = $('#InfoPopover');
    popover.attr('data-content', data);
}

// -------------------------------------------------------

function disableSendButton()
{
    var button = document.getElementById("SendButton");
    if (button !== null && button !== undefined) {
        button.disabled = true;
    }
}

function resetForm(DefaultDeliveryCountry)
{
    var button = document.getElementById("SendButton");
    if (button !== null && button !== undefined) {
        button.disabled = false;
    }

    var licensePlateInput = document.getElementById("LicensePlateInput");
    if (licensePlateInput !== null && licensePlateInput !== undefined) {
        licensePlateInput.value = "";
    }

    var transportnumber = document.getElementById("TransportnumberInput");
    if (transportnumber !== null && transportnumber !== undefined) {
        transportnumber.value = "";
    }

    $('#DeliverCountryInput').val(DefaultDeliveryCountry)

    var forwardingAgency = document.getElementById("ForwardingAgencyInput");
    if (forwardingAgency !== null && forwardingAgency !== undefined) {
        forwardingAgency.value = "";
    }

    var deliverCheckBox = document.getElementById("DeliverCheckBox");
    if (deliverCheckBox !== null && deliverCheckBox !== undefined) {
        deliverCheckBox.checked = true;
    }

    var pickUpCheckBox = document.getElementById("PickUpCheckBox");
    if (pickUpCheckBox !== null && pickUpCheckBox !== undefined) {
        pickUpCheckBox.checked = false;
    }
    
    var comment = document.getElementById("CommentInput");
    if (comment !== null && comment !== undefined) {
        comment.value = "";
    }
}

function forwardingAgencyOptionSelected(item) {
    if (item === null || item === undefined)
        return;

    var inputElement = document.getElementById("ForwardingAgencyInput");
    if (inputElement !== null && inputElement !== undefined) {
        inputElement.value = item.innerText;
    }
}