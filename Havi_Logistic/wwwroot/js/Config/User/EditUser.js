﻿document.getElementById("User").className += " active";

$(document).ready(function () {
    setupInfoPopOver();
});

function setupInfoPopOver() {
    $('[data-toggle="popover"]').popover();
    var data = "<ul>";
    data += "<li>Der Benutzername ist ein Pflichtfeld und muss einzigartig sein.</li>";
    data += "<hr />";
    data += "<li>Das Passwort muss mindestens 6 Zeichen lang sein, eine Zahl, einen Groß- und einen Kleinbuchstaben enthalten</li>";
    data += "<li>Wenn Sie kein Passwort eintragen, bleibt das aktuelle Passwort unverändert.</li>";
    data += "<hr />";
    data += "<li>Mittels der Berechtigungsgruppe können Sie festlegen, welche Bereiche der Benutzer sehen darf und welche Aktionen ausgeführt werden dürfen.</li>";
    data += "</ul>";
    var popover = $('#InfoPopover');
    popover.attr('data-content', data);
}
