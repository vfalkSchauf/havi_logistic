﻿var connection = new signalR.HubConnectionBuilder().withUrl("/ProcessingHub").build();

var table = null;

$(document).ready(function () {
    table = $('#table').DataTable({
        "paging": true,
        "searching": true,
        "stateSave": true,
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [7, 10] }
        ],
        "order": [[5, "desc"]],
        "language": {
            "lengthMenu": "Zeige _MENU_ Einträge pro Seite",
            "zeroRecords": "Keine Einträge gefunden.",
            "info": "Zeige Seite _PAGE_ von _PAGES_",
            "infoEmpty": "Keine Einträge verfügbar",
            "paginate": {
                "previous": "Vorherige Seite",
                "next": "Nächste Seite"
            },
            "search": "Suche:"
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Alle"]],
        "dom": '<"top"f>rt<"bottom"lp><"clear">'
    });
    setupInfoPopOver();
    // Increase the time since registration by 1 every minute. Not 100% accurate with actual time but good enough. will be accurate at every reload of the page
    setInterval(updateTimeSinceRegistration, 60000);
});

connection.onclose(function () {
    setTimeout(function () {
        connection.start({ pingInterval: 6000 });
    }, 5000); // Restart connection after 5 seconds.
});

connection.start({ pingInterval: 6000 }).catch(function (err) {
    console.log("trying to restart SignalR connection.");
    setTimeout(function () {
        connection.start({ pingInterval: 6000 });
    }, 5000);
    return console.error(err.toString());
})
    .then(() => {
        connection.invoke("joinGroup", document.getElementById("ClientRole").value).catch(err => console.error(err.toString()));
    })

connection.on("Error", function (errorMessage) {
    confirm(errorMessage);
    lockInput(false);
    location.reload(true);
});


function enableEditing() {
    try {
        var enableEditingInput = document.getElementById("EnableEditing");
        var enableEditing = enableEditingInput.innerText;


        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();

            if (data !== undefined && data !== null) {
                var id = data.DT_RowId.replace("TR ", "");

                var licensePlateInput = document.getElementById("LicensePlate " + id);
                var commentInput = document.getElementById("Comment " + id);

                if (enableEditing == "Bearbeiten") {
                    data[0] = "<td><input id=\"LicensePlate " + id + "\" value=\"" + licensePlateInput.value + "\"  onchange=\"licensePlateChanged('" + id + "')\" /></td>";
                    data[13] = "<td><input id=\"Comment " + id + "\" value=\"" + commentInput.value + "\"  onchange=\"commentChanged('" + id + "')\" /></td>";
                } else {
                    data[0] = "<td><input id=\"LicensePlate " + id + "\" value=\"" + licensePlateInput.value + "\"  onchange=\"licensePlateChanged('" + id + "')\" disabled /></td>";
                    data[13] = "<td><input id=\"Comment " + id + "\" value=\"" + commentInput.value + "\"  onchange=\"commentChanged('" + id + "')\" disabled /></td>";
                }
            }
            this.data(data);
        });

        if (enableEditing == "Bearbeiten") {
            enableEditingInput.innerText = "Sperren";
        } else {
            enableEditingInput.innerText = "Bearbeiten";
        }

        table.draw();
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
}

function updateTimeSinceRegistration() {
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();

        if (data !== undefined && data !== null) {
            if (data.length >= 4) {
                // Increase the time since registration by 1. Not 100% accurate but good enough. will be exact when reloaded from the server.
                data[5] = parseInt(data[5]) + 1;
            }
        }
        this.data(data);
    });
    table.draw();
}

function redrawTable() {
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        this.data(data);
    });
    table.draw();
}

function setupInfoPopOver() {
    $('[data-toggle="popover"]').popover();
    var data = "<ul>";
    data += "<li style='background-color: aliceblue'>Signalisiert eine ausstehende Änderung.</li>";

    data += "<li style='background-color: yellow'>Signalisiert eine kürzliche Änderung.</li>";

    data += "<li style='background-color: lightgreen'>Signalisiert eine Ausfahrt.</li>";

    data += "<li style='background-color: lightblue'>Signalisiert einen neuen Eintrag.</li>";
    data += "<hr />";
    data += "<li>Die Art: <span style='background-color:lightgreen'>An</span> steht für eine Anlieferung.</li>";
    data += "<li>Die Art: <span style='background-color:lightcoral'>Ab</span> steht für eine Abholung.</li>";
    data += "<li>Die Art: <span style='background-color:lightblue'>An & Ab</span> steht für eine Anlieferung und Abholung.</li>";
    data += "</ul>";
    var popover = $('#InfoPopover');
    popover.attr('data-content', data);
}

function lockInput(lock) {
    var buttons = $('#table').find("button");
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].disabled = lock;
    }
    var selection = $('#table').find("select");
    for (var j = 0; j < selection.length; j++) {
        selection[j].disabled = lock;
    }
}

function licensePlateChanged(id) {
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var licensePlateInput = document.getElementById("LicensePlate " + id);
    var licensePlate = licensePlateInput.value;

    connection.invoke("UpdateLicensePlate", id, licensePlate).catch(function (err) {
        if (err.toString()) {
            alert("Fehler beim Ändern des Kommentars. " + err);
            console.error(err.toString());
        }
    });
}

connection.on("UpdateLicensePlate", function (id, licensePlate) {
    try {
        var enableEditingInput = document.getElementById("EnableEditing");
        var enableEditing = enableEditingInput.innerText;


        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();

            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    if (enableEditing == "Bearbeiten") {
                        data[0] = "<td><input id=\"LicensePlate " + id + "\" value=\"" + licensePlate + "\"  onchange=\"licensePlateChanged('" + id + "')\" /></td>";
                    } else {
                        data[0] = "<td><input id=\"LicensePlate " + id + "\" value=\"" + licensePlate + "\"  onchange=\"licensePlateChanged('" + id + "')\" disabled /></td>";
                    }
                }

            }
            this.data(data);
        });
        table.draw();

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});

function commentChanged(id) {
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var commentInput = document.getElementById("Comment " + id);
    var comment = commentInput.value;

    connection.invoke("UpdateComment", id, comment).catch(function (err) {
        if (err.toString()) {
            alert("Fehler beim Ändern des Kommentars. " + err);
            console.error(err.toString());
        }
    });
}

connection.on("UpdateComment", function (id, comment) {
    try {
        var enableEditingInput = document.getElementById("EnableEditing");
        var enableEditing = enableEditingInput.innerText;


        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();

            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    if (enableEditing != "Bearbeiten") {
                        data[13] = "<td><input id=\"Comment " + id + "\" value=\"" + comment + "\"  onchange=\"commentChanged('" + id + "')\" /></td>";
                    } else {
                        data[13] = "<td><input id=\"Comment " + id + "\" value=\"" + comment + "\"  onchange=\"commentChanged('" + id + "')\" disabled /></td>";
                    }
                }
            }
            this.data(data);
        });
        table.draw();


        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});


function gateSelected(sender, id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";

    var gate = sender.value;
    connection.invoke("SetGate", id, gate).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            // redraw to make sure the original gate is shown again.
            if (table !== null && table !== undefined) {
                redrawTable();
            }
            alert("Sie verfügen nicht über die benötigten Rechte, um das Tor zu verändern.");
        }
        else {
            alert("Fehler beim Ändern des Tores. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("SetGate", function (id, selectedGate, gatesString) {
    try {
        lockInput(false);

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();

            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    var deliverAndPickUp = document.getElementById("DeliverAndPickUp");
                    var pickUp = document.getElementById("PickUp");
                    var deliver = document.getElementById("Deliver");

                    var canSeePickUp = false;
                    var canSeeDeliver = false;

                    if (deliverAndPickUp !== null && deliverAndPickUp !== undefined || pickUp !== null && pickUp !== undefined) {
                        canSeePickUp = true;
                    }
                    if (deliverAndPickUp !== null && deliverAndPickUp !== undefined || deliver !== null && deliver !== undefined) {
                        canSeeDeliver = true;
                    }

                    //<td>
                    //    <select class="form-control" id="gateSelect @regist.ID" onchange="gateSelected(this, '@regist.ID')" size="1" style="min-width:70px;">
                    //        @{
                    //                            var found = false;
                    //                        }
                    //                        @foreach (var gate in Model.Gates)
                    //                        {

                    //                            if (times.Where(x => x.IsLastEdited == true).FirstOrDefault().Gate == gate.Name)
                    //                            {
                    //            <option selected>@gate.Name</option>
                    //                                found = true;
                    //                            }
                    //                            else
                    //                            {
                    //            <option>@gate.Name</option>
                    //        }

                    //                        }
                    //                        @if (!found)
                    //                        {
                    //            <option selected>@times.Where(x => x.IsLastEdited == true).FirstOrDefault().Gate</option>
                    //        }
                    //    </select>
                    //</td>


                    var gateSelect = [];
                    //var gatesAsArray = gatesString.split(";");
                    //var test = gatesAsArray[0].name;
                    //var i;
                    //for (i = 0; i < gatesAsArray.length; i++) {
                    //    gates += JSON.parse(gatesAsArray[i]);
                    //}
                    
                        var gates = JSON.parse(gatesString);

                    gateSelect.push("<td><select class=\"form-control\" id=\"gateSelect " + id + "\" onchange=\"gateSelected(this, '" + id + "')\" size=\"1\" style=\"min-width:70px;\">");


                    //for (index = 0; index < gates.length; index++) {
                    for (var i = 0; i < gates.length; i++) {
                        if (gates[i].name === selectedGate) {
                            gateSelect.push("<option selected>" + gates[i].name + "</option>")
                        }
                        else if (canSeePickUp && canSeeDeliver) {
                            gateSelect.push("<option>" + gates[i].name + "</option>")
                        }
                        else if (gates[i].gateType === "Deliver" && canSeeDeliver) {
                            gateSelect.push("<option>" + gates[i].name + "</option>")
                        }
                        else if (gates[i].gateType === "PickUp" && canSeePickUp) {
                            gateSelect.push("<option>" + gates[i].name + "</option>")
                        }
                        else if (gates[i].gateType === "DeliverAndPickUp" && (canSeePickUp || canSeeDeliver)) {
                            gateSelect.push("<option>" + gates[i].name + "</option>")
                        }
                    };

                    //if (!gates.includes(selectedGate)) {
                    //    gateSelect.push("<option selected> </option>")
                    //}
                    gateSelect.push("</select>");
                    data[7] = gateSelect.join("");

                    //var select = data[7];
                    //var split = select.split(">");
                    //var stop = split;
                    //var vals = new Array();
                    //for (var i = 2; i < split.length - 2; i += 2) {
                    //    var val = split[i].split("<")[0];
                    //    vals.push(val);
                    //}
                    //var newData = split[0] + ">";
                    //for (var i = 0; i < vals.length; i++) {
                    //    if (vals[i] == value) {
                    //        newData += "<option selected>" + value + "</option>";
                    //    }
                    //    else {
                    //        newData += "<option>" + vals[i] + "</option>";
                    //    }
                    //}
                    //newData += "</select>"
                    //data[7] = newData;
                }
            }
            this.data(data);
        });
        table.draw();

        //var select = document.getElementById("gateSelect " + id);
        //if (select === null || select === undefined) {
        //    console.error("Couldnt find select html element.");
        //    return;
        //}
        //for (var i = 0; i < select.options.length; i++) {
        //    var option = select.options[i];
        //    if (option === null || option === undefined)
        //        continue;
        //    if (option.value === value) {
        //        select.selectedIndex = i;
        //        break;
        //    }
        //}
        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});


function call(id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";
    connection.invoke("Call", id).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            alert("Sie verfügen nicht über die benötigten Rechte, um ein Fahrzeug aufzurufen.");
        }
        else {
            alert("Fehler beim Aufruf. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("SetCall", function (id, serverTime) {
    try {
        lockInput(false);
        var time = new Date(serverTime);
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    data[8] = "<td id='Call " + id + "' style='text-align: center;'><p style='text-align:center'><span onclick='toggleFullDate(this, \"" + getLongDate(time) + ":" + time.getSeconds() + "\",\"" + getShortDate(time) + "\");'>" + getShortDate(time) + "</span></p></td>";
                    data[9] = "<button id='WithdrawCall " + id + "' style='align-self:start' class='btn btn-dark' onclick='withdrawCall(\"" + id + "\")'>X</button>";
                    data[10] = "<td id='Entry " + id + "' style='text-align:center;'><button class='btn btn-primary' onclick='setEntry(\"" + id + "\")'>Einfahrt</button></td >"
                }
            }
            this.data(data);
        });
        table.draw();

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});

function withdrawCall(id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";
    connection.invoke("WithdrawCall", id).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            alert("Sie verfügen nicht über die benötigten Rechte, um den Aufruf eines Fahrzeuges zurückzuziehen.");
        }
        else {
            alert("Fehler beim Zurückziehen. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("WithdrawCall", function (id) {
    try {
        lockInput(false);
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    data[8] = "<td id='Call " + id + "' style='text-align: center;'><button class='btn btn-success' onclick='call(\"" + id + "\")'>Aufruf</button></td>"
                    data[9] = "<button id='WithdrawCall @regist.ID' class='btn btn-dark' disabled onclick='withdrawCall(\"" + id + "\")'>X</button>";
                    data[10] = "<td id='Entry " + id + "' style='text-align:center;'><button class='btn btn-primary' disabled onclick='setEntry(\"" + id + "\")'>Einfahrt</button></td >"
                }
            }
            this.data(data);
        });
        table.draw();

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});

function setEntry(id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";
    connection.invoke("SetEntry", id).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            alert("Sie verfügen nicht über die benötigten Rechte, um die Einfahrt zu setzten.");
        }
        else {
            alert("Fehler beim Setzten der Einfahrt. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("SetEntry", function (id, serverTime) {
    try {
        lockInput(false);
        var time = new Date(serverTime);

        var td = document.getElementById("Entry " + id);
        if (td === null || td === undefined)
            return;

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    data[10] = "<td id='Entry " + id + "' style='text-align: center;'><p style='text-align:center'><span onclick='toggleFullDate(this, \"" + getLongDate(time) + ":" + time.getSeconds() + "\",\"" + getShortDate(time) + "\");'>" + getShortDate(time) + "</span></p></td>";
                    data[11] = "<button id='WithdrawEntry @regist.ID' class='btn btn-dark' onclick='withdrawEntry(\"" + id + "\")'>X</button>";
                    data[9] = "<button id='WithdrawCall @regist.ID' class='btn btn-dark' disabled onclick='withdrawCall(\"" + id + "\")'>X</button>";
                }
            }
            this.data(data);
        });
        table.draw();

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
            // location.reload();
        }, 3000);

    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});

function withdrawEntry(id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";
    connection.invoke("WithdrawEntry", id).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            alert("Sie verfügen nicht über die benötigten Rechte, um den Aufruf eines Fahrzeuges zurückzuziehen.");
        }
        else {
            alert("Fehler beim Zurückziehen. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("WithdrawEntry", function (id) {
    try {
        lockInput(false);
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    data[10] = "<td id='Entry " + id + "' style='text-align:center;'><button class='btn btn-primary' onclick='setEntry(\"" + id + "\")'>Einfahrt</button></td >"
                    data[11] = "<button id='WithdrawEntry @regist.ID' class='btn btn-dark' disabled onclick='withdrawEntry(\"" + id + "\")'>X</button>";
                    data[9] = "<button id='WithdrawCall " + id + "' style='align-self:start' class='btn btn-dark' onclick='withdrawCall(\"" + id + "\")'>X</button>";
                }
            }
            this.data(data);
        });
        table.draw();

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;
        var previousStyle = row.style;
        row.style = "background-color:yellow";
        setTimeout(function () {
            row.style = previousStyle;
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});

function getLongDate(time) {
    if (time === null || time === undefined)
        return "";
    return "" + time.getDate() + "." + (time.getMonth() + 1) + "." + time.getFullYear() + " " + getShortDate(time);
}

function getShortDate(time) {
    if (time === null || time === undefined)
        return "";
    var hours = time.getHours() < 10 ? "0" + time.getHours() : "" + time.getHours();
    var mins = time.getMinutes() < 10 ? "0" + time.getMinutes() : "" + time.getMinutes();
    return "" + hours + ":" + mins;
}

function toggleFullDate(sender, longStr, shortStr) {
    if (sender === null || sender === undefined)
        return;
    if (sender.innerText === longStr)
        sender.innerText = shortStr;
    else
        sender.innerText = longStr;
}

function setExit(id) {
    lockInput(true);
    var row = document.getElementById("TR " + id);
    if (row === null || row === undefined)
        return;

    var oldStyle = row.style;
    row.style = "background-color:aliceblue";
    connection.invoke("SetExit", id).catch(function (err) {
        if (err.toString().includes("unauthorized")) {
            alert("Sie verfügen nicht über die benötigten Rechte, um die Ausfahrt zu setzten.");
        }
        else {
            alert("Fehler beim Setzten der Ausfahrt. " + err);
            console.error(err.toString());
        }
        lockInput(false);
        row.style = oldStyle;
    });
}

connection.on("SetExit", function (id, serverTime) {
    try {
        lockInput(false);
        var time = new Date(serverTime);
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data !== undefined && data !== null) {
                if (data.DT_RowId === "TR " + id) {
                    data[12] = "<p style='text-align:center'><span onclick='toggleFullDate(this, \"" + getLongDate(time) + ":" + time.getSeconds() + "\",\"" + getShortDate(time) + "\");'>" + getShortDate(time) + "</span></p>";
                }
            }
            this.data(data);
        });
        table.draw();

        var td = document.getElementById("Exit " + id);
        if (td === null || td === undefined)
            return;

        while (td.firstChild) {
            td.removeChild(td.firstChild);
        }

        var span = document.createElement("span");
        span.onclick = function () {
            toggleFullDate(span, getLongDate(time) + ":" + time.getSeconds(), getShortDate(time));
        };
        span.innerText = getShortDate(time);

        td.appendChild(span);

        var row = document.getElementById("TR " + id);
        if (row === null || row === undefined)
            return;

        row.style = "background-color:lightgreen";
        setTimeout(function () {
            table.row(row).remove().draw();
            //location.reload();
        }, 3000);
    }
    catch (err) {
        console.error(err.toString());
        alert(err.toString());
    }
});


connection.on("AddRegistration", function (model) {

    if (table === null || table === undefined)
        return;

    var regist = model.registration;

    var userRole = document.getElementById("ClientRole").value;
    var gates;

    if (userRole === "DeliverAndPickUp")
        gates = model.gates;
    else if (userRole === "Deliver")
        gates = model.gates.filter(function (item) {
            return item.gateType === 1 || item.gateType === 0;
        })
    else if (userRole === "PickUp")
        gates = model.gates.filter(function (item) {
            return item.gateType === 2 || item.gateType === 0;
        })

    var columns = new Array(11);
    var gate = model.times.find(x => x.isLastEdited === true).gate;
    var timeOfRegistration = new Date(model.times.find(x => x.isLastEdited === true).timeOfRegistration);
    var id = regist.id;

    columns[0] = regist.licensePlate === null ? "" : regist.licensePlate;

    columns[1] = regist.forwardingAgency === null ? "" : regist.forwardingAgency;

    columns[2] = regist.transportnumber === null ? "" : regist.transportnumber;

    columns[3] = regist.deliveryCountry === null ? "" : regist.deliveryCountry;

    var p1 = '<span onclick="toggleFullDate(this,';
    var p2 = "\'" + getLongDate(timeOfRegistration) + "\',";
    var p3 = "\'" + getShortDate(timeOfRegistration) + "\')\">";
    var p4 = "" + getShortDate(timeOfRegistration) + "</span>";
    columns[4] = p1 + p2 + p3 + p4;

    // 0 because the registration just happened anyways. Thats the hole point of this anyawys
    columns[5] = "0";

    var span = "";

    switch (regist.jobType) {
        case 0:
            span = "<div style='background-color: lightblue; text-align: center;'>An & Ab</div>";
            break;

        case 1:
            span = "<div style='background-color: lightgreen; text-align: center;'>An</div>";
            break;

        case 2:
            span = "<div style='background-color: lightcoral; text-align: center;'>Ab</div>";
            break;


        default:
            "?";
            break;
    }

    columns[6] = span;

    var gatesSelect = "<select class='form-control' id='gateSelect " + id + "' onchange='gateSelected(this, " + id + ")' size='1' style='min-width: 70px;'>";
    var found = false;
    for (var i = 0; i < gates.length; i++) {
        if (gate === gates[i].name) {
            gatesSelect += "<option selected>" + gates[i].name + "</option>";
            found = true;
        }
        else {
            gatesSelect += "<option>" + gates[i].name + "</option>";
        }
    }
    if (!found) {
        gatesSelect += "<option selected>" + gate + "</option>";
    }
    gatesSelect += "</select>";
    columns[7] = gatesSelect;

    //<td id='Call " + id + "' style='text-align:center'>
    columns[8] = "<button class='btn btn-success' onclick='call(" + id + ")'>Aufruf</button>";
    columns[9] = "<button id='WithdrawCall @regist.ID' class='btn btn-dark' disabled onclick='withdrawCall(\"" + id + "\")'>X</button>";

    columns[10] = "<button class='btn btn-primary' disabled onclick='setEntry(" + id + ")'>Einfahrt</button>";
    columns[11] = "<button id='WithdrawEntry @regist.ID' class='btn btn-dark' disabled onclick='withdrawEntry(\"" + id + "\")'>X</button>";

    columns[12] = "<button class='btn btn-danger' onclick='setExit(" + id + ")'>Ausfahrt</button>";
    columns[13] = regist.comment === null ? "" : regist.comment;

    var row = table.row.add(columns).node();
    row.id = 'TR ' + id;
    row.children[4].style.textAlign = "center";

    row.children[5].style.textAlign = "center";


    row.children[8].id = "Call " + id;
    row.children[8].style.textAlign = "center";

    row.children[10].id = "Entry " + id;
    row.children[10].style.textAlign = "center";

    row.children[12].id = "Exit " + id;
    row.children[12].style.textAlign = "center";

    var previousStyle = row.style;
    row.style = "background-color:lightblue";
    setTimeout(function () {
        row.style = previousStyle;
        location.reload();
    }, 3000);

    table.draw(true);
});
