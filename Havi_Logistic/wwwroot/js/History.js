﻿var connection = new signalR.HubConnectionBuilder().withUrl("/HistoryHub").build();

var table = null;
var oldData = null;

connection.onclose(function () {
    setTimeout(function () {
        connection.start({ pingInterval: 6000 });
    }, 5000); // Restart connection after 5 seconds.
});

connection.start({ pingInterval: 6000 }).catch(function (err) {
    console.log("trying to restart SignalR connection.");
    setTimeout(function () {
        connection.start({ pingInterval: 6000 });
    }, 5000);
    return console.error(err.toString());
})
    .then(() => {
        connection.invoke("joinGroup", document.getElementById("ClientRole").value).catch(err => console.error(err.toString()));
    })

connection.on("Error", function (errorMessage) {
    confirm(errorMessage);
    location.reload(true);
});


$(document).ready(function () {
    table = $('#table').DataTable({
        "paging": true,
        "searching": true,
        "stateSave": true,
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [] }
        ],
        "order": [[3, "desc"]],
        "language": {
            "lengthMenu": "Zeige _MENU_ Einträge pro Seite",
            "zeroRecords": "Keine Einträge gefunden.",
            "info": "Zeige Seite _PAGE_ von _PAGES_",
            "infoEmpty": "Keine Einträge verfügbar",
            "paginate": {
                "previous": "Vorherige Seite",
                "next": "Nächste Seite"
            },
            "search": "Suche:"
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Alle"]],
        "dom": '<"top"f>rt<"bottom"lp><"clear">'
    });
    setupInfoPopOver();
    filterTable();
});

function exportSelection() {
    if (table === null || table === undefined)
        return;
    var data = table.rows().data();

    var csvContent = "data: text/csv;charset=utf-8,";

    var keys = "Licenseplate,ForwardingAgency,ERPID,TimeOfRegistration,Duration,Type,Gate,TimeOfCall,TimeOfEntry,TimeOfExit";
    csvContent += keys;

    for (var i = 0; i < data.length; i++) {
        var row = data[i];
        var rowCsv = "\n";

        rowCsv += "\"" + row[0] + "\"" + ",";
        rowCsv += "\"" + row[1] + "\"" + ",";
        rowCsv += row[2] + ",";
        rowCsv += row[3] + ",";
        rowCsv += row[4] + ",";

        var type = row[5].split(">");
        type = type[1].split("<")[0];
        rowCsv += type + ",";

        var gate = row[6].split(">");
        gate = gate[1].split("<")[0];
        rowCsv += gate + ",";

        rowCsv += row[7] + ",";
        rowCsv += row[8] + ",";
        rowCsv += row[9];

        csvContent += rowCsv;
    }

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "AuswahlHistorieExport.csv");
    document.body.appendChild(link); // Required for FormFile

    link.click(); // This will download the data file named "my_data.csv".
}


// called from the _layout.cshtml
function filterTable() {
    if (table === null || table === undefined)
        return;
    if (oldData === null || oldData === undefined || oldData.length === 0)
        oldData = table.rows().data();

    var newData = new Array();

    var showHistory = document.getElementById("HistoryCheck").checked;
    var licensePlate = document.getElementById("LicenseplateInput").value;
    var forwardingAgency = document.getElementById("ForwardingAgencyInput").value;
    var erpID = document.getElementById("ERPIDInput").value;
    var transportnumber = document.getElementById("TransportnumberInput").value;
    var deliveryCountry = document.getElementById("DeliveryCountryInput").value;
    var registMin = document.getElementById("RegistrationMin").value;
    var registMax = document.getElementById("RegistrationMax").value;
    var durationMin = document.getElementById("DurationMinInput").value;
    var durationMax = document.getElementById("DurationMaxInput").value;
    var type = document.getElementById("TypeSelect").value;
    var gate = document.getElementById("GateInput").value;
    var callMin = document.getElementById("CallMin").value;
    var callMax = document.getElementById("CallMax").value;
    var entryMin = document.getElementById("EntryMin").value;
    var entryMax = document.getElementById("EntryMax").value;
    var exitMin = document.getElementById("ExitMin").value;
    var exitMax = document.getElementById("ExitMax").value;
    var comment = document.getElementById("CommentInput").value;

    for (var i = 0; i < oldData.length; i++) {
        if (oldData[i] !== null && oldData[i] !== undefined) {
            var addToNewData = true;
            var row = oldData[i];

            if (showHistory === false) {
                if (row[11] === "-" || row[11] === " - ") {
                    addToNewData = false;
                }
            }


            if (licensePlate !== null && licensePlate !== "") {
                if (!row[0].toUpperCase().includes(licensePlate.toUpperCase())) {
                    addToNewData = false;
                }
            }

            if (forwardingAgency !== null && forwardingAgency !== "") {
                if (!row[1].toUpperCase().includes(forwardingAgency.toUpperCase())) {
                    addToNewData = false;
                }
            }

            if (erpID !== null && erpID !== "") {
                try {
                    var erpIDInt = parseInt(erpID);
                    var dataErpID = parseInt(row[2]);
                    if (erpIDInt !== dataErpID)
                        addToNewData = false;
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (transportnumber !== null && transportnumber !== "") {
                if (!row[3].toUpperCase().includes(transportnumber.toUpperCase())) {
                    addToNewData = false;
                }
            }

            if (deliveryCountry !== null && deliveryCountry !== "") {
                if (!row[4].toUpperCase().includes(deliveryCountry.toUpperCase())) {
                    addToNewData = false;
                }
            }

            if (registMin !== null && registMin !== "") {
                try {
                    if (row[5] === " - " || row[5] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var registDate = new Date(registMin);
                        var dateAndTimeSplit = row[5].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataRegistDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataRegistDate < registDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (registMax !== null && registMax !== "") {
                try {
                    if (row[5] === " - " || row[5] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var registDate = new Date(registMax);
                        var dateAndTimeSplit = row[5].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataRegistDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataRegistDate > registDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (durationMin !== null && durationMin !== "") {
                try {
                    var duration = parseInt(durationMin);
                    var dataDuration = parseInt(row[6]);
                    if (dataDuration < duration)
                        addToNewData = false;
                }
                catch (e) {
                    addToNewData = false;
                }
            }
            if (durationMax !== null && durationMax !== "") {
                try {
                    var duration = parseInt(durationMax);
                    var dataDuration = parseInt(row[6]);
                    if (dataDuration > duration)
                        addToNewData = false;
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (type !== null && type !== "" && type !== "*") {
                if (type === "An & Ab")
                    type = "An &amp; Ab";
                var dataType = row[7].split(">");
                dataType = dataType[1].split("<")[0];
                if (dataType !== type)
                    addToNewData = false;
            }

            if (gate !== null && gate !== "") {
                var dataGate = row[8].split(">")[1];
                dataGate = dataGate.split("<")[0];
                if (gate !== dataGate)
                    addToNewData = false;
            }

            if (callMin !== null && callMin !== "") {
                try {
                    if (row[9] === " - " || row[9] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var callDate = new Date(callMin);
                        var dateAndTimeSplit = row[9].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataCallDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataCallDate < callDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }
            if (callMax !== null && callMax !== "") {
                try {
                    if (row[9] === " - " || row[9] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var callDate = new Date(callMax);
                        var dateAndTimeSplit = row[9].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataCallDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataCallDate > callDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (entryMin !== null && entryMin !== "") {
                try {
                    if (row[10] === " - " || row[10] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var entryDate = new Date(entryMin);
                        var dateAndTimeSplit = row[10].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataEntryDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataEntryDate < entryDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }
            if (entryMax !== null && entryMax !== "") {
                try {
                    if (row[10] === " - " || row[10] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var entryDate = new Date(entryMax);
                        var dateAndTimeSplit = row[10].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataEntryDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataEntryDate > entryDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (exitMin !== null && exitMin !== "") {
                try {
                    if (row[11] === " - " || row[11] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var exitDate = new Date(exitMin);
                        var dateAndTimeSplit = row[11].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataExitDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataExitDate < exitDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }
            if (exitMax !== null && exitMax !== "") {
                try {
                    if (row[11] === " - " || row[11] === "-") {
                        addToNewData = false;
                    }
                    else {
                        var exitDate = new Date(exitMax);
                        var dateAndTimeSplit = row[11].split(" ");
                        var dateSplit = dateAndTimeSplit[0].split(".");

                        var dataExitDate = new Date(dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + "T" + dateAndTimeSplit[1]);
                        if (dataExitDate > exitDate)
                            addToNewData = false;
                    }
                }
                catch (e) {
                    addToNewData = false;
                }
            }

            if (comment !== null && comment !== "") {
                if (!row[12].toUpperCase().includes(comment.toUpperCase())) {
                    addToNewData = false;
                }
            }

            if (addToNewData) {
                newData.push(oldData[i]);
            }
        }
    }
    table.clear();
    table.rows.add(newData);
    table.draw(true);
}


function resetFilter() {
    if (oldData === null || oldData === undefined || oldData.length === 0 || table === null || table === undefined) {
        location.reload(true);
    }
    table.clear();
    table.rows.add(oldData);
    table.draw(true);

    document.getElementById("HistoryCheck").checked = false;
    document.getElementById("LicenseplateInput").value = "";
    document.getElementById("ForwardingAgencyInput").value = "";
    document.getElementById("ERPIDInput").value = "";
    document.getElementById("TransportnumberInput").value = "";
    document.getElementById("DeliveryCountryInput").value = "";
    document.getElementById("RegistrationMin").value = "";
    document.getElementById("RegistrationMax").value = "";
    document.getElementById("DurationMinInput").value = "";
    document.getElementById("DurationMaxInput").value = "";
    document.getElementById("TypeSelect").selectedIndex = 0;
    document.getElementById("GateInput").value = "";
    document.getElementById("CallMin").value = "";
    document.getElementById("CallMax").value = "";
    document.getElementById("EntryMin").value = "";
    document.getElementById("EntryMax").value = "";
    document.getElementById("ExitMin").value = "";
    document.getElementById("ExitMax").value = "";
    document.getElementById("CommentInput").value = "";

    oldData = null;

    filterTable();
}

function setupInfoPopOver() {
    $('[data-toggle="popover"]').popover();
    var data = "<ul>";
    data += "<li>Um eine Auswahl von Einträgen zu Exportieren, nutzen Sie die Funktion \"Erweitert Filtern\" um bestimmte Datensätze auszuwählen. Anschließend können Sie diese Auswahl, durch Klick auf \"Auswahl exportieren\", als CSV-Datei Herunterladen.</li>"
    data += "<hr />";
    data += "<li>Die Art: <span style='background-color:lightgreen'>An</span> steht für eine Anlieferung.</li>";
    data += "<li>Die Art: <span style='background-color:lightcoral'>Ab</span> steht für eine Abholung.</li>";
    data += "<li>Die Art: <span style='background-color:lightblue'>An & Ab</span> steht für eine Anlieferung und Abholung.</li>";
    data += "</ul>";
    var popover = $('#InfoPopover');
    popover.attr('data-content', data);
}

function getShortDate(time) {
    if (time === null || time === undefined)
        return "";
    var hours = time.getHours() < 10 ? "0" + time.getHours() : "" + time.getHours();
    var mins = time.getMinutes() < 10 ? "0" + time.getMinutes() : "" + time.getMinutes();
    var secs = time.getSeconds() < 10 ? "0" + time.getSeconds() : "" + time.getSeconds();
    return "" + hours + ":" + mins + ":" + secs;
}

function getLongDate(time) {
    if (time === null || time === undefined)
        return "";
    return "" + time.getDate() + "." + (time.getMonth() + 1) + "." + time.getFullYear() + " " + getShortDate(time);
}

connection.on("AddEntry", function (regist, times) {
    if (table === null || table === undefined)
        return;


    var id = regist.id;

    var i;
    for (i = times.length - 1; i >= 0; i--) {
        var columns = new Array(10);

        columns[0] = regist.licensePlate === null ? "" : regist.licensePlate;

        columns[1] = regist.forwardingAgency === null ? "" : regist.forwardingAgency;

        columns[2] = regist.forwardingAgencyERPID;

        columns[3] = regist.transportnumber === null ? "" : regist.transportnumber;

        columns[4] = regist.deliveryCountry === null ? "" : regist.deliveryCountry;

        var registTime = new Date(times[i].timeOfRegistration);
        columns[5] = getLongDate(registTime);

        if (times[i].timeOfExit !== "0001-01-01T00:00:00") {
            var exitTime = new Date(times[i].timeOfExit);
            var diff = Math.abs(exitTime - registTime);
            var timeSpent = Math.floor((diff / 1000) / 60);
            columns[6] = timeSpent;
        } else {
            columns[6] = 0;
        }

        var span = "";
        switch (regist.jobType) {
            case 0:
                span = "<div style='background-color: lightgreen; text-align: center;'>An</div>";
                break;

            case 1:
                span = "<div style='background-color: lightcoral; text-align: center;'>Ab</div>";
                break;

            case 2:
                span = "<div style='background-color: lightblue; text-align: center;'>An & Ab</div>";
                break;
            default:
                "?";
                break;
        }
        columns[7] = span;

        columns[8] = times[i].gate;


        var call = getLongDate(new Date(times[i].timeOfCall));
        columns[9] = call === "1.1.1 00:00:00" ? " - " : call;

        var entry = getLongDate(new Date(times[i].timeOfEntry));
        columns[10] = entry === "1.1.1 00:00:00" ? " - " : entry;

        var exit = getLongDate(new Date(times[i].timeOfExit));
        columns[11] = exit === "1.1.1 00:00:00" ? " - " : exit;

        columns[12] = regist.comment === null ? "" : regist.comment;

        var row = table.row.add(columns).node();

        var previousStyle = row.style;
        row.style = "background-color:lightblue";
        changeColor(row, previousStyle);

        oldData.push(columns);
    }

    table.draw(true);
    //oldData = table.rows().data();

    filterTable();
});

function changeColor(row, style) {
    setTimeout(row.style = style, 3000);
}