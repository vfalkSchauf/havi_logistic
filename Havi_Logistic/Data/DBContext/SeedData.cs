﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Data.DBContext
{
    /// <summary>
    /// Used to seed the DB with some necessary objects.
    /// </summary>
    public static class SeedData
    {
        public static void Initialize(ApplicationDBContext context)
        {
            if (!context.GeneralSettings.Any())
            {
                context.GeneralSettings.Add(new Entities.GeneralSettings
                {
                    DefaultDeliveryGate = "",
                    DefaultPickUpGate = ""
                });
            }

            if (!context.ADSettings.Any())
            {
                context.ADSettings.Add(new Entities.ADSettings
                {
                    DomainNames = null,
                    DomainUserName = null,
                    DomainUserPassword = null,
                    ServerIP = null,
                    UseAD = true,
                    GenerateAccountsForNewADUsers = true
                });
            }

            if (!context.AuthorizationGroups.Any())
            {
                context.AuthorizationGroups.Add(new Entities.AuthorizationGroup
                {
                    Name = "Administatoren",
                    ADGroupName = "",
                    AllowEntrance = true,
                    CanUseConfig = true,
                    CanUseHistory = true,
                    CanUseProcessing = true,
                    CanUseRegistration = true,
                    AllowDeliver = true,
                    AllowPickup = true,
                    Export = true,
                    SetEntrance = true,
                    SetExit = true,
                    SetGate = true
                });
            }

            if (!context.DeliveryCountries.Any())
            {
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Belgien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Bulgarien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Dänemark" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Deutschland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Estland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Finnland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Frankreich" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Griechenland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Grossbritannien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Holland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Italien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Irland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Kroatien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Lettland" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Litauen" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Luxemburg" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Malta" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Österreich" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Polen" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Portugal" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Rumänien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Schweden" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Slowakei" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Slowenien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Spanien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Tschechien" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Ungarn" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Zypern" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Liechtenstein" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Island" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Norwegen" });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Schweiz " });
                context.DeliveryCountries.Add(new DeliveryCountry { Country = "Turkei " });
            }

            context.SaveChanges();
        }

        public static async Task CreateRolesAndAdmin(IServiceProvider serviceProvider)
        {
            var roles = new List<string> { "Registration", "Processing", "History", "Configuration", "Gate", "Call", "DeliverAndPickUp", "WithdrawCall", "Entry", "WithdrawEntry", "Export", "Exit", "Deliver", "PickUp" };
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            foreach (var role in roles)
            {
                if (await roleManager.FindByNameAsync(role) == null)
                    await roleManager.CreateAsync(new IdentityRole(role));
            }


            var admin = new AppUser
            {
                UserName = "admin",
                Email = "admin@schauf.eu",
                AuthorizationGroup = "Administatoren"
            };
            var userManager = serviceProvider.GetRequiredService<UserManager<AppUser>>();
            if (await userManager.FindByNameAsync("admin") == null)
            {
                var result = await userManager.CreateAsync(admin, "Schauf!1");
                if (result.Succeeded)
                {
                    await userManager.AddToRolesAsync(admin, roles);
                }
            }
        }
    }
}
