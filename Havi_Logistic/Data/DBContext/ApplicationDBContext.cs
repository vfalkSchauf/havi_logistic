﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MVC.Data.Entities;

namespace MVC.Data.DBContext
{
    public class ApplicationDBContext : IdentityDbContext<AppUser>
    {
        public DbSet<OpenRegistration> OpenRegistrations { get; set; }
        public DbSet<ClosedRegistration> RegistrationHistory { get; set; }

        public DbSet<ForwardingAgency> ForwardingAgencies { get; set; }
        public DbSet<Gate> Gates { get; set; }

        public DbSet<UnknownForwardingAgency> UnkownForwardingAgencies { get; set; }

        public DbSet<ADSettings> ADSettings { get; set; }
        public DbSet<AuthorizationGroup> AuthorizationGroups { get; set; }


        public DbSet<GeneralSettings> GeneralSettings { get; set; }

        public DbSet<DisplayConfiguration> Displays { get; set; }

        public DbSet<DeliveryCountry> DeliveryCountries { get; set; }

        public DbSet<OpenTimes> OpenTimes { get; set; }

        public DbSet<ClosedTimes> ClosedTimes { get; set; }

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ForwardingAgency>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<ForwardingAgency>().HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<Gate>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<Gate>().HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<UnknownForwardingAgency>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<UnknownForwardingAgency>().HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<ADSettings>()
                .HasKey(c => new { c.ID });

            modelBuilder.Entity<AuthorizationGroup>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<AuthorizationGroup>().HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<GeneralSettings>()
                .HasKey(c => new { c.ID });

            modelBuilder.Entity<DisplayConfiguration>()
                .HasKey(c => new { c.ID });

            modelBuilder.Entity<RegistrationInformation>()
                .HasKey(c => new { c.ID });

            modelBuilder.Entity<RegistrationTimes>()
                .HasKey(c => new { c.ID });

            modelBuilder.Entity<DeliveryCountry>()
                .HasKey(c => new { c.ID });
            modelBuilder.Entity<DeliveryCountry>().HasIndex(g => g.Country).IsUnique();

            modelBuilder.Entity<AppUser>(entity =>
            {
                entity.ToTable(name: "User");
            });

            modelBuilder.Entity<IdentityRole>(entity =>
            {
                entity.ToTable(name: "Role");
            });
            modelBuilder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRoles");
            });

            modelBuilder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaims");
            });

            modelBuilder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogins");
            });

            modelBuilder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaims");

            });

            modelBuilder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserTokens");

            });
        }
    }
}
