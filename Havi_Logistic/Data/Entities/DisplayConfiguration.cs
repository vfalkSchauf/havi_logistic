﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class DisplayConfiguration
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        #region tcp communication
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public int TcpTimeoutInMs { get; set; }
        // wait this time after sending data, to receive answer or possibly swap protocol
        public int ModeBreakInMs { get; set; }
        #endregion

        #region general settings
        public string Name { get; set; }
        public EDisplayType Type { get; set; }
        public int Rows { get; set; }
        public int CharsPerLine { get; set; }
        public int RelayTime { get; set; }
        #endregion

        #region styling
        public ETextAlign TextAlign { get; set; }
        public bool UsePaging { get; set; }
        public EGtxTimespan PagingTimespan { get; set; }
        #endregion

        public bool FlashWhenCalled { get; set; }
        public EEntryRemovalType EntryRemovalType { get; set; }

    }
}
