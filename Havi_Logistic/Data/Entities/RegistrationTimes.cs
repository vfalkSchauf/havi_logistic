﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class RegistrationTimes
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid ID { get; set; }

        public Guid RegistratoinInformationID { get; set; }

        public string Gate { get; set; }

        public bool IsLastEdited { get; set; }

        public DateTime TimeOfRegistration { get; set; }

        public DateTime TimeOfCall { get; set; }

        public DateTime TimeOfEntry { get; set; }

        public DateTime TimeOfExit { get; set; }
    }
}
