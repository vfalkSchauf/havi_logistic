﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Data.Entities
{
    public class DeliveryCountry
    {
        public Guid ID { get; set; }

        public string Country { get; set; }

    }
}
