﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class ADSettings
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public bool UseAD { get; set; }

        // if set to true any new user that visits the website and sends windows authentication, will 
        // lead to the creation of a new account in the user db. The Group will be determined by the 
        // ad groups of that user. this account can then be used to login without ad once the password is set.
        public bool GenerateAccountsForNewADUsers { get; set; }

        public string ServerIP { get; set; }

        public string DomainNames { get; set; }

        public string DomainUserName { get; set; }

        public string DomainUserPassword { get; set; }
    }
}
