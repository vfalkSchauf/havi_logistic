﻿using Microsoft.AspNetCore.Identity;

namespace MVC.Data.Entities
{
    public class AppUser : IdentityUser
    {
        public string AuthorizationGroup { get; set; }
    }
}
