﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class Gate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool UseRelayAtCall { get; set; }

        //[EnumDataType(typeof(EDirectionArrows))]
        public EDirectionArrows GateDirection { get; set; }

        public EJobType GateType { get; set; }

    }
}
