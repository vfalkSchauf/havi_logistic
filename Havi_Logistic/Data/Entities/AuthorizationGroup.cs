﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class AuthorizationGroup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public string Name { get; set; }
        public string ADGroupName { get; set; }

        public bool CanUseConfig { get; set; }
        public bool CanUseProcessing { get; set; }
        public bool CanUseHistory { get; set; }
        public bool CanUseRegistration { get; set; }

        public bool AllowPickup { get; set; }
        public bool AllowDeliver { get; set; }

        public bool AllowEntrance { get; set; }
        public bool SetEntrance { get; set; }
        public bool SetExit { get; set; }
        public bool SetGate { get; set; }
        public bool Export { get; set; }
    }
}
