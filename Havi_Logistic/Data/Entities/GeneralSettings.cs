﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class GeneralSettings
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public string DefaultDeliveryGate { get; set; }

        public string DefaultPickUpGate { get; set; }

        public string DefaultDeliveryCountry { get; set; }
    }
}
