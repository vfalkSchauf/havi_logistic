﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public abstract class RegistrationInformation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid ID { get; set; }

        public string LicensePlate { get; set; }

        public string LicensePlateCompressed { get; set; }

        // Spedition
        public string ForwardingAgency { get; set; }

        public EJobType JobType { get; set; }

        public string Transportnumber { get; set; }

        public string DeliveryCountry { get; set; }

        public string Comment { get; set; }
    }
}
