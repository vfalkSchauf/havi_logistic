﻿using System;

namespace MVC.Data.Entities
{
    public class ClosedRegistration : RegistrationInformation
    {

        public int ForwardingAgencyERPID { get; set; }

        public ClosedRegistration() : base()
        {
        }

        public ClosedRegistration(OpenRegistration openRegist, int ERPID)
        {
            this.ID = openRegist.ID;
            this.ForwardingAgency = openRegist.ForwardingAgency;
            this.ForwardingAgencyERPID = ERPID;
            this.JobType = openRegist.JobType;
            this.LicensePlate = openRegist.LicensePlate;
            this.LicensePlateCompressed = openRegist.LicensePlateCompressed;
            this.Comment = openRegist.Comment;
            this.Transportnumber = openRegist.Transportnumber;
            this.DeliveryCountry = openRegist.DeliveryCountry;
        }
    }
}
