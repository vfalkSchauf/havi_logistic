﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Data.Entities
{
    public class ClosedTimes : RegistrationTimes
    {
        public ClosedTimes() :base() { }

        public ClosedTimes(OpenTimes openTimes)
        {
            this.ID = openTimes.ID;
            this.Gate = openTimes.Gate;
            this.IsLastEdited = openTimes.IsLastEdited;
            this.RegistratoinInformationID = openTimes.RegistratoinInformationID;
            this.TimeOfCall = openTimes.TimeOfCall;
            this.TimeOfEntry = openTimes.TimeOfEntry;
            this.TimeOfExit = openTimes.TimeOfExit;
            this.TimeOfRegistration = openTimes.TimeOfRegistration;
        }
    }
}
