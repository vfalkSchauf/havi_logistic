﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Data.Entities
{
    public class ForwardingAgency
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public string Name { get; set; }

        public string DeliveryGate { get; set; }

        public string PickUpGate { get; set; }

        public string DeliveryAndPickUpGate { get; set; }

        public int ERP_ID { get; set; }
    }
}
