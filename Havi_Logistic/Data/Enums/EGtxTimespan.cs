﻿namespace MVC.Data.Entities
{
    public enum EGtxTimespan : byte
    {
        T2 = 1,
        T5 = 2,
        T10 = 3,
        T20 = 4,
        T30 = 5,
        T45 = 6,
        T60 = 7,
        T90 = 8,
        T120 = 9
    }
}
