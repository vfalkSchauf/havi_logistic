﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Data.Entities
{
    public enum EJobType
    {
        [Display(Name = "Wareneingang und Warenausgang")]
        DeliverAndPickUp = 0,
        [Display(Name = "Wareneingang")]
        Deliver = 1,
        [Display(Name = "Warenausgang")]
        PickUp = 2
    }
}
