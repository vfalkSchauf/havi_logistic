﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Data.Entities
{
    public enum EDirectionArrows
    {
        [Display(Name = "Kein Richtungsangabe")]
        None = 0x00,
        [Display(Name = "\u2191")]
        Up = 0x01,
        [Display(Name = "\u2192")]
        Right = 0x02,
        [Display(Name = "\u2193")]
        Down = 0x03,
        [Display(Name = "\u2190")]
        Left = 0x04
    }
}
