﻿namespace MVC.Data.Entities
{
    public enum ETextAlign
    {
        Center, Left, Right
    }
}
